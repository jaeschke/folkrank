package org.bibsonomy.folkrank.map;

import java.util.Arrays;
import java.util.Vector;

import org.bibsonomy.folkrank.data.APRFolkRankResult;
import org.bibsonomy.folkrank.data.DefaultNodeMap;
import org.bibsonomy.folkrank.data.FolkRankData;
import org.bibsonomy.folkrank.data.FolkRankPref;
import org.bibsonomy.folkrank.data.FolkRankResult;
import org.bibsonomy.folkrank.data.StandardFolkRankResult;

import de.unikassel.cs.kde.common.container.BijectiveIntegerMap;

/**
 * Takes FolkRank data as input and maps it such that there are no gaps. Later
 * on, allows to map FolkRankResults back.
 * 
 * @author rja
 * @version $Id$
 */
public class GenericDataMapper implements DataMapper {

	private final Vector<BijectiveIntegerMap<Integer>> map;
	private final int dims;
	private final int[] noOfItemsPerDimension;
	
	public GenericDataMapper(final int dims) {
		this.dims = dims;
		this.noOfItemsPerDimension = new int[dims];
		/*
		 * initialize the maps
		 */
		this.map = new Vector<BijectiveIntegerMap<Integer>>(dims);
		for (int dim = 0; dim < dims; dim++) {
			this.map.add(new BijectiveIntegerMap<Integer>());
		}
	}

	/**
	 * From the given <tt>data</tt> object the <tt>facts</tt> and the <tt>counts</tt>
	 * are used.
	 * 
	 * @see org.bibsonomy.folkrank.process.DataMapper#mapData(org.bibsonomy.folkrank.data.FolkRankData)
	 */
	public FolkRankData mapData(final FolkRankData data) {
		return mapInternal(data.getFacts(), data.getCounts());
	}

	protected FolkRankData mapInternal(final int[][] facts, final int[][] counts) {
		final int[][] newFacts = new int[facts.length][dims]; 
		/*
		 * map the items
		 */
		for (int dim = 0; dim < dims; dim++) {
			this.noOfItemsPerDimension[dim] = counts[dim].length;
			final BijectiveIntegerMap<Integer> mapDim = map.get(dim);
			for (int i = 0; i < facts.length; i++) {
				newFacts[i][dim] = mapDim.add(facts[i][dim]);
			}
		}
		/*
		 * count items per dimension
		 */
		final int[] noOfItemsPerDimensionNew = new int[dims];
		for (int dim = 0; dim < dims; dim++) {
			noOfItemsPerDimensionNew[dim] = map.get(dim).size();
		}

		final FolkRankData newData = new FolkRankData(facts.length, noOfItemsPerDimensionNew, new DefaultNodeMap(noOfItemsPerDimensionNew));
		
		for (int i = 0; i < newFacts.length; i++) {
			newData.setFact(i, newFacts[i]);
		}
		return newData;
	}

	/* (non-Javadoc)
	 * @see org.bibsonomy.folkrank.process.DataMapper#mapBack(org.bibsonomy.folkrank.data.FolkRankResult)
	 */
	public FolkRankResult mapResult(final FolkRankResult result) {
		
		/*
		 * create new result
		 */
		final FolkRankResult newResult;
		if (result instanceof APRFolkRankResult) {
			newResult = new APRFolkRankResult();
			newResult.setAPRWeights(mapWeights(result.getAPRWeights()));
		} else {
			newResult = new StandardFolkRankResult();
		}
		newResult.setWeights(mapWeights(result.getWeights()));
		
		for (final Double error : result.getErrors()) {
			newResult.addError(error);	
		}
		
		return newResult;
	}

	private double[][] mapWeights(final double[][] weights) {
		final double[][] newWeights = new double[dims][];
		
		for (int dim = 0; dim < dims; dim++) {
			// get map for this dimension
			final BijectiveIntegerMap<Integer> mapDim = map.get(dim);
			// allocate memory for results
			newWeights[dim] = new double[noOfItemsPerDimension[dim]];
			// initialize all values  to -\infty
			Arrays.fill(newWeights[dim], Double.NEGATIVE_INFINITY);
			// iterate over weights
			for (int i = 0; i < weights[dim].length; i++) {
				// map back and store weight
				newWeights[dim][mapDim.getInverse(i)] = weights[dim][i];
			}
		}
		return newWeights;
	}

	@Override
	public FolkRankPref mapPrefs(final FolkRankPref pref) {
		final FolkRankPref newPref = new FolkRankPref(pref.getBasePrefWeight());
		
		final int[][] prefItems = pref.getPrefItems();
		
		final int[][] newPrefItems = new int[dims][];
		
		for (int dim = 0; dim < dims; dim++) {
			final int[] prefItemsDim = prefItems[dim];
			final BijectiveIntegerMap<Integer> mapDim = map.get(dim);

			newPrefItems[dim] = new int[prefItemsDim.length];
			for (int i = 0; i < prefItemsDim.length; i++) {
				newPrefItems[dim][i] = mapDim.get(prefItemsDim[i]);
			}
		}
		
		newPref.setPreference(newPrefItems, pref.getPrefValues());
		
		return newPref;
	}

	@Override
	public int mapNode(int dim, int f) {
		return this.map.get(dim).get(f);
	}

	@Override
	public int mapNodeBack(int dim, int f) {
		return this.map.get(dim).getInverse(f);
	}
	
}
