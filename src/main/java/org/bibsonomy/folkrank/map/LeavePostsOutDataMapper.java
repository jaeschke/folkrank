package org.bibsonomy.folkrank.map;

import org.bibsonomy.folkrank.data.DefaultNodeMap;
import org.bibsonomy.folkrank.data.FolkRankData;
import org.bibsonomy.folkrank.data.FolkRankPref;
import org.bibsonomy.folkrank.data.FolkRankResult;


public class LeavePostsOutDataMapper implements DataMapper {

	private final int user;
	private final int[] resources;
	private final int uDim;
	private final int rDim;

	/** Precondition: leaving out u and r does NOT cause u or r to be completely 
	 * removed (i.e. there exists at least one TAS with u and one TAS with r)
	 * @param user
	 * @param resources - FIXME: it would be more efficient, to access them using a hash set 
	 * @return
	 */
	public LeavePostsOutDataMapper(final int user, final int[] resources, final int uDim, final int rDim) {
		this.user = user;
		this.resources = resources;
		this.uDim = uDim;
		this.rDim = rDim;
	}


	@Override
	public FolkRankData mapData(final FolkRankData data) {
		/*
		 * count number of TAS that will be left out for the given user/resource combinations
		 */
		final int[][] facts = data.getFacts();
		int tasLeftOutCtr = 0;
		for (final int fact[]: facts) {
			if (fact[uDim] == user) {
				for (final int resource: resources) {
					if (fact[rDim] == resource) { 
						tasLeftOutCtr++;
						break;
					}	
				}
			}
		}
		/*
		 * calculate number of items per dimension
		 */
		final int[][] inCounts = data.getCounts();
		final int[] noOfItemsPerDimension = new int[inCounts.length];
		for (int dim = 0; dim < inCounts.length; dim++) {
			noOfItemsPerDimension[dim] = inCounts[dim].length;
		}
		final FolkRankData out = new FolkRankData(facts.length - tasLeftOutCtr, noOfItemsPerDimension, new DefaultNodeMap(noOfItemsPerDimension));

		/*
		 * copy facts
		 */
		int factId = 0;
		factLoop: for (int fact[]: facts) {
			if (fact[uDim] == user) {
				for (int resource: resources) {
					if (fact[rDim] == resource) {
						continue factLoop;
					}
				}
			}
			out.setFact(factId, fact);
			factId++;
		}
		return out;
	}

	/** Returns <tt>result</tt> since nothing is mapped (only left out).
	 * 
	 * @see org.bibsonomy.folkrank.map.DataMapper#mapResult(org.bibsonomy.folkrank.data.FolkRankResult)
	 */
	@Override
	public FolkRankResult mapResult(FolkRankResult result) {
		return result;
	}
	
	@Override
	public FolkRankPref mapPrefs(FolkRankPref pref) {
		return pref;
	}


	@Override
	public int mapNode(int dim, int f) {
		return f;
	}


	@Override
	public int mapNodeBack(int dim, int f) {
		return f;
	}

}
