package org.bibsonomy.folkrank.map;

import org.bibsonomy.folkrank.data.DefaultNodeMap;
import org.bibsonomy.folkrank.data.FolkRankData;
import org.bibsonomy.folkrank.data.FolkRankPref;
import org.bibsonomy.folkrank.data.FolkRankResult;


public class LeavePostOutDataMapper implements DataMapper {

	public static final int U = 1;
	public static final int R = 2;

	private final int user;
	private final int resource;
	

	/** Precondition: leaving out u and r does NOT cause u or r to be completely 
	 * removed (i.e. there exists at least one TAS with u and one TAS with r)
	 * @param in
	 * @param user
	 * @param resource
	 * @return
	 */
	public LeavePostOutDataMapper (final int user, final int resource) {
		this.user = user;
		this.resource = resource;
	}

	@Override
	public FolkRankData mapData(final FolkRankData data) {
		/*
		 * count number of tas for this user/post combination
		 */
		int[][] facts = data.getFacts();
		int tasCtr = 0;
		for (int fact[]: facts) {
			if (fact[U] == user && fact[R] == resource) tasCtr++;
		}
		/*
		 * calculate number of items per dimension
		 */
		int[][] counts = data.getCounts();
		int[] noOfItemsPerDimension = new int[counts.length];
		for (int dim = 0; dim < counts.length; dim++) {
			noOfItemsPerDimension[dim] = counts[dim].length;
		}
		final FolkRankData out = new FolkRankData(facts.length - tasCtr, noOfItemsPerDimension, new DefaultNodeMap(noOfItemsPerDimension));

		/*
		 * copy fact
		 */
		int factId = 0;
		for (int fact[]: facts) {
			if (fact[U] == user && fact[R] == resource) continue;
			out.setFact(factId, fact);
			factId++;
		}

		return out;
	}



	/**
	 * Results the <tt>result</tt>, since no mapping took place.
	 * 
	 * @param result
	 * @return
	 */
	@Override
	public FolkRankResult mapResult(FolkRankResult result) {
		return result;
	}

	@Override
	public FolkRankPref mapPrefs(FolkRankPref pref) {
		return pref;
	}

	@Override
	public int mapNode(int dim, int f) {
		return f;
	}

	@Override
	public int mapNodeBack(int dim, int f) {
		return f;
	}

}
