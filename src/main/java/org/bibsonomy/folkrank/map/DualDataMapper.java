package org.bibsonomy.folkrank.map;

import org.bibsonomy.folkrank.data.FolkRankData;
import org.bibsonomy.folkrank.data.FolkRankPref;
import org.bibsonomy.folkrank.data.FolkRankResult;

/**
 * @author rja
 * @version $Id$
 */
public class DualDataMapper implements DataMapper {

	private final DataMapper dataMapper1;
	private final DataMapper dataMapper2;
	
	public DualDataMapper(DataMapper dataMapper1, DataMapper dataMapper2) {
		super();
		this.dataMapper1 = dataMapper1;
		this.dataMapper2 = dataMapper2;
	}

	@Override
	public FolkRankData mapData(FolkRankData data) {
		// 1 > 2
		return dataMapper2.mapData(dataMapper1.mapData(data));
	}

	@Override
	public FolkRankPref mapPrefs(FolkRankPref pref) {
		// 1 > 2
		return dataMapper2.mapPrefs(dataMapper1.mapPrefs(pref));
	}

	@Override
	public FolkRankResult mapResult(FolkRankResult result) {
		// 1 < 2
		// take care: here we must map back!
		return dataMapper1.mapResult(dataMapper2.mapResult(result));
	}

	@Override
	public int mapNode(int dim, int f) {
		return dataMapper2.mapNode(dim, dataMapper1.mapNode(dim, f));
	}

	@Override
	public int mapNodeBack(int dim, int f) {
		return dataMapper1.mapNodeBack(dim, dataMapper2.mapNodeBack(dim, f));
	}

}
