package org.bibsonomy.folkrank.map;

import org.bibsonomy.folkrank.data.FolkRankData;
import org.bibsonomy.folkrank.data.FolkRankPref;
import org.bibsonomy.folkrank.data.FolkRankResult;

/**
 * @author rja
 * @version $Id$
 */
public interface DataMapper {

	public FolkRankData mapData(final FolkRankData data);
	
	public FolkRankPref mapPrefs(final FolkRankPref pref);

	public FolkRankResult mapResult(final FolkRankResult result);
	
	public int mapNode (final int dim, final int f);
		
	public int mapNodeBack (final int dim, final int f);

}