package org.bibsonomy.folkrank.map;

import org.bibsonomy.folkrank.data.FolkRankData;
import org.bibsonomy.folkrank.data.FolkRankPref;
import org.bibsonomy.folkrank.data.FolkRankResult;

public class NoMappingDataMapper implements DataMapper {

	@Override
	public FolkRankData mapData(FolkRankData data) {
		// TODO Auto-generated method stub
		return data;
	}

	@Override
	public FolkRankPref mapPrefs(FolkRankPref pref) {
		// TODO Auto-generated method stub
		return pref;
	}

	@Override
	public FolkRankResult mapResult(FolkRankResult result) {
		// TODO Auto-generated method stub
		return result;
	}

	@Override
	public int mapNode(int dim, int f) {
		// TODO Auto-generated method stub
		return f;
	}

	@Override
	public int mapNodeBack(int dim, int f) {
		// TODO Auto-generated method stub
		return f;
	}

	
}
