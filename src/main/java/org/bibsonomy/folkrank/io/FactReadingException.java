package org.bibsonomy.folkrank.io;

/** This exception is thrown by FactReaders when an error occurs during 
 * initalization or reading.
 *  
 * @author rja
 */
public class FactReadingException extends Exception {

    /** Last time computed: 2007-02-01
     * 
     */
    private static final long serialVersionUID = -3668806000537549237L;

    /** Constructor which allows to add a message and an existing exception to
     * this exception.
     * 
     * @param message
     * @param cause
     */
    public FactReadingException(String message, Throwable cause) {
        super(message, cause);
    }

    /** Constructor which allows to a add a message to this exception.
     * 
     * @param message
     */
    public FactReadingException(String message) {
        super(message);
    }

    /** Constructor which allows to add an existing exception to this exception.
     * @param cause
     */
    public FactReadingException(Throwable cause) {
        super(cause);
    }

}
