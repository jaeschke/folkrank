package org.bibsonomy.folkrank.io;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/** This class allows reading facts from a stream containing one fact a line and
 * where the items in a fact are separated with a fixed separator.
 * 
 * @author rja
 */
public class StreamFactReader extends AbstractFactReader implements FactReader<String> {

	/**
	 * If the number of dimensions is unknown in the constructor, the Reader will try to determine the number for himself by reading the first line of input
	 * It will then reset. If reset is not supported an Exception will be thrown
	 */
	public static final int UNKNOWN_NO_OF_DIMENSIONS = -1;
	
	
    /** Create a new StreamFactReader instance which reads facts from 
     * the stream fats where the items of a fact are separated by itemSeparator.
     *  
     * @param facts - each fact a line.
     * @param itemSeparator - a string which is used in the stream to 
     * separate the items of a fact. Care must be taken that this string is 
     * never a part of an item.
     * @param noOfDimensions TODO
     * @param factFileName TODO
     * @throws FactReadingException - if an error occurs opening the stream.
     */
    public StreamFactReader (final InputStream facts, final String itemSeparator, int noOfDimensions) throws FactReadingException {
    	super(itemSeparator);
        try {
        	this.setReader(new BufferedReader(new InputStreamReader(facts, "UTF-8")));
           	this.setNoOfDimensions(noOfDimensions);
        } catch (Exception e) {
            throw new FactReadingException(e);
        }
        
    }
    


    /** Resets the reader. If the underlying reader does not support this,
     * an exception is thrown.
     * 
     * @see org.bibsonomy.folkrank.io.FactReader#reset()
     */
    @Override
	public void reset() throws FactReadingException {
        try {
            this.getReader().reset();
            this.resetLineCtr();
        } catch (IOException e) {
            throw new FactReadingException(e);
        }
    }

}
