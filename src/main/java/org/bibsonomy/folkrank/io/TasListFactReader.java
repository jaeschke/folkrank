package org.bibsonomy.folkrank.io;

import java.util.List;

public class TasListFactReader<V> implements FactReader<V> {

    private List<V[]> tas;
    
    public TasListFactReader(final List<V[]> tas) {
        this.tas = tas;
    }
    
    public void close() throws FactReadingException {
        this.tas = null;
    }

    public V[] getFact() throws FactReadingException {
        if (tas.size() > 0) {
            return tas.remove(0);
        }
        return null;
    }

    public int getNoOfDimensions() throws FactReadingException {
        if (tas.size() > 0) {
            return tas.get(0).length;
        }
        return 0;
    }

    public boolean hasNext() throws FactReadingException {
        return tas.size() > 0;
    }

    public void reset() throws FactReadingException {
        throw new UnsupportedOperationException("Reset is not supported.");
    }

}
