package org.bibsonomy.folkrank.io;

import org.bibsonomy.folkrank.data.FolkRankData;

/** A fact writer allows to write a FolkRankData object to a persistent medium
 * in order to later read it again with a fact reader.
 *  
 * @author rja
 */
public interface FactWriter {
    
    /** Writes the facts such that they can be read again with an appropriate
     * reader.  
     * 
     * @param facts - the facts to be written.
     * @throws FactWritingException - if something exceptional happens during 
     * writing.
     */
    public void write (final FolkRankData facts) throws FactWritingException;

}
