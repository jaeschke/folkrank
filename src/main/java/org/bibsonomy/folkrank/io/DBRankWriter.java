package org.bibsonomy.folkrank.io;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Date;
import java.util.SortedSet;

import org.apache.log4j.Logger;
import org.bibsonomy.folkrank.data.FolkRankData;
import org.bibsonomy.folkrank.data.FolkRankParam;
import org.bibsonomy.folkrank.data.FolkRankPref;
import org.bibsonomy.folkrank.data.FolkRankResult;
import org.bibsonomy.folkrank.data.ItemWithWeight;
import org.bibsonomy.folkrank.data.NodeMap;
import org.bibsonomy.folkrank.database.DBHandler;
import org.bibsonomy.folkrank.process.HelperMethods;

/** Allows writing of top-k results for each dimension into a BibSonomy-style
 * database. Only preference given for one item is supported.
 *  
 * @author rja
 */
public class DBRankWriter {

    /*
    CREATE TABLE `rankings` (
            `id` int(11) NOT NULL,
            `time` timestamp NOT NULL default CURRENT_TIMESTAMP,
            `alpha` double default NULL,
            `beta` double default NULL,
            `gamma` double default NULL,
            `basepref` double default NULL,
            `dim` int(11) default NULL,
            `item` varchar(255) default NULL,
            `itemtype` tinyint(1) unsigned default NULL,
            `itempref` double default NULL,
            `delta` double default NULL,
            `iter` int(11) default NULL,
            PRIMARY KEY  (`id`),
            KEY `dim_item` (`dim`,`item`)
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
    
    CREATE TABLE `weights` (
            `id` int(11) default NULL,
            `weight` double default NULL,
            `dim` int(11) default NULL,
            `item` varchar(255) default NULL,
            `itemtype` tinyint(1) unsigned default NULL,
            KEY `id` (`id`),
            KEY `dim` (`dim`),
            CONSTRAINT `weights_ibfk_1` FOREIGN KEY (`id`) REFERENCES `rankings` (`id`) ON DELETE CASCADE
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
    */
    
    private Connection conn = null;
    private PreparedStatement stmt_insert_weight = null;
    private PreparedStatement stmt_insert_ranking = null;
    private Statement stmt = null; // to switch tables
    private ResultSet rst = null;
    
	/*
	 * how often to try to write rankings, when we run into deadlocks
	 */
	private static final int MAX_RETRY_COUNT = 5;

    private static final int RESOURCE_DIM = 2; // which one are the resources?
    
	private static final Logger log = Logger.getLogger(DBRankWriter.class);
    
    private static final String QUERY_INSERT_RANKING = "INSERT INTO " + "rankings_new" + " (id, time, alpha, beta, gamma, basepref, dim, item, itemtype, itempref, delta, iter) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
    private static final String QUERY_INSERT_WEIGHT  = "INSERT INTO " + "weights_new" +  " (id, weight, dim, item, itemtype) VALUES (?,?,?,?,?)";
    private final DBIDHandler idHandler;
    
    /** Initialize this writer with all the needed parameters to connect to the
     * database server and issue a query which returns the facts.
     * 
     * @throws SQLException - if it is not possible to set up a connection to the
     * database.
     * @throws FactReadingException - if an exception during initialization of 
     * the MySQL driver, during connecting or querying occurs.
     */
    public DBRankWriter () throws SQLException {
        this.conn = DBHandler.getInstance().getMasterConnection();
        this.conn.setAutoCommit(false);
        
        this.idHandler = new DBIDHandler(DBIDHandler.ID.RANKING, this.conn);
        this.stmt_insert_weight = conn.prepareStatement(QUERY_INSERT_WEIGHT);
        this.stmt_insert_ranking = conn.prepareStatement(QUERY_INSERT_RANKING);
    }
    
    public void close() {
        idHandler.close();
        try {rst.close();} catch (SQLException e) {err(e,"rst");/* do nothing */} finally {rst = null;}
        try {stmt_insert_weight.close();} catch (SQLException e) {err(e,"empty");/* do nothing */} finally {stmt_insert_weight = null;}
        try {stmt_insert_ranking.close();} catch (SQLException e) {err(e,"add");/* do nothing */} finally {stmt_insert_ranking = null;}
        try {stmt.close();} catch (SQLException e) {err(e,"stmt");/* do nothing */} finally {stmt = null;}
        try {conn.close();} catch (SQLException e) {err(e,"conn");/* do nothing */} finally {conn = null;}
    }
    
    private void err (final Exception e, final String msg) {
        System.err.println("error closing something in DBRankWriter: " + msg + " : "+ e);
    }
       
    
    /** Writes the top-k ranked items for each dimension into the database. 
     * Additionally, the parameters/preferences used for the ranking are stored
     * in the ranking table. Currently only one item which has been given 
     * preference will be stored.
     * 
     * @param facts - the facts data the ranking was calculated on
     * @param result - the result of the ranking
     * @param pref - the preference values used for the ranking
     * @param param - the parameters used for the ranking
     * @param k - the number of highest ranked items for each dimension which 
     * should be stored
     * @throws SQLException
     */
    public synchronized void writeTopK (final FolkRankData facts, FolkRankResult result, FolkRankPref pref, FolkRankParam param, int k) throws SQLException {
        /*
         * get item which was used to rank
         */
        final NodeMap nodeMap = facts.getNodeMap();
        final double[][] prefValues = pref.getPrefValues();
        final int[][] prefItems     = pref.getPrefItems();
        
        String prefItem  = null;
        int prefDim      = -1;
        double prefValue = -1; 
        for (int dim = 0; dim < prefItems.length; dim++) {
            for (int item = 0; item < prefItems[dim].length; item++) {
                prefItem  = nodeMap.getMapping(dim, prefItems[dim][item]);
                prefValue = prefValues[dim][item];
                prefDim   = dim;
                /*
                 * since we support only ranking of ONE item at a time, we break here
                 */
                break;
            }
        }

        /*
         * get the top k items for each dimension
         */
        final SortedSet<ItemWithWeight<String>>[] topK = HelperMethods.getTopK(facts, result, k);
        
        /*
         * write the result
         */
        writeTopK(topK, facts.getDate(), 
        		param.getAlpha(), param.getBeta(), param.getGamma(), 
        		result.getErrors().getLast(), result.getErrors().size(), 
        		prefDim, prefItem, prefValue, pref.getBasePrefWeight()[prefDim]);
    }

	/** Writes the topK elements to the database (using one transaction).
	 * 
	 * @param topK
	 * @param date
	 * @param alpha
	 * @param beta
	 * @param gamma
	 * @param delta
	 * @param iter
	 * @param prefDim
	 * @param prefItem
	 * @param prefValue
	 * @param basePref
	 * @throws SQLException - if the transaction could not be rolled back on error.
	 */
	public void writeTopK(final SortedSet<ItemWithWeight<String>>[] topK, final Date date, 
			double alpha, double beta, double gamma, 
			double delta, int iter,
			int prefDim, String prefItem, double prefValue, double basePref) throws SQLException {
		
		int retryCount = MAX_RETRY_COUNT;
		boolean transactionCompleted = false;
		int waitingTimeMilli = 2;

		do {
			try {
	            /*
	             * insert ranking metadata
	             */
	            int rankingId = insertRanking (date, alpha, beta, gamma, delta, iter, prefDim, prefItem, prefValue, basePref);

	            /*
	             * insert new weights
	             */
	            for (int dim = 0; dim < topK.length; dim++) {
	                for (final ItemWithWeight<String> item: topK[dim]) {
	                    insertWeight (rankingId, dim, item.item, item.weight);        
	                }
	            }
				conn.commit();
				
				transactionCompleted = true;
			} catch (final SQLException e) {
				conn.rollback();
				final String sqlState = e.getSQLState();
				/*
				 * http://dev.mysql.com/doc/refman/5.1/en/connector-j-usagenotes-troubleshooting.html#connector-j-examples-transaction-retry
				 * 
				 * The two SQL states that are 'retry-able' are 08S01
				 * for a communications error, and 40001 for deadlock.
				 *
				 * Only retry if the error was due to a stale connection,
				 * communications problem or deadlock
				 */
				if ("08S01".equals(sqlState) || "40001".equals(sqlState)) {
					/*
					 * try it again
					 */
					retryCount--;
					log.warn("Could not insert ranking for " + prefDim + "/" + prefItem + ". RetryCount: " + retryCount + ".", e);
					/*
					 * waiting some time for the next retry
					 */
					try {
						Thread.sleep(waitingTimeMilli);
					} catch (InterruptedException e1) {
						log.fatal(e);
					}
					waitingTimeMilli = waitingTimeMilli * 2;
				} else {
					/*
					 * don't try it again
					 */
					retryCount = 0;
					log.fatal("Could not insert ranking for " + prefDim + "/" + prefItem + ", non retryable error.", e);
				}
			} 
			/*
			 * loop until transaction is completed or retryCount is zero
			 */
		} while (!transactionCompleted && (retryCount > 0));
	}
    

    /** Inserts a ranking into the database.
     * 
     * @param date - the date when the snapshot for the ranking was taken.
     * @param alpha - ranking parameter.
     * @param beta - ranking parameter.
     * @param gamma - ranking parameter.
     * @param delta - achieved error.
     * @param iter - needed number of iterations.
     * @param prefDim - dimension of the item which was given preference.
     * @param prefItem - item which was given preference.
     * @param prefValue - the value of preference the item got.
     * @param basePref - the base preference each item got.
     * @return - the ranking id the ranking got in the database.
     * @throws SQLException - if a database error occurs.
     */
    public int insertRanking (Date date, 
        double alpha, double beta, double gamma, 
        double delta, int iter, 
        int prefDim, String prefItem, double prefValue, double basePref) throws SQLException {
        
        int prefItemType    = 0;
        String prefItemName = prefItem;
        if (prefDim == RESOURCE_DIM) {
            /*
             * extract resource type (1 = bookmark, 2 = bibtex) from resource hash
             * (first character of internal representation is resource type)
             */
            prefItemType = Integer.parseInt(prefItem.substring(0, 1));
            prefItemName = prefItem.substring(1, prefItem.length());
        }
        /*
         * get new ranking id
         */
        int id = idHandler.getNextId();
        /*
         * insert ranking
         */
        stmt_insert_ranking.setInt(1, id);
        stmt_insert_ranking.setTimestamp(2, new Timestamp(date.getTime()));
        stmt_insert_ranking.setDouble(3, alpha);
        stmt_insert_ranking.setDouble(4, beta);
        stmt_insert_ranking.setDouble(5, gamma);
        stmt_insert_ranking.setDouble(6, basePref);
        stmt_insert_ranking.setInt(7, prefDim);
        stmt_insert_ranking.setString(8, prefItemName);
        stmt_insert_ranking.setInt(9, prefItemType);
        stmt_insert_ranking.setDouble(10, prefValue);
        stmt_insert_ranking.setDouble(11, delta);
        stmt_insert_ranking.setInt(12, iter);
        stmt_insert_ranking.executeUpdate();
        return id;
    }

    /** Inserts a weight belonging to a ranking into the database.
     * 
     * @param rankingId - the id of the ranking the weight belongs to.
     * @param dim - the dimension of the item.
     * @param item - the item.
     * @param weight - the weight of the item.
     * @throws SQLException - if a database error occurs.
     */
    public void insertWeight (int rankingId, int dim, String item, double weight) throws SQLException {
        int itemtype = 0;
        String itemname = item;
        if (dim == RESOURCE_DIM) {
            /*
             * extract resource type (1 = bookmark, 2 = bibtex) from resource
             * (see insertRanking for longer explanation)
             */
            itemtype = Integer.parseInt(item.substring(0, 1));
            itemname = item.substring(1, item.length());
        }
        /*
         * insert weight
         */
        stmt_insert_weight.setInt(1, rankingId);
        stmt_insert_weight.setDouble(2, weight);
        stmt_insert_weight.setInt(3, dim);
        stmt_insert_weight.setString(4, itemname);
        stmt_insert_weight.setInt(5, itemtype);
        stmt_insert_weight.executeUpdate();
    }

}
