package org.bibsonomy.folkrank.io;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import org.bibsonomy.folkrank.data.FolkRankData;
import org.bibsonomy.folkrank.data.NodeMap;

/** Writes a fact array into a file. The items are represented as integers and 
 * for each dimension a file is written which allows to map integers to strings.
 * 
 * @author rja
 */
public class IntegerFileFactWriter implements FactWriter {

    private static final int OFFSET = 1; // TODO: make this more general, it is needed in the reader, too.
    private static final String itemSeparator = " "; // to separate items in a fact
    private final String factsFileName;
    private final String mappingFileNames[];
    
    /** Initializes the writer with the names of the files the data should be 
     * written to. The fact file is written one fact a line and each item in a
     * fact represented as integer and separated to the next one with a space.
     * 
     * @param factsFileName - the name of the file where the facts should be 
     * written to. If the file exists, it is overwritten.
     * @param mappingFileNames - the names of the files - for each dimension one
     * - where the mappings for each dimension are written to.
     */
    public IntegerFileFactWriter (String factsFileName, String[] mappingFileNames) {
        this.factsFileName = factsFileName;
        this.mappingFileNames = mappingFileNames;
    }
    
    /** Write the facts to the file.
     * @see org.bibsonomy.folkrank.io.FactWriter#write(org.bibsonomy.folkrank.data.FolkRankData)
     */
    public void write(final FolkRankData facts) throws FactWritingException {
        try {
            /*
             * write facts
             */
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(factsFileName), "UTF-8"));
            for (final int[] fact:facts.getFacts()) {
                for (final int dim: fact) {
                    writer.write((dim + OFFSET) + itemSeparator);
                }
                writer.write("\n");
            }
            writer.close();
            /*
             * write mappings
             */
            final NodeMap nodeMap = facts.getNodeMap();
            for (int dim = 0; dim < facts.getDims(); dim++) {
                writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(this.mappingFileNames[dim]), "UTF-8"));
                for (final String s: nodeMap.getMapping(dim)) {
                    writer.append(s + "\n");
                }
                writer.close();
            }
            
        } catch (Exception e) {
            throw new FactWritingException(e);
        }
    }

}
