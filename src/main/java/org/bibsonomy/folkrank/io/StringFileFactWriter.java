package org.bibsonomy.folkrank.io;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

import org.bibsonomy.folkrank.data.FolkRankData;
import org.bibsonomy.folkrank.data.NodeMap;

/** Writes a fact array of integers into a file. Facts are represented as 
 * strings.
 * 
 * @author rja
 */
public class StringFileFactWriter implements FactWriter {

    private final OutputStream stream;
    private final String factSeparator;
    
    /** Initialize the writer with the file name where the facts should be 
     * written to and a separator which separates items within a fact.
     * Note that this separator must not be part of an item, otherwise it will
     * be difficult to re-extract the facts from the file.
     *  
     * @param factsFileName - the name of the file the facts should be written 
     * to. If the file exists, it is overwritten.
     * @param factSeparator - a separator which is written between the items of 
     * a fact to separate them when reading the file.
     * @throws FileNotFoundException if file is not found
     */
    public StringFileFactWriter (String factsFileName, String factSeparator) throws FileNotFoundException {
        this.stream = new FileOutputStream(factsFileName);
        this.factSeparator = factSeparator;
    }
    
    /** Initialize the writer with an output stream where the facts should be 
     * written to and a separator which separates items within a fact.
     * Note that this separator must not be part of an item, otherwise it will
     * be difficult to re-extract the facts from the file.
     * @param stream - the output stream where this writer will write to.
     * @param factSeparator - a separator which is written between the items of 
     * a fact to separate them when reading the file.
     */
    public StringFileFactWriter (OutputStream stream, String factSeparator) {
        this.stream = stream;
        this.factSeparator = factSeparator;
    }
    
    /** Write the facts to the file.
     * 
     * @see org.bibsonomy.folkrank.io.FactWriter#write(org.bibsonomy.folkrank.data.FolkRankData)
     */
    public void write(final FolkRankData facts) throws FactWritingException {
        try {
            /*
             * write facts
             */
            final NodeMap mapping = facts.getNodeMap();
            final BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(stream, "UTF-8"));
            for (final int[] fact:facts.getFacts()) {
                for (int dim = 0; dim < fact.length; dim++) {
                    writer.write(mapping.getMapping(dim, fact[dim]));
                    if (dim < fact.length - 1) {
                        /*
                         * write separator only between items, not after last
                         * item
                         */
                        writer.write(factSeparator);
                    }
                }
                writer.write("\n");
            }
            writer.close();
        } catch (IOException e) {
            throw new FactWritingException(e);
        }
    }

}
