package org.bibsonomy.folkrank.io;


/** This FactReader can be given an array from which it reads the facts.
 * 
 * @author rja
 */
public class ArrayFactReader<V> implements FactReader<V> {

    private V[][] facts = null;
    private int pos = -1;
    
    /** Initialize with an array of facts.
     * 
     * @param facts
     */
    public ArrayFactReader (V[][] facts) {
        this.facts = facts; 
    }
    
    /** Return the next fact.
     * @see org.bibsonomy.folkrank.io.FactReader#getFact()
     */
    public V[] getFact() {
        return facts[pos];
    }

    /** Check if another fact is available.
     * @see org.bibsonomy.folkrank.io.FactReader#hasNext()
     */
    public boolean hasNext() {
        pos++;
        return facts.length > pos;
    }

    /** Reset the FactReader.
     * @see org.bibsonomy.folkrank.io.FactReader#reset()
     */
    public void reset() {
        pos = -1;
    }

    /** Returns the number of dimensions for the returned facts. If the fact 
     * array is null, a FactReadingException is thrown.
     * @see org.bibsonomy.folkrank.io.FactReader#getNoOfDimensions()
     */
    public int getNoOfDimensions() throws FactReadingException {
        if (facts == null) {
            throw new FactReadingException("Number of dimensions is not known, yet.");
        }
        return facts[0].length;
    }

    /** Sets the fact array to null and makes this reader practically unusable. 
     * @see org.bibsonomy.folkrank.io.FactReader#close()
     */
    public void close() throws FactReadingException {
        facts = null;
    }
    
}
