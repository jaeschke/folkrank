package org.bibsonomy.folkrank.io;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

public class AbstractFactReader implements FactReader<String> {

	private static final int LINE_CTR_LOG_INTERVAL = 1000000;
    private BufferedReader reader;
    protected final String itemSeparator; 
 	private String line;
    private int lineCtr;
    private int noOfDimensions;

    protected static final Logger log = Logger.getLogger(AbstractFactReader.class);;
    
    public AbstractFactReader(String itemSeparator) {
        this.itemSeparator = Pattern.quote(itemSeparator);
        this.lineCtr =0;
	}

	/** Returns the next fact from the file.
     * 
     * @see org.bibsonomy.folkrank.io.FactReader#getFact()
     */
    public String[] getFact() throws FactReadingException {
        /*
         * decompose line
         */
        return this.line.split(this.itemSeparator);
    }

    /** 
     * @see org.bibsonomy.folkrank.io.FactReader#getNoOfDimensions()
     */
    public int getNoOfDimensions() throws FactReadingException {
        return this.noOfDimensions;
    }

    
    /** Returns <code>true</code> if there are more facts to read available.
     * @see org.bibsonomy.folkrank.io.FactReader#hasNext()
     */
    public boolean hasNext() throws FactReadingException {
        try {
        	this.lineCtr++;
        	
        	if (this.lineCtr % LINE_CTR_LOG_INTERVAL == 0) log.debug("read " + lineCtr + " facts");
        	
            return (this.line = this.reader.readLine()) != null;
        } catch (IOException e) {
            throw new FactReadingException(e);
        }
    }

  /** Closes the readers associated with that reader.
     * @see org.bibsonomy.folkrank.io.FactReader#close()
     */
    public void close() throws FactReadingException {
        try {
            reader.close();
        } catch (IOException e) {
            throw new FactReadingException(e);
        }
    }

    public BufferedReader getReader() {
 		return reader;
 	}

	public void setReader(BufferedReader reader) {
		this.resetLineCtr();
		this.reader = reader;
	}

   	public void resetLineCtr() {
 		this.lineCtr = 0;
 	}

  	public void setNoOfDimensions(int noOfDimensions) {
 		this.noOfDimensions = noOfDimensions;
 	}

	public void reset() throws FactReadingException {
		throw new FactReadingException("reset is not implemented for this factReader.");
	}

    
}
