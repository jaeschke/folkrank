package org.bibsonomy.folkrank.io;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;

public class DBIDHandler {

	private PreparedStatement stmt_get = null;
	private PreparedStatement stmt_inc = null;

	private static final Logger log = Logger.getLogger(DBIDHandler.class);

	/** Constructor creating an instance of DBIDHandler. If needed ID columns 
	 * in ids table do not exist, they're created. 
	 * 
	 * @param id - the ID which should be handled by this ID handler.
	 * @param conn - a database connection
	 * @throws SQLException
	 */
	public DBIDHandler(final ID id, final Connection conn) throws SQLException {
		/*
		 * Creating ID, if neccessary.
		 */
		log.debug("Creating ID " + id.getDescription());
		conn.createStatement().executeUpdate("INSERT IGNORE INTO ids (name, value, description) VALUES (" + id.getName() + ", 1, '" + id.getDescription()+ "')");
		if (!conn.getAutoCommit()) {
			conn.commit();
		}
		/*
		 * preparing statements
		 */
		stmt_get  = conn.prepareStatement("SELECT value FROM ids WHERE name = " + id.getName() + " LIMIT 1 FOR UPDATE");
		stmt_inc  = conn.prepareStatement("UPDATE ids SET value = value + 1 WHERE name = " + id.getName());
	}

	/** Get a new Id with the given name; increment id counter.
	 * 
	 * @return
	 * @throws SQLException - If the statements failed.
	 */
	public int getNextId () throws SQLException {
		int idValue = -1;
		final ResultSet rst = stmt_get.executeQuery();
		if (rst.next()) {
			/*
			 * increment id
			 */
			if (1 == stmt_inc.executeUpdate()) {
				idValue = rst.getInt("value");
			}
		}
		rst.close();
		return idValue;
	}

	/**
	 * Closes all prepared statements.
	 */
	public void close() {
		try {stmt_get.close();} catch (final SQLException e) {/* do nothing */} finally {stmt_get = null;}
		try {stmt_inc.close();} catch (final SQLException e) {/* do nothing */} finally {stmt_inc = null;}
	}

	/**
	 * An ID in the ids table.
	 * 
	 * @author rja
	 *
	 */
	public static enum ID {
		RANKING_JOB(11),
		RANKING(10);

		private int id;

		private ID (final int id) {
			this.id = id;
		}

		public int getName() {
			return this.id;
		}

		public String getDescription() {
			switch (id) {
			case 11:
				return "ranking job";
			case 10:
				return "ranking";
			}
			return null;
		}

	}

}
