package org.bibsonomy.folkrank.io;


/**
 * TODO Move to kde-intern
 * @author sdo
 *
 * @param <A>
 * @param <B>
 */
public class Pair<A, B> {
	/**
	 * The first element
	 */
	public A a;

	/**
	 * The second element which is comparable
	 */
	public B b;
	/** The only available constructor. 
	 *  
	 * @param item - a string which represents the item.
	 * @param weight - the weight of the item.
	 */
	public Pair(A a, B b) {
		super();
		this.a = a;
		this.b = b;
	}

	/** Returns the first Element (a).
	 * @return - a.
	 */
	public A getA() {
		return this.a;
	}
	/** Returns the second element (b9)
	 * @return - b
	 */
	public B getB() {
		return this.b;
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		@SuppressWarnings("rawtypes")
		Pair other = (Pair) obj;
		if (a == null) {
			if (other.a != null)
				return false;
		} else if (!a.equals(other.a))
			return false;
		if (b == null) {
			if (other.b != null)
				return false;
		} else if (!b.equals(other.b))
			return false;
		return true;
	}




	/** Returns a String representation of this pair in the form
	 * <code>a(b)</code>.
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return this.a.toString() + "(" + this.b.toString() + ")";
	}

}