package org.bibsonomy.folkrank.io;


/** A FactReader returns every single fact as an array (for each 
 * dimension one element). By reading the facts until hasNext() == false one 
 * can gather all facts of a dataset. 
 * 
 * @author rja
 */
public interface FactReader<V> {

    /** Returns the next fact as an array of strings - one string for each 
     * dimension.
     * 
     * @return An Array of strings, one string for each dimension.
     * @throws FactReadingException
     */
    public V[] getFact() throws FactReadingException;
    /** Tests if there are more facts available to read. 
     *
     * @return <code>true</code> if there are more facts to read.
     * @throws FactReadingException
     */
    public boolean hasNext() throws FactReadingException;
    /** Resets the fact reader so that the facts can be read again. 
     * <strong>Note:</strong> some implementations may not return the same facts 
     * after calling reset() since the facts may have changed since the last 
     * reading.  
     *  
     * @throws FactReadingException
     */
    public void reset() throws FactReadingException;
    /** Returns the number of dimensions of the fact source.
     * @return An integer determining the number of dimensions. 
     * @throws FactReadingException
     */
    public int getNoOfDimensions() throws FactReadingException;
    /** Close the reader. One a reader has been closed, reset(), hasNext() or 
     * getFact() operations will throw a FactReadingException.
     * 
     * @throws FactReadingException
     */
    public void close () throws FactReadingException;
    
}
