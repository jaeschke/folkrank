package org.bibsonomy.folkrank.io;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

/** This class allows reading facts from a file containing one fact a line and
 * where the items in a fact are separated with a fixed separator.
 * 
 * @author rja
 */
public class FileFactReader extends AbstractFactReader implements FactReader<String> {

    private final String factsFileName;

    /** Create a new FileFactReader instance which reads facts from 
     * factsFileName where the items of a fact are separated by itemSeparator.
     *  
     * @param factsFileName - the name of the file which contains the facts - 
     * each fact a line.
     * @param itemSeparator - a string which is used in the facts file to 
     * separate the items of a fact. Care must be taken that this string is 
     * never a part of an item.
     * @throws FactReadingException - if an error occurs opening the file.
     * @throws FileNotFoundException 
     */
    public FileFactReader (final String factsFileName, final String itemSeparator) throws FactReadingException, FileNotFoundException {
    	super(itemSeparator);
    	this.factsFileName = factsFileName;
    	try {
    		this.setReader(new BufferedReader(new InputStreamReader(new FileInputStream(factsFileName), "UTF-8")));
    		if (!this.hasNext()) {
    			throw new IllegalArgumentException("The factsFileName does not contain a single line of facts. The number of Dimensions could not be determined.");
    		}
            this.setNoOfDimensions(this.getFact().length);
            this.reset();
            /*
             * TODO: implement proper exception handling
             */
        } catch (Exception e) {
            throw new FactReadingException(e);
        }
        
    }
    
    /** Resets the reader. If the underlying file reader does not support this,
     * an exception is thrown.
     * 
     * @see org.bibsonomy.folkrank.io.FactReader#reset()
     */
    @Override
	public void reset() throws FactReadingException {
        try {
            getReader().close();
            setReader(new BufferedReader(new InputStreamReader(new FileInputStream(factsFileName), "UTF-8")));
        } catch (IOException e) {
            throw new FactReadingException(e);
        }
    }

}
