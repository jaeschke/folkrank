package org.bibsonomy.folkrank.io;

/** This exception is thrown by FactWriters when something exceptional happens
 * during initialization or writing.
 *   
 * @author rja
 */
public class FactWritingException extends Exception {

    /** Calculated on 2007-02-01
     *
     */
    private static final long serialVersionUID = 3212759053915985031L;

    /** Constructor which allows to add a message and an existing exception to
     * this exception.
     * 
     * @param message
     * @param cause
     */
    public FactWritingException(String message, Throwable cause) {
        super(message, cause);
    }

    /** Constructor which allows to a add a message to this exception.
     * 
     * @param message
     */
    public FactWritingException(String message) {
        super(message);
    }

    /** Constructor which allows to add an existing exception to this exception.
     * @param cause
     */
    public FactWritingException(Throwable cause) {
        super(cause);
    }

}
