package org.bibsonomy.folkrank.io;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;
import org.bibsonomy.folkrank.database.DBHandler;

public class DBJobQueue {


	/*
	 * Status of jobs in ranking_queue table.
	 */
	private static final int STATUS_JOB_FREE    = 0;
	private static final int STATUS_JOB_RUNNING = 1;
	/*
	 * how often to try to get/remove a job, when we run into deadlocks
	 */
	private static final int MAX_RETRY_COUNT = 5;

	private ResultSet rst                = null;
	private PreparedStatement stmt_empty = null;
	private PreparedStatement stmt_add   = null;
	private PreparedStatement stmt_del   = null;
	private PreparedStatement stmt_upda  = null;
	private PreparedStatement stmt_next  = null;
	private PreparedStatement stmt_kill  = null;
	private DBIDHandler idHandler        = null;
	private Connection conn              = null;


	private static final Logger log = Logger.getLogger(DBJobQueue.class);

	/*
	 * 
	 *  CREATE TABLE `ranking_queue` (
	 *    `dim` int(11) NOT NULL default '0',
	 *    `item` varchar(255) character set utf8 collate utf8_bin NOT NULL default '',
	 *    `date` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,  PRIMARY KEY  (`dim`,`item`),
	 *    KEY `date_idx` (`date`)
	 *  ) ENGINE=InnoDB DEFAULT CHARSET=utf8 
	 */


	/**
	 * @throws SQLException - If the statements could not be prepared.
	 */
	public DBJobQueue () throws SQLException  {
		this.conn = DBHandler.getInstance().getJobQueueConnection();
		/*
		 * idHandler gets the same connection
		 */
		this.idHandler = new DBIDHandler(DBIDHandler.ID.RANKING_JOB, this.conn);
		/*
		 * disable auto commit
		 */
		this.conn.setAutoCommit(false);
		//conn.setTransactionIsolation(Connection.TRANSACTION_REPEATABLE_READ);
		conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
		/*
		 * prepare statements
		 */
		stmt_empty = conn.prepareStatement("SELECT * FROM ranking_queue WHERE status < 3 LIMIT 1");
		stmt_add   = conn.prepareStatement("INSERT INTO ranking_queue (id, dim, item) VALUES (?, ?, ?)");
		stmt_del   = conn.prepareStatement("DELETE FROM ranking_queue WHERE dim = ? AND item = ?");
		//stmt_del   = conn.prepareStatement("UPDATE ranking_queue SET status = 3 WHERE dim = ? AND item = ?");

		/*
		 * These two statements are for #getNextJob().
		 * 
		 * First, we pick a free (status = 0) job from the database and mark 
		 * it as running (status = 1). We also remember its ID with 
		 * LAST_INSERT_ID(id). Then, we query for this IDs dim and item column.
		 * 
		 * Doing it that way is not only multi-threading-, but also multi-user-safe!
		 * 
		 * See also:
		 * 
		 * http://dev.mysql.com/doc/refman/5.1/en/information-functions.html#function_last-insert-id
		 * http://dev.mysql.com/doc/refman/5.1/en/innodb-locking-reads.html
		 */
		stmt_upda  = conn.prepareStatement("UPDATE ranking_queue SET status = " + STATUS_JOB_RUNNING + ", id = LAST_INSERT_ID(id) WHERE status = " + STATUS_JOB_FREE + " LIMIT 1");
		stmt_next  = conn.prepareStatement("SELECT dim, item, id FROM ranking_queue JOIN (SELECT LAST_INSERT_ID() AS id) AS insert_id USING (id)");

		stmt_kill  = conn.prepareStatement("UPDATE ranking_queue SET status = " + STATUS_JOB_FREE + " WHERE status = " + STATUS_JOB_RUNNING + " AND TIME_TO_SEC(TIMEDIFF(CURRENT_TIMESTAMP(),date)) > ?");

	}

	/** Gets the next job from the database.
	 * 
	 * @return
	 * @throws SQLException - if the statements failed and the transaction could not be rolled back.
	 */
	public synchronized RankingJob getNextJob () throws SQLException {
		RankingJob job = null;

		int retryCount = MAX_RETRY_COUNT;
		boolean transactionCompleted = false;
		int waitingTimeMilli = 2;

		do {
			try {
				job = getJob();

				transactionCompleted = true;
			} catch (final SQLException e) {
				conn.rollback();
				final String sqlState = e.getSQLState();
				/*
				 * http://dev.mysql.com/doc/refman/5.1/en/connector-j-usagenotes-troubleshooting.html#connector-j-examples-transaction-retry
				 * 
				 * The two SQL states that are 'retry-able' are 08S01
				 * for a communications error, and 40001 for deadlock.
				 *
				 * Only retry if the error was due to a stale connection,
				 * communications problem or deadlock
				 */
				if ("08S01".equals(sqlState) || "40001".equals(sqlState)) {
					/*
					 * try it again
					 */
					retryCount--;
					log.warn("Could not get a new job. RetryCount: " + retryCount + ". Exception was: " + e);
					/*
					 * waiting some time for the next retry
					 */
					try {
						Thread.sleep(waitingTimeMilli);
					} catch (InterruptedException e1) {
						log.fatal(e);
					}
					waitingTimeMilli = waitingTimeMilli * 2;
				} else {
					/*
					 * don't try it again
					 */
					retryCount = 0;
					log.fatal("Could not get a new job, non retryable error: " + e);
				}
			} finally {
				/*
				 * close result set
				 */
				if (rst != null) {
					try {
						rst.close();
					} catch (final SQLException e) {
						log.fatal("Could not close result set: " + e);
					}
				}
			}
			/*
			 * loop until transaction is completed or retryCount is zero
			 */
		} while (!transactionCompleted && (retryCount > 0));
		return job;
	}

	/** Inner method which calls the database statements.
	 * 
	 * @return
	 * @throws SQLException
	 */
	private RankingJob getJob() throws SQLException {
		RankingJob job = null;
		/*
		 * Mark job as running. If there is no non-running job, the update row count is zero.
		 */
		if (stmt_upda.executeUpdate() > 0) {
			/*
			 * get last updated row
			 */
			rst = stmt_next.executeQuery();
			if (rst.next()) {
				final int dim = rst.getInt("dim");
				final String item = rst.getString("item");
				final int id = rst.getInt("id");
				/*
				 * create job
				 */
				job = new RankingJob(dim, item, id);
				/*
				 * release result set
				 */
				rst.close();
				rst = null;
			}
		}
		conn.commit();
		return job;
	}

	/** Calls commit on the idHandler and also on this classes connection.
	 * 
	 * @throws SQLException - if the commit failed
	 */
	public void commitAdd() throws SQLException {
		conn.commit();
	}

	/** Checks if the queue contains some elements.
	 * 
	 * @return <code>true</code> if queue contains zero elements.
	 */
	public boolean isEmpty () {
		/*
		 * if "SELECT * FROM ranking_queue LIMIT 1" returns a result, the queue is not empty  
		 */
		try {
			return !stmt_empty.executeQuery().next();
		} catch (SQLException e) {
			log.error("Could not check if queue is empty. Assuming it to be not empty.", e);
		}
		return false;
	}

	/** Kills all jobs which are running longer than maxSeconds seconds.
	 * 
	 * @param maxSeconds
	 * @return
	 * @throws SQLException
	 */
	public int killOldJobs(final int maxSeconds) {
		int numberOfKilledJobs = 0;
		try {
			stmt_kill.setInt(1, maxSeconds);
			numberOfKilledJobs = stmt_kill.executeUpdate();
			conn.commit();
			if (numberOfKilledJobs > 0) {
				log.info("Killed " + numberOfKilledJobs + " jobs which were running longer than " + maxSeconds + " seconds.");
			} 
		} catch (final SQLException e) {
			log.error("could not kill jobs: " + e);
		}
		return numberOfKilledJobs;
	}

	/** Removes a job from the queue.
	 * @param job
	 * @throws SQLException - if transaction could not be rolled back on error.
	 */ 
	public synchronized void remove (final RankingJob job) throws SQLException {		
		int retryCount = MAX_RETRY_COUNT;
		boolean transactionCompleted = false;
		int waitingTimeMilli = 2;

		do {
			try {
				stmt_del.setInt(1, job.dim);
				stmt_del.setString(2, job.item);
				stmt_del.executeUpdate();
				conn.commit();
				
				transactionCompleted = true;
			} catch (final SQLException e) {
				conn.rollback();
				final String sqlState = e.getSQLState();
				/*
				 * http://dev.mysql.com/doc/refman/5.1/en/connector-j-usagenotes-troubleshooting.html#connector-j-examples-transaction-retry
				 * 
				 * The two SQL states that are 'retry-able' are 08S01
				 * for a communications error, and 40001 for deadlock.
				 *
				 * Only retry if the error was due to a stale connection,
				 * communications problem or deadlock
				 */
				if ("08S01".equals(sqlState) || "40001".equals(sqlState)) {
					/*
					 * try it again
					 */
					retryCount--;
					log.warn("Could not delete job " + job + ". RetryCount: " + retryCount + ". Exception was: " + e);
					/*
					 * waiting some time for the next retry
					 */
					try {
						Thread.sleep(waitingTimeMilli);
					} catch (InterruptedException e1) {
						log.fatal(e);
					}
					waitingTimeMilli = waitingTimeMilli * 2;
				} else {
					/*
					 * don't try it again
					 */
					retryCount = 0;
					log.fatal("Could not delete job " + job + ", non retryable error: " + e);
				}
			} 
			/*
			 * loop until transaction is completed or retryCount is zero
			 */
		} while (!transactionCompleted && (retryCount > 0));
	}

	public void add (final RankingJob job) throws SQLException {
		try {
			addJob(job, idHandler.getNextId());
			conn.commit();
		} catch (final SQLException e) {
			conn.rollback();
			throw e;
		}
	}

	/**
	 * @param job
	 * @throws SQLException
	 */
	public void addWithoutCommit(RankingJob job) throws SQLException {
		addJob(job, idHandler.getNextId());
	}

	private void addJob(RankingJob job, final int nextId) throws SQLException {
		stmt_add.setInt(1, nextId);
		stmt_add.setInt(2, job.dim);
		stmt_add.setString(3, job.item);
		stmt_add.executeUpdate();
	}


	public void close() throws FactReadingException {
		idHandler.close();
		try {rst.close();}        catch (SQLException e) {err(e,"rst");  /* do nothing */} finally {rst        = null;}
		try {stmt_empty.close();} catch (SQLException e) {err(e,"empty");/* do nothing */} finally {stmt_empty = null;}
		try {stmt_add.close();}   catch (SQLException e) {err(e,"add");  /* do nothing */} finally {stmt_add   = null;}
		try {stmt_del.close();}   catch (SQLException e) {err(e,"del");  /* do nothing */} finally {stmt_del   = null;}
		try {stmt_next.close();}  catch (SQLException e) {err(e,"next"); /* do nothing */} finally {stmt_next  = null;}
		try {stmt_upda.close();}  catch (SQLException e) {err(e,"upda"); /* do nothing */} finally {stmt_upda  = null;}
		try {stmt_kill.close();}  catch (SQLException e) {err(e,"kill"); /* do nothing */} finally {stmt_kill  = null;}
		try {conn.close();}       catch (SQLException e) {err(e,"conn"); /* do nothing */} finally {conn       = null;}
	}

	private void err (final Exception e, final String msg) {
		log.fatal(msg + " : " + e);
	}

	public static class RankingJob {
		public String item;
		public int dim;
		public int id;

		public RankingJob(int dim, String item, int id) {
			super();
			this.item = item;
			this.id = id;
			this.dim = dim;
		}
		
		@Override
		public String toString() {
			return dim + "/" + id + "(" + item + ")";
		}
		
	}
}
