package org.bibsonomy.folkrank.io;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


/** This FactReader reads facts from a MySQL database. This is done by giving it
 * a "SELECT" query string which returns for each dimension of the fact a 
 * column. Then one row of the result set is interpreted as a fact.
 * 
 * @author rja
 */
public class DBFactReader implements FactReader<String> {

    private Connection conn        = null;
    private ResultSet rst          = null;
    private PreparedStatement stmt = null;
    private final int noOfDimensions;

    /** Initialize this reader with all the needed parameters to connect to the
     * database server and issue a query which returns the facts.
     * 
     * @param conn - a connection to an SQL database.
     * @param query - The SQL query which selects all facts which should be 
     * returned by this reader. The query must select at least noOfDimensions
     * columns. Each column is regarded as a dimension of the facts. 
     * @param noOfDimensions - the number of dimensions of the facts to read. 
     * Must not be larger than the number of selected columns in the query.
     * @throws FactReadingException - if an exception during initialization of 
     * the MySQL driver, during connecting or querying occurs.
     */
    public DBFactReader (Connection conn, String query, int noOfDimensions) throws FactReadingException {
        this.noOfDimensions = noOfDimensions;
        try {
            this.conn = conn;
            stmt = conn.prepareStatement(query);
            rst  = stmt.executeQuery();
            /*
             * TODO: implement proper exception handling
             */
        } catch (SQLException e) {
            throw new FactReadingException(e);
        }
    }

    /** Returns the next fact from the database. 
     * 
     * @see org.bibsonomy.folkrank.io.FactReader#getFact()
     */
    public String[] getFact() throws FactReadingException {
        try {
            String[] fact = new String[noOfDimensions];
            for (int dim = 0; dim < fact.length; dim++) {
                fact[dim] = rst.getString(dim + 1); // columns go from 1 ... length
            }
            return fact;
        } catch (SQLException e) {
            throw new FactReadingException (e);

        }
    }

    /** Returns <code>true</code> if another fact is available, otherwise 
     * <code>false</code>.
     * 
     * @see org.bibsonomy.folkrank.io.FactReader#hasNext()
     */
    public boolean hasNext() throws FactReadingException {
        try {
            return rst.next();
        } catch (SQLException e) {
            throw new FactReadingException (e);
        } 
    }

    /** Resets the fact reader by re-executing the SQL statement. This also 
     * means that the facts in the subsequent calls to getFact() may be 
     * different, since the underlying database may have changed.
     * 
     * @throws FactReadingException - if an error with the database connection
     * occurs.
     * @see org.bibsonomy.folkrank.io.FactReader#reset()
     */
    public void reset() throws FactReadingException {
        try {
            rst = stmt.executeQuery();
        } catch (SQLException e) {
            throw new FactReadingException (e);
        }
    }

    /** Returns the number of dimensions of the facts.  
     * @see org.bibsonomy.folkrank.io.FactReader#getNoOfDimensions()
     */
    public int getNoOfDimensions() throws FactReadingException {
        return noOfDimensions;
    }

    /** Close result set, statement and connection. 
     * @see org.bibsonomy.folkrank.io.FactReader#close()
     */
    public void close() throws FactReadingException {
        try {rst.close(); } catch (SQLException e) {/* do nothing */} finally {rst  = null;}
        try {stmt.close();} catch (SQLException e) {/* do nothing */} finally {stmt = null;}
        try {conn.close();} catch (SQLException e) {/* do nothing */} finally {conn = null;}
    }
}
