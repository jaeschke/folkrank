//package org.bibsonomy.folkrank;
//
//import java.util.Arrays;
//
//import org.bibsonomy.folkrank.data.FolkRankData;
//import org.bibsonomy.folkrank.data.FolkRankParam;
//import org.bibsonomy.folkrank.data.FolkRankPref;
//import org.bibsonomy.folkrank.data.FolkRankResult;
//
//public class NarrowFolkRank extends AbstractFolkRank {
//
//	
//	   /**
//     * Constructor setting param for baseline and preferenced iterations
//     * @param baselineParam
//     * @param preferenceParam
//     */
//    public NarrowFolkRank (final FolkRankParam baselineParam, final FolkRankParam preferenceParam) {
//    	super(baselineParam, preferenceParam);
//    }
//    
//    /**
//     * Constructor setten the same param for both baseline and preferenced iterations
//     * @param param
//     */
//    public NarrowFolkRank (final FolkRankParam param) {
//        this(param, param);
//    }
// 
//    @Override
//	public FolkRankResult computePageRank(FolkRankData factsData, FolkRankPref pref) {
//        /*
//         * input data
//         */
//        final int[][] facts      = factsData.getFacts();
//        final int[][] counts     = factsData.getCounts();
//        /*
//         * Construct another data structure from the tas list of the form
//         * 
//         */
//          
//        /*
//         * weight vectors
//         */
//        final double[][] weights    = new double[counts.length][]; // weight 
//        final double[][] newWeights = new double[counts.length][]; // temporary weight
//        final double[][] spread     = new double[counts.length][]; // spreading weight
//
//        final double[] pWeights     = new double[counts.length]; // random surfer weights
//        final double[] pWeightsSum  = new double[counts.length]; // sum of random surfer + preference weights
//
//        final int[][] prefNodes     = pref.getPrefItems();  // nodes with preference
//        final double[][] prefValues = pref.getPrefValues(); // preference values 
//        
//        /*
//         * These vectors are used to control the final weight computation:
//         * alpha * oldWeight  +  beta * newWeight  +  gamma * pref
//         * with alpha + beta + gamma = 1 
//         */
//        final double alpha[] = new double[counts.length]; Arrays.fill(alpha, param.getAlpha());
//        final double beta [] = new double[counts.length]; Arrays.fill(beta,  param.getBeta());
//        final double gamma[] = new double[counts.length]; Arrays.fill(gamma, param.getGamma());
//        /*
//         * TODO: is this correct?
//         * If we have no preference items, we ensure normalized vectors by 
//         * adding gamma to alpha and beta.
//         */
//        for (int dim = 0; dim < alpha.length; dim++) {
//        	if (prefNodes == null || prefNodes[dim] == null || prefNodes[dim].length == 0) {
//        		final double sum = alpha[dim] + beta[dim];
//				alpha[dim] += (alpha[dim] * gamma[dim]) / sum;
//        		beta[dim]  += (beta[dim]  * gamma[dim]) / sum;
//        	}
//        }
//
//        /*
//         * output data
//         */
//        //FolkRankResult result = new StandardFolkRankResult();
//        final FolkRankResult result = new APRFolkRankResult(); // TODO: choose correct one
//        
//        
//
//        
//
//        /*
//         * allocate memory for weight vectors
//         */
//        for (int dim = 0; dim < weights.length; dim++) {
//            newWeights[dim] = new double[counts[dim].length];
//            spread[dim]     = new double[counts[dim].length];
//            weights[dim]    = new double[counts[dim].length];
//        }
//        
//        /*
//         * initialize weights, random surfer, preference sums
//         */
//        
//        param.getWeightInitializationStrategy().initalizeWeights(pref, weights, pWeights, pWeightsSum);
//        
////        Util.printWeightsSums(weights, "  pre ");
//        
//        /* ********************************************************************
//         * main loop
//         * ********************************************************************/
//        int iter = 0;
//        double delta = Double.MAX_VALUE;
//        while (iter < param.getMaxIter() && delta > param.getEpsilon()) {
//            iter++;
//
//            /*
//             * initalize new weights with zero
//             */
//            for (double[] newWeightsDim:newWeights) {
//                Arrays.fill(newWeightsDim, 0.0);
//            }
//
//            /*
//             * initalize spread
//             */
//            for (int dim = 0; dim < spread.length; dim++) {
//                for (int node = 0; node < spread[dim].length; node++) {
//                    spread[dim][node] = weights[dim][node] / counts[dim][node];  
//                }
//            }
//
//            /*
//             * spread weight
//             */
//            for (int fact[]: facts) {
//                for (int dim = 0; dim < newWeights.length; dim++) {
//                    /*
//                     * newWeights[dim][fact[dim]] gets weight from all other
//                     * nodes at this hyperedge
//                     */
//                    for (int dimAdd = 0; dimAdd < newWeights.length; dimAdd++) {
//                        if (dim != dimAdd) {
//                            /*
//                             * get weight from surrounding nodes
//                             */
//                            newWeights[dim][fact[dim]] += spread[dimAdd][fact[dimAdd]];
//                        } else {
//                            /*
//                             * ignore own weight
//                             */
//                        }
//                    }
//                }
//            }
//
//            //newweights = A*weights
//            /*
//             * calculate new weights
//             */
//            delta = 0.0;
//            double newWeight;
//            for (int dim = 0; dim < weights.length; dim++) {
//                
//                /*
//                 * spread preference
//                 */
//                if (prefValues != null) {
//                    for (int node = 0; node < prefNodes[dim].length; node++) {
//                        /*
//                         * TODO: explain
//                         */
//                        double prefSpread = (gamma[dim] / beta[dim]) * (prefValues[dim][node] / pWeightsSum[dim]);
//                        newWeights[dim][prefNodes[dim][node]] += prefSpread;
//                    }
//                }                
//                
//                /*
//                 * accumulate old weight, new weight and random surfer into
//                 * new weight
//                 */
//                // What for? These three vars are never used
//                double sumOld  = 0;
//                double sumNew  = 0;
//                double sumPref = 0;
//                for (int node = 0; node < weights[dim].length; node++) {
//                    
//                    double oldW = alpha[dim] * weights[dim][node];
//                    double newW = beta[dim]  * newWeights[dim][node];
//                    double prefW = gamma[dim] * pWeights[dim];
//                    
//                    sumOld  += oldW;
//                    sumNew  += newW;
//                    sumPref += prefW;
//                    
//                    newWeight = oldW + 
//                                newW + 
//                                prefW; 
//                    
//                    /*
//                     * compute weight change
//                     */
//                    delta += Math.abs(weights[dim][node] - newWeight);
//                    
//                    weights[dim][node] = newWeight;
//                }
//            }
//            
//            result.addError(delta);
//            
//        } // main loop
//        
////        Util.printWeightsSums(weights, "  post");
////        
////        for (int dim = 0; dim < weights.length; dim++) {
////            
////            /*
////             * spread preference
////             */
////            if (prefValues != null) {
////                for (int node = 0; node < prefNodes[dim].length; node++) {
////                    /*
////                     * TODO: explain
////                     */
////                    double prefSpread = (gamma[dim] / beta[dim]) * (prefValues[dim][node] / pWeightsSum[dim]); 
////                    System.out.println("ps: " + prefSpread + " = (" + gamma + " / " + beta + ") * (" + prefValues[dim][node] + " / " + pWeightsSum[dim] + ")");
////                }
////            }    
////        }
//        
//        result.setWeights(weights);
//        
//        return result;
//	}
//
//	@Override
//	public FolkRankResult computeFolkRank(FolkRankData facts, FolkRankPref pref) {
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//}
