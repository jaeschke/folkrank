package org.bibsonomy.folkrank;

import java.sql.SQLException;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.apache.log4j.Logger;
import org.bibsonomy.folkrank.data.FolkRankData;
import org.bibsonomy.folkrank.data.FolkRankParam;
import org.bibsonomy.folkrank.data.FolkRankPref;
import org.bibsonomy.folkrank.data.FolkRankResult;
import org.bibsonomy.folkrank.data.NodeMap;
import org.bibsonomy.folkrank.data.OutputData;
import org.bibsonomy.folkrank.io.DBJobQueue;
import org.bibsonomy.folkrank.io.DBJobQueue.RankingJob;
import org.bibsonomy.folkrank.process.HelperMethods;

/**
 * Thread which processes the JobQueue and computes the FolkRank.
 * 
 * @author:  rja
 * @version: $Id$
 * $Author$
 * 
 */
public class JobQueueProcessor implements Runnable {

	private static final Logger log = Logger.getLogger(JobQueueProcessor.class);

	private final NodeMap nodeMap;
	private final DBJobQueue queue;
	private final FolkRankData facts;
	private final FolkRankParam param;
	private final ConcurrentLinkedQueue<OutputData<String>> output;

	private final int k;
	private final int milliSecondsBetweenComputations;
	
	public JobQueueProcessor(final NodeMap nodeMap, FolkRankParam param, DBJobQueue queue, FolkRankData facts, int k, int milliSecondsBetweenComputations, ConcurrentLinkedQueue<OutputData<String>> output) {
		this.nodeMap = nodeMap;
		this.queue = queue;
		this.facts = facts;
		this.param = param;
		this.k = k;
		this.milliSecondsBetweenComputations = milliSecondsBetweenComputations;
		this.output = output;
	}

	public void run() {
		/* 
		 * every thread has its own object of FolkRank and FolkRankPref
		 */
		final FolkRankPref pref = new FolkRankPref(new double[] {1.0, 1.0, 1.0});
		final FolkRank folk = new FolkRank(param);

		/*
		 * create preference objects
		 */
		final int[][] prefItems = new int[facts.getDims()][];
		final double[][] prefValues = new double[facts.getDims()][];
		pref.setPreference(prefItems, prefValues);

		try {
			RankingJob job = queue.getNextJob();
			
			while (job != null) {
				log.debug("got job " + job.id + ": " + job.dim + "/" + job.item);
				
				/*
				 * process job
				 */

				/*
				 * get item id
				 * since we don't have a mapping string -> integer, we have to do a
				 * search over all strings to find the correct integer. TODO: 
				 * improve this!
				 */
				int itemId = -1;
				int itemDim = job.dim;
				boolean found = false;
				final String[] mapping = nodeMap.getMapping(itemDim);
				for (int item = 0; item < mapping.length; item++) {
					if (mapping[item].equals(job.item)) {
						itemId = item;
						found = true;
						break;
					}
				}
				if (!found) {
					log.fatal("could not find no. for item " + job.item + " in dim " + itemDim + ".");
					log.fatal("mapping[itemDim].length = " + mapping.length);
					/*
					 * delete job (otherwise we go into an infinite loop ...)
					 */
					queue.remove(job);
					return;
				}

				/*
				 * set preference
				 */
				for (int dim = 0; dim < facts.getDims(); dim++) {
					if (dim != itemDim) {
						/*
						 * give zero preference to each entry ...
						 */
						prefItems[dim] = new int[]{};
						prefValues[dim] = new double[]{};
					} else {
						/*
						 * ... and much preference to item for job
						 * (much = no. of items in this dimension)
						 */
						prefItems[dim] = new int[]{itemId};
						prefValues[dim] = new double[]{mapping.length};
					}
				}

				/*
				 * calculate ranking
				 */
				final FolkRankResult result = folk.computeFolkRank(facts, pref);
				/*
				 * save result
				 */
				output.offer(getTopK(job, result, pref)); 
			
				
				log.debug("finished job " + job.id + ": " + job.dim + "/" + job.item);
				
				/*
				 * wait some time
				 */
				try {
					Thread.sleep(milliSecondsBetweenComputations);
				} catch (InterruptedException e) {
					log.fatal(e);
				}
				
				/*
				 * get next job and mark it as active
				 */
				job = queue.getNextJob();

			}
		} catch (final SQLException e){
			log.fatal(e);
		}
	}

	/**
	 * Method to calculate the top k items and put them into a datastructure 
	 * such that the main thread can write them to the database.
	 * 
	 * @param job
	 * @param result
	 * @param pref
	 * @return
	 */
	private OutputData<String> getTopK(final RankingJob job, final FolkRankResult result, final FolkRankPref pref){
		/*
         * Since we support only ranking of ONE item at a time, 
         * we pick the first value.
         */
		double prefValue = pref.getPrefValues()[job.dim][0];

		final OutputData<String> resultData = new OutputData<String>();
		
		// add the values to the OutputData object
		resultData.setTopK(HelperMethods.getTopK(facts, result, k));

		resultData.setDate(facts.getDate()); 
		resultData.setAlpha(param.getAlpha());
		resultData.setBeta(param.getBeta());
		resultData.setGamma(param.getGamma());
		resultData.setDelta(result.getErrors().getLast());
		resultData.setIter(result.getErrors().size());
		resultData.setPrefDim(job.dim); // dimension of ranked item
		resultData.setPrefItem(job.item); // ranked item (String!)
		resultData.setPrefValue(prefValue); 
		resultData.setBasePref(pref.getBasePrefWeight()[job.dim]);

		return resultData;
	}

}

