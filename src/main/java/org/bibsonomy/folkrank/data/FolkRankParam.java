package org.bibsonomy.folkrank.data;

import org.bibsonomy.folkrank.strategy.WeightInitializationStrategy;
import org.bibsonomy.folkrank.strategy.ZeroTolerantSeparatedWeightInitializationStrategy;

/** The <code>FolkRankParam</code> class holds the parameters of the FolkRank
 * algorithm. It does not contain any data used for the computation.
 *   
 * @author rja
 */
public class FolkRankParam {

    /* These three parameters must sum up to one. They determine how much of the
     * old weight, the new weight (result from weight spreading) and the 
     * preference determines the new weight vector. 
     */
    private double alpha = 0.0; // old weight
    private double beta  = 0.7; // new weight
    private double gamma = 0.3; // preference
    
    
    /** Stopping criterion 1: if computation error is smaller than epsilon (or 
     * number of iterations larger than maxIter), then computation is stopped.
     */
    private double epsilon = 10e-6;
    /** Stopping criterion 2: if number of iterations is larger than maxIter (or
     * computation error smaller than epsilon), then computation is stopped.
     */
    private int maxIter = 10;
    
    /** Since there are several ways how preference and weight could be 
     * initialized, different strategies how to do this can be implemented and
     * given to the FolkRank. 
     */
    private WeightInitializationStrategy weightInitializationStrategy = new ZeroTolerantSeparatedWeightInitializationStrategy();
    
    public FolkRankParam() {
		// TODO Auto-generated constructor stub
	}
  
    public FolkRankParam(final double d) {
		setD(d);
	}

    public FolkRankParam(final double d, final double epsilon, final int maxIter) {
		setD(d);
		this.epsilon = epsilon;
		this.maxIter = maxIter;
	}
    
	/** Set the main parameters for the FolkRank computation. Alpha, beta and 
     * gamma determine how much of the old, new, and preference weight, resp.
     * is used to determine the new weight. 
     * 
     * If alpha + beta + gamma = 1 does not hold, the parameters are normalized
     * such that they add up to one. 
     *  
     * @param alpha - Factor to determine which fraction of the old weight flows
     * into the new weight. The default is 0.0.
     * @param beta - Factor to determine which fraction of the new weight from 
     * the weight spreading flows into the new weight. The default is 0.7.
     * @param gamma - Factor to determine which fraction of the preference 
     * weight flows into the new weight. The default is 0.3.
     */
    public void setAlphaBetaGamma (final double alpha, final double beta, final double gamma) {
        final double sum = alpha + beta + gamma;
        if (Math.abs(sum - 1.0) < 10e-6) {
            this.alpha = alpha;
            this.beta  = beta;
            this.gamma = gamma;
        } else {
            /*
             * parameters do not add up to 1 ... normalize them
             */
            this.alpha = alpha / sum;
            this.beta  = beta  / sum;
            this.gamma = gamma / sum;
        }
    }
    
    /**
     * The simplified version of FolkRank used the parameter d in the formula
     * 
     * dA^Tw + (1 - d)p 
     * 
     * instead of \alpha, \beta, \gamma. 
     * 
     * This is a convenience method which calls {@link #setAlphaBetaGamma(0, d, 1 - d)}. 
     * Thus, d must be between 0 and 1. 
     * 
     * @param d
     */
    public void setD(final double d) {
    	setAlphaBetaGamma(0, d, 1 - d);
    }
    
    /** Set the stopping conditions for the FolkRank computation. If either the 
     * number of iterations exceeds maxIter or the error drops below epsilon, 
     * the computation stops.
     *  
     * @param epsilon - The maximal error the computation should have before 
     * stopping. The default is 10e-6.
     * @param maxIter - The maximum number of iterations . The default is 10.
     */
    public void setStopCondition (double epsilon, int maxIter) {
        this.epsilon = epsilon;
        this.maxIter = maxIter;
    }
    
    /** Returns alpha.
     * 
     * @return The parameter alpha.
     */
    public double getAlpha() {
        return alpha;
    }
    /** Returns beta.
     * 
     * @return The parameter beta.
     */
    public double getBeta() {
        return beta;
    }
    /** Returns gamma.
     * 
     * @return The parameter gamma.
     */
    public double getGamma() {
        return gamma;
    }

    
    /** Returns epsilon, the error bound for the computation.
     * 
     * @return The error bound for the computation.
     */
    public double getEpsilon() {
        return epsilon;
    }
    /** Returns the maximal number of iterations.
     * 
     * @return The maximal number of iterations.
     */
    public int getMaxIter() {
        return maxIter;
    }
    
    /** Returns the weight initialization strategy.
     * 
     * @return The weight initialization strategy.
     */
    public WeightInitializationStrategy getWeightInitializationStrategy() {
        return weightInitializationStrategy;
    }
    /** Set the weight initialization strategy.
     * @param weightStrategy - The weight initialization strategy.
     */
    public void setWeightInitializationStrategy(WeightInitializationStrategy weightStrategy) {
        this.weightInitializationStrategy = weightStrategy;
    }

}
