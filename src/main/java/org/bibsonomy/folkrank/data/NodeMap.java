package org.bibsonomy.folkrank.data;

import java.util.Map;

/**
 * @author rja
 * @version $Id$
 */
public interface NodeMap {

	/** Adds for this dimension the mapping of key to value to the list of 
	 * mappings. 
	 * 
	 * @param dimension - the dimension the mapping belongs to.
	 * @param key - the key of the mapping.
	 * @param value - the value of the mapping.
	 */
	public abstract void addMapping(final int dimension, final int key,	final String value);

	/** Stores the inverse mapping.
	 * @param dimension
	 * @param mapping 
	 */
	public abstract void addInverseMapping(final int dimension,	final Map<String, Integer> mapping);

	/** Returns the mapping (integer) for value <code>key</code>.
	 * @param dimension
	 * @param key
	 * @return
	 */
	public abstract Integer getInverseMapping(final int dimension, final String key);

	public abstract String getMapping(final int dimension, final int value);

	public abstract String[] getMapping(final int dimension);

}