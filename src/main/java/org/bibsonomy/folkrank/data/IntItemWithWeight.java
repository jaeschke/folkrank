package org.bibsonomy.folkrank.data;


/**
 * 
 * @author:  rja
 * @version: $Id$
 * $Author$
 * 
 */
/** Internal class which is used to represent the top-k items together with
 * their weight in a set when finding them.
 * 
 * @author rja
 */
public class IntItemWithWeight implements Comparable<IntItemWithWeight> {
    /**
     * An int which represents the item.
     */
    public int item;
    /**
     * The weight of the item.
     */
    public double weight;
    /** The only available constructor. 
     *  
     * @param item - a string which represents the item.
     * @param weight - the weight of the item.
     */
    public IntItemWithWeight(int item, double weight) {
        super();
        this.item = item;
        this.weight = weight;
    }
    /**
     * Disabled default constructor. 
     */
    private IntItemWithWeight () {
        /*
         * do nothing, since this constructor is not useable.
         */ 
    }

    /** Returns the item.
     * @return - the item.
     */
    public int getItem() {
        return item;
    }
    /** Returns the weight of the item.
     * @return - the weight of the item.
     */
    public double getWeight() {
        return weight;
    }

    /**
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (! (obj instanceof ItemWithWeight)) {
            return false;
        }
        return equals(obj);
    }

    /** Two items are equal if there string representations are equal. This 
     * is true, since only items of the same dimension should be compared.
     * 
     * @param other - the item to compare with this item.
     * @return - true if this.item == other.item.
     */
    private boolean equals (IntItemWithWeight other) {
        return this.item == other.item;
    }

    /** Compares two items by their weight. 
     * @param o - the other item to compare with this item.
     * @return - 0 if they're equal, -1/+1 otherwise.
     * 
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    public int compareTo(IntItemWithWeight o) {
        if (o == null) { throw new NullPointerException(); }
        int sgn = (int) Math.signum(o.weight - this.weight);
        if (sgn != 0) {
            return sgn;
        } else {
            return o.item - this.item;
        }
    }
    /**
     * @see java.lang.Object#hashCode()
     */
    @Override
	public int hashCode () {
        return item;
    }
    
    @Override
    public String toString() {
    	return item + ": " + weight;
    }
    
}


