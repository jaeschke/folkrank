package org.bibsonomy.folkrank.data;

import java.util.LinkedList;

public interface FolkRankResult {

    /** Returns the weight vectors for each dimension.
     * 
     * @return An array of weight vectors - one vector for each dimension.
     */
    public double[][] getWeights();

    /** Sets the weight vectors.
     * 
     * @param weights - An array of weight vectors - one for each dimension.
     */
    public void setWeights(double[][] weights);

    /** Returns a list of errors from the computation. For each iteration one
     * error value.
     *  
     * @return A list of double values. For each iteration one value. 
     */
    public LinkedList<Double> getErrors();

    /** Add an error value to the list of error values.
     * 
     * @param error - An error value.
     */
    public void addError(double error);

    
    /** Returns the weight vectors of the adapted PageRank for each dimension.
     * 
     * @return An array of weight vectors - one vector for each dimension.
     */
    public double[][] getAPRWeights();

    /** Sets the weight vectors for the adapted PageRank.
     * 
     * @param weights - An array of weight vectors - one for each dimension.
     */
    public void setAPRWeights(double[][] weights);
    
}