package org.bibsonomy.folkrank.data;

import java.util.LinkedList;

/** This class stores the result of the FolkRank computation, that is basically
 * a weight vector for each dimension.
 * 
 * @author rja
 */
public class StandardFolkRankResult implements FolkRankResult {

    private double[][] weights;
    private LinkedList<Double> errors = new LinkedList<Double>();
    
    /**
     * @see org.bibsonomy.folkrank.data.FolkRankResult#getWeights()
     */
    public double[][] getWeights() {
        return weights;
    }
    /**
     * @see org.bibsonomy.folkrank.data.FolkRankResult#setWeights(double[][])
     */
    public void setWeights(double[][] weights) {
        this.weights = weights;
    }

    /**
     * @see org.bibsonomy.folkrank.data.FolkRankResult#getErrors()
     */
    public LinkedList<Double> getErrors() {
        return errors;
    }
    /**
     * @see org.bibsonomy.folkrank.data.FolkRankResult#addError(double)
     */
    public void addError(double error) {
        this.errors.add(error);
    }
    
    /*
     * not supported by this implementation
     */
    public double[][] getAPRWeights() {
        return null;
    }
    public void setAPRWeights(double[][] weights) {
    }
    
}
