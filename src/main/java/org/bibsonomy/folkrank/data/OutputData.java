package org.bibsonomy.folkrank.data;

import java.util.Date;
import java.util.SortedSet;

public class OutputData<V> {
	private Date date;
	private double alpha; 
	private double beta;
	private double gamma; 
	private double delta; 
	private int iter; 
	private int prefDim; 
	private String prefItem; 
	private double prefValue; 
	private double basePref;
	
	private SortedSet<ItemWithWeight<V>>[] topK;
    
	public SortedSet<ItemWithWeight<V>>[] getTopK() {
		return topK;
	}
	public void setTopK(SortedSet<ItemWithWeight<V>>[] sortedSets) {
		this.topK = sortedSets;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public double getAlpha() {
		return alpha;
	}
	public void setAlpha(double alpha) {
		this.alpha = alpha;
	}
	public double getBeta() {
		return beta;
	}
	public void setBeta(double beta) {
		this.beta = beta;
	}
	public double getGamma() {
		return gamma;
	}
	public void setGamma(double gamma) {
		this.gamma = gamma;
	}
	public double getDelta() {
		return delta;
	}
	public void setDelta(double delta) {
		this.delta = delta;
	}
	public int getIter() {
		return iter;
	}
	public void setIter(int iter) {
		this.iter = iter;
	}
	public int getPrefDim() {
		return prefDim;
	}
	public void setPrefDim(int prefDim) {
		this.prefDim = prefDim;
	}
	public String getPrefItem() {
		return prefItem;
	}
	public void setPrefItem(String prefItem) {
		this.prefItem = prefItem;
	}
	public double getPrefValue() {
		return prefValue;
	}
	public void setPrefValue(double prefValue) {
		this.prefValue = prefValue;
	}
	public double getBasePref() {
		return basePref;
	}
	public void setBasePref(double basePref) {
		this.basePref = basePref;
	}
}
