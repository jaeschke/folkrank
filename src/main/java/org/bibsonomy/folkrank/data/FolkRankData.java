package org.bibsonomy.folkrank.data;

import java.util.Arrays;
import java.util.Date;

/** This class holds the data the FolkRank computation is based on. In 
 * particular, it contains a list of all facts, a mapping of internal ids to 
 * strings and counts of the item occurrences in the facts. 
 * 
 * @author rja
 */
public class FolkRankData {

	public final static int MISSING_VALUE = -1;
	/**
	 * the tas facts[i] = the i'th tas
	 */
    private final int[][] facts;
    /**
     * the counts for each item (used to normalize to 1 in the matrix), i.e. counts[d][i] = how often does the i-th item of the d-th dimension occur
     * actually each item is counted (noOfDimensions -1) times, since each hyperedge is interpreted as several edges.
     * Ie each node is connected with a regular edge to each of the other nodes of the hyperedges (= noOfDimensions-1 such nodes) 
     */
    private final int[][] counts;
    private final Date date;
    private final NodeMap nodeMap;
    private final int dims;
    
    /** Constructs a new fact array. The size of numberOfItemsPerDimensions
     * defines how many dimensions will be used.
     * 
     * @param noOfFacts - the number of facts.
     * @param noOfItemsPerDimension - the number of items for each dimension. 
     */
    public FolkRankData (final int noOfFacts, final int[] noOfItemsPerDimension, final NodeMap nodeMap) {
    	this.dims    = noOfItemsPerDimension.length;
        this.facts   = new int[noOfFacts][];
        this.nodeMap = nodeMap;
        this.counts  = new int[dims][];
        this.date    = new Date(); // TODO: it might be useful to set this from outside 
        
        for (int dim = 0; dim < this.dims; dim++) {
            /*
             * initialize counts with zero 
             */
        	this.counts[dim] = new int[noOfItemsPerDimension[dim]];
            Arrays.fill(this.counts[dim], 0);
        }
    }
    


    /** Sets the fact with the specified id.
     * 
     * @param factId - position in fact array.
     * @param fact - value to be written into fact array.
     */
    public void setFact (int factId, int[] fact) {
        this.facts[factId] = fact;
        /*
         * Count occurences of items in this fact. Each item is counted several times,
         * since one hyperedge is interpreted as (noOfDimensions-1)*noOfDimensions directed 
         * edges.
         */
        for (int dim = 0; dim < fact.length; dim++) {
        	if (fact[dim] != MISSING_VALUE) {
        		this.counts[dim][fact[dim]] += fact.length-1-this.getNumberOfMissingValues(fact); // each counted noOfDimensions -1 times
        	}
        }
    }
    
    private int getNumberOfMissingValues(int[] fact) {
    	int count = 0;
    	for (int dim = 0; dim<fact.length; dim++) {
    		if (fact[dim] == MISSING_VALUE) {
    			count++;
    			System.out.println("Write found missing value");
    		}
    	}
    	return count;
    }
    
    /** Returns the fact array.
     * 
     * @return An array of facts.
     */
    public int[][] getFacts() {
        return facts;
    }

    /** Returns the counts for each item in each dimension.
     * 
     * @return An array of counts.
     */
    public int[][] getCounts() {
        return counts;
    }
    
    
    /** Returns the date of the current dataset.
     * @return The date of the dataset. 
     */
    public Date getDate() {
        return date;
    }

	public NodeMap getNodeMap() {
		return nodeMap;
	}

	/**
	 * @return the number of dimensions
	 */
	public int getDims() {
		return dims;
	}
    
}
