package org.bibsonomy.folkrank.data;

/** Internal class which is used to represent the top-k items together with
 * their weight in a set when finding them.
 * 
 * @author rja
 */
public class ItemWithWeight<V> implements Comparable<ItemWithWeight<V>> {
    /**
     * A string which represents the item.
     */
    public V item;
    /**
     * The weight of the item.
     */
    public double weight;
    /** The only available constructor. 
     *  
     * @param item - a string which represents the item.
     * @param weight - the weight of the item.
     */
    public ItemWithWeight(V item, double weight) {
        super();
        this.item = item;
        this.weight = weight;
    }
    /**
     * Disabled default constructor. 
     */
    private ItemWithWeight () {
       /*
        * do nothing, since this constructor is not useable.
        */ 
    }

    /** Returns the item.
     * @return - the item.
     */
    public V getItem() {
        return item;
    }
    /** Returns the weight of the item.
     * @return - the weight of the item.
     */
    public double getWeight() {
        return weight;
    }
    
    /**
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (! (obj instanceof ItemWithWeight)) {
            return false;
        }
        return equals((ItemWithWeight) obj);
    }
    
    /** Two items are equal if there string representations are equal. This 
     * is true, since only items of the same dimension should be compared.
     * 
     * @param other - the item to compare with this item.
     * @return - true if this.item == other.item.
     */
    private boolean equals (ItemWithWeight<V> other) {
        return this.item.equals(other.item);
    }
    
    /** Compares two items by their weight. 
     * @param o - the other item to compare with this item.
     * @return - 0 if they're equal, -1/+1 otherwise.
     * 
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    public int compareTo(ItemWithWeight<V> o) {
        if (o == null) { throw new NullPointerException(); }
        int sgn = (int) Math.signum(o.weight - this.weight);
        if (sgn != 0) {
            return sgn;
        } else {
        	if (o.item instanceof Comparable && this.item instanceof Comparable) {
        		return ((Comparable) o.item).compareTo(this.item);
        	} else {
        		// we can't distinguish them then
        		return 0;
        	}
        }
    }
    /**
     * @see java.lang.Object#hashCode()
     */
    @Override
	public int hashCode () {
        return item.hashCode();
    }
    
    /** Returns a String representation of this item with weight in the form
     * <code>item(weight)</code>.
     * 
     * @see java.lang.Object#toString()
     */
    @Override
	public String toString() {
    	return item + "(" + weight + ")";
    }
    
    
    
}