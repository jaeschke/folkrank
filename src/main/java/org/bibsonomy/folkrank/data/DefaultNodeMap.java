package org.bibsonomy.folkrank.data;

import java.util.Map;
import java.util.Vector;

/**
 * @author rja
 * @version $Id$
 */
public class DefaultNodeMap implements NodeMap {

	private final int[] noOfItemsPerDimension;
	private final Vector<String[]> keyToValueMapping;
	private final Vector<Map<String, Integer>> valueToKeyMapping;

	public DefaultNodeMap(final int[] noOfItemsPerDimension) {
		this.noOfItemsPerDimension = noOfItemsPerDimension;
		this.keyToValueMapping = new Vector<String[]>(noOfItemsPerDimension.length);
		for (int dim = 0; dim < noOfItemsPerDimension.length; dim++) {
			final String[] element = new String[this.noOfItemsPerDimension[dim]];
			this.keyToValueMapping.add(dim, element);
		}
		this.valueToKeyMapping = new Vector<Map<String, Integer>>(noOfItemsPerDimension.length);
	}

	/* (non-Javadoc)
	 * @see org.bibsonomy.folkrank.data.NodeMap#addMapping(int, int, java.lang.String)
	 */
	public void addMapping(final int dimension, final int key, final String value) {
		this.keyToValueMapping.get(dimension)[key] = value;
	}



	/* (non-Javadoc)
	 * @see org.bibsonomy.folkrank.data.NodeMap#addInverseMapping(int, java.util.Map)
	 */
	public void addInverseMapping(final int dimension, final Map<String, Integer> mapping) {
		this.valueToKeyMapping.add(dimension, mapping);
	}

	/* (non-Javadoc)
	 * @see org.bibsonomy.folkrank.data.NodeMap#getInverseMapping(int, java.lang.String)
	 */
	public Integer getInverseMapping (final int dimension, final String key) {
		return this.valueToKeyMapping.get(dimension).get(key);
	}

	/* (non-Javadoc)
	 * @see org.bibsonomy.folkrank.data.NodeMap#getMapping(int, int)
	 */
	public String getMapping(final int dimension, final int value) {
		return keyToValueMapping.get(dimension)[value];
	}

	/* (non-Javadoc)
	 * @see org.bibsonomy.folkrank.data.NodeMap#getMapping(int)
	 */
	public String[] getMapping(final int dimension) {
		return keyToValueMapping.get(dimension);
	}
}
