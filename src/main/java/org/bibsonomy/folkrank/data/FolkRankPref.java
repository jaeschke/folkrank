package org.bibsonomy.folkrank.data;

/** The <code>FolkRankPref</code> class holds the preference weights for the 
 * FolkRank algorithm. 
 *   
 * @author rja
 */
public class FolkRankPref {

    /** For each dimension a list of items which will get extra preference 
     * weight. 
     */
    private int[][] prefItems       = null;
    /** For each dimension a list of preference weights which will be given to 
     * the items in prefItems. 
     */
    private double[][] prefValues   = null;
    /** For each dimension the weight each item gets. 
     *
     */
    private double[] basePrefWeight = null;

    /** Sets for each dimension the preference weight <em>each node</em> gets.      
     * @param basePrefWeight - For each dimension the weight <em>each item</em>
     * gets.
     */
    public FolkRankPref (double[] basePrefWeight) {
        this.basePrefWeight = basePrefWeight;
    }
    
    /** Set the items which will get extra preference and the corresponding 
     * preferene values.
     * 
     * @param prefItems - For each dimension an array of items which will get
     * extra preference.
     * @param prefValues - For each dimension an array of preference values 
     * which the corresponding item from prefItems will get.
     */
    public synchronized void setPreference (int[][] prefItems, double[][] prefValues) {
        this.prefItems  = prefItems;
        this.prefValues = prefValues;
    }
    
    /** Sets for each dimension the preference weight <em>each node</em> gets.      
     * @param basePrefWeight - For each dimension the weight <em>each item</em>
     * gets.
     */
    /*private void setBasePrefWeight(double[] basePrefWeight) {
        this.basePrefWeight = basePrefWeight;
    }*/

    /** Returns the array which contains for each dimension the items which 
     * should get extra preference weight.
     * 
     * @return An array of items which should get extra preference weight.
     */
    public int[][] getPrefItems() {
        return prefItems;
    }
    /** Returns an array which contains for each dimension the values the items
     * in prefItems get as extra preference.
     * 
     * @return An array of preference values.
     */
    public double[][] getPrefValues() {
        return prefValues;
    }

    /** Returns the preference weight for each dimension which 
     * <em>each item</em> gets.
     * 
     * @return An array of preference weights.
     */
    public double[] getBasePrefWeight() {
        return basePrefWeight;
    }
}
