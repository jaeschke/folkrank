package org.bibsonomy.folkrank.data;


/**
 * Stores the weights for the adapted PageRank.
 * 
 * @author rja
 *
 */
public class APRFolkRankResult extends StandardFolkRankResult implements FolkRankResult {
    
    private double[][] aprWeights;
    
    @Override
	public double[][] getAPRWeights() {
        return aprWeights;
    }
    @Override
	public void setAPRWeights(double[][] weights) {
        // copy weights, because array is overwritten
        this.aprWeights = new double[weights.length][];
        for (int dim = 0; dim < weights.length; dim++) {
            this.aprWeights[dim] = weights[dim].clone();
        }
    }

}
