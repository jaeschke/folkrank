package org.bibsonomy.folkrank;

import java.util.List;
import java.util.Properties;
import java.util.SortedSet;

import org.apache.log4j.Logger;
import org.bibsonomy.folkrank.data.FolkRankData;
import org.bibsonomy.folkrank.data.FolkRankParam;
import org.bibsonomy.folkrank.data.FolkRankPref;
import org.bibsonomy.folkrank.data.FolkRankResult;
import org.bibsonomy.folkrank.data.ItemWithWeight;
import org.bibsonomy.folkrank.data.NodeMap;
import org.bibsonomy.folkrank.io.FactReader;
import org.bibsonomy.folkrank.io.TasListFactReader;
import org.bibsonomy.folkrank.process.FactReaderFactPreprocessor;
import org.bibsonomy.folkrank.process.HelperMethods;
import org.bibsonomy.folkrank.strategy.SeparatedWeightInitializationStrategy;
import org.bibsonomy.folkrank.strategy.WeightInitializationStrategy;

/** Wrapper around FolkRank which holds the data as a static attribute and 
 * allows several threads to run FolkRank upon the same data.
 * 
 * @author rja
 */
public class FolkRankInMemoryMultiThreading implements Runnable {

	private static final Logger log = Logger.getLogger(FolkRankInMemoryMultiThreading.class);

	public enum ItemType {
		USER(0), TAG(1), RESOURCE(2);

		private int id;
		public static int noOfDimensions = 3;

		private ItemType (int i) {
			this.id = i;
		}
	}

	/*
	 * data is static
	 */
	 private static FolkRankData facts;
	 private static FolkRankParam param;
	 private static int topK = 20;

	 /*
	  * each instance has an algorithm and preferences
	  */
	 private final FolkRank folk;
	 private FolkRankResult result;
	 private FolkRankPref pref;

	 public void setPreference (final ItemType itemType, final List<String> items) {
		 /*
		  * configure preferences
		  */
		 final int[][] prefItems = new int[ItemType.noOfDimensions][];
		 final double[][] prefValues = new double[prefItems.length][];
		 final NodeMap nodeMap = facts.getNodeMap();
		 for (int dim = 0; dim < prefItems.length; dim++) {

			 if (dim == itemType.id) {
				 /*
				  * set preference
				  */
				 prefItems[dim]  = new int[items.size()];
				 prefValues[dim] = new double[items.size()]; 
				 for (int i = 0; i < prefItems[dim].length; i++) {
					 prefItems[dim][i]  = nodeMap.getInverseMapping(dim, items.get(i));
					 prefValues[dim][i] = facts.getFacts()[dim].length / items.size();
				 }
			 } else {
				 prefItems[dim] = new int[]{};
				 prefValues[dim] = new double[]{};
			 }

		 }
		 pref = new FolkRankPref(new double[] {1.0, 1.0, 1.0}); // prefs for each item 
		 pref.setPreference(prefItems, prefValues);
	 }

	 public FolkRankInMemoryMultiThreading (final ItemType itemType, final List<String> items) {
		 this.setPreference(itemType, items);
		 /*
		  * instantiating folkrank
		  */
		 log.debug("instantiating folkrank");
		 folk = new FolkRank(param);
	 }

	 public FolkRankInMemoryMultiThreading () {
		 /*
		  * instantiating folkrank
		  */
		 log.debug("instantiating folkrank");
		 folk = new FolkRank(param);
	 }


	 /** Updates the data the folkrank computation is based on.
	  * Waits for all threads to end before updating the data.
	  * 
	  * @param tas - list of string arrays. Each array represents a TAS {user, tag, resource}.
	  * 
	  */
	 public static void updateData (final List<String[]> tas) {
		 log.info("Updating data (" + tas.size() + " triples)");
		 /*
		  * preprocess data
		  */
		 final FactReader<String> factReader = new TasListFactReader<String>(tas);
		 final FactReaderFactPreprocessor prepro = new FactReaderFactPreprocessor(factReader, true);
		 prepro.process();
		 /*
		  * update data TODO: take care of threads!
		  */
		 if (facts != null) { 
			 synchronized (facts) {
				 facts = prepro.getFolkRankData();    
			 }
		 } else {
			 facts = prepro.getFolkRankData();
		 }
	 }

	 /** Configures the class to run folkrank (i.e., sets strategy and other
	  * parameters).
	  * 
	  * @param prop
	  */
	 public static void configure(final Properties prop) {

		 /*
		  * configure FolkRrank
		  */
		 log.debug("configuring folkrank parameters");
		 param = new FolkRankParam();
		 param.setAlphaBetaGamma(getDouble(prop, "folkrank.alpha"), getDouble(prop, "folkrank.beta"), getDouble(prop, "folkrank.gamma"));
		 param.setStopCondition(getDouble(prop, "folkrank.epsilon"), getInt(prop, "folkrank.maxIter"));


		 log.debug("initializing weight initializing strategy");
		 WeightInitializationStrategy strategy = null;       
		 try {
			 strategy = (WeightInitializationStrategy) Class.forName(prop.getProperty("folkrank.strategy")).newInstance();
		 } catch (Exception e) {
			 /*
			  * implement proper exception handling
			  */
			 log.warn("initialization of weight initialization strategy failed, " +
					 "setting " + 
					 SeparatedWeightInitializationStrategy.class.getName() + 
			 " as default.");
			 strategy = new SeparatedWeightInitializationStrategy();
		 }
		 param.setWeightInitializationStrategy(strategy);

		 /*
		  * initialize number of top-k items to compute.
		  */
		 try {
			 topK = Integer.parseInt(prop.getProperty("folkrank.k").trim());
		 } catch (NumberFormatException e) {
			 log.fatal("could not get k (number of rankings to save): " + e);
		 }
	 }


	 /** Actually runs the FolkRank calculation.
	  * @see java.lang.Runnable#run()
	  */
	 public void run() {
		 /*
		  * calculate ranking
		  */
		 result = folk.computeFolkRank(facts, pref);
	 }

	 public SortedSet<ItemWithWeight<String>>[] getResult () {
		 return HelperMethods.getTopK(facts, result, topK);
	 }



	 /** Extract a double property.
	  * @param propName
	  * @return A double value contained in propName.
	  */
	 private static double getDouble(final Properties prop, final String propName) {
		 return Double.parseDouble(prop.getProperty(propName).trim());
	 }

	 /** Extract an integer property.
	  * @param propName
	  * @return An integer value contained in propName.
	  */
	 private static int getInt(final Properties prop, final String propName) {
		 return Integer.parseInt(prop.getProperty(propName).trim());
	 }

}
