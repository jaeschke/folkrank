package org.bibsonomy.folkrank;

import java.util.Arrays;

import org.apache.derby.tools.sysinfo;
import org.bibsonomy.folkrank.data.APRFolkRankResult;
import org.bibsonomy.folkrank.data.FolkRankData;
import org.bibsonomy.folkrank.data.FolkRankParam;
import org.bibsonomy.folkrank.data.FolkRankPref;
import org.bibsonomy.folkrank.data.FolkRankResult;

public class FolkRank {

    private FolkRankResult baselineResult = null;
    private FolkRankParam param = null;
    private FolkRankParam baseParam = null;
    
    public FolkRank (final FolkRankParam baseParam, final FolkRankParam param) {
    	this.baseParam = baseParam;
    	this.param = param;
    }
    
    public FolkRank (final FolkRankParam param) {
        this(param, param);
    }
    
    /**
     * Sets baselineResult and all params to null
     */
    public void resetBaseline () {
        baselineResult = null;
        param = null;
        baseParam = null;
    }
    
    public synchronized FolkRankResult computeFolkRank (final FolkRankData facts, final FolkRankPref pref) {
        /*
         * initialize baseline, if necessary
         */
        if (this.baselineResult == null) {
            this.baselineResult = compute(facts, new FolkRankPref(pref.getBasePrefWeight()), this.baseParam);
        }
        /*
         * compute weights with preference
         */
        final FolkRankResult preferenceResult = compute(facts, pref, this.param);
        /*
         * compute difference weights
         */
        final double[][] baselineWeights = this.baselineResult.getWeights();
        final double[][] preferenceWeights = preferenceResult.getWeights();
        /*
         * for evaluation: remember weights of adapted PageRank
         */
        preferenceResult.setAPRWeights(preferenceWeights);  

        for (int dim = 0; dim < baselineWeights.length; dim++) {
            for (int item = 0; item < baselineWeights[dim].length; item++) {
                preferenceWeights[dim][item] -= baselineWeights[dim][item];
            }
        }
        return preferenceResult;
    }
    
    public synchronized FolkRankResult computeFolkRank (final FolkRankData facts, final FolkRankPref w0Pref, final FolkRankPref pref) {
        /*
         * initialize baseline, if necessary
         */
        if (this.baselineResult == null) {
            this.baselineResult = compute(facts, w0Pref, this.baseParam);
        }
        /*
         * compute weights with preference
         */
        final FolkRankResult preferenceResult = compute(facts, pref, this.param);
        /*
         * compute difference weights
         */
        final double[][] baselineWeights = this.baselineResult.getWeights();
        final double[][] preferenceWeights = preferenceResult.getWeights();
        /*
         * for evaluation: remember weights of adapted PageRank
         */
        preferenceResult.setAPRWeights(preferenceWeights);  

        for (int dim = 0; dim < baselineWeights.length; dim++) {
            for (int item = 0; item < baselineWeights[dim].length; item++) {
                preferenceWeights[dim][item] -= baselineWeights[dim][item];
            }
        }
        return preferenceResult;
    }
    
    /**
     * Computes the PageRank using the param given in the constructor.
     * 
     * @param factsData
     * @param pref
     * @return
     */
    public synchronized FolkRankResult compute (final FolkRankData factsData, final FolkRankPref pref) {
    	return compute(factsData, pref, this.param);
    }
    
    /**
     * Computes the PageRank with the given param.
     * 
     * @param factsData
     * @param pref
     * @param param
     * @return
     */
    private synchronized FolkRankResult compute (final FolkRankData factsData, final FolkRankPref pref, final FolkRankParam param) {

        /*
         * input data
         */
        final int[][] facts      = factsData.getFacts();
        final int[][] counts     = factsData.getCounts();

        /*
         * weight vectors
         */
        final double[][] weights    = new double[counts.length][]; // weight 
        final double[][] newWeights = new double[counts.length][]; // temporary weight during the iteration step
        final double[][] spread     = new double[counts.length][]; // temporary array that contains the weight, a node spreads in ech step of the iteration

        /**
         * random surfer weights for each dimension
         */
        final double[] pWeights     = new double[counts.length]; 
        /**
         * sum of random surfer + preference weights = sum of all entries in p for one dimension 
         */
        final double[] pWeightsSum  = new double[counts.length]; 

        /**
         * nodes with preference
         */
        final int[][] prefNodes     = pref.getPrefItems();
        /**
         * preference values for these nodes
         */
        final double[][] prefValues = pref.getPrefValues();
        
        /*
         * These vectors are used to control the final weight computation:
         * alpha * oldWeight  +  beta * newWeight  +  gamma * pref
         * with alpha + beta + gamma = 1 
         */
        final double alpha[] = new double[counts.length]; Arrays.fill(alpha, param.getAlpha());
        final double beta [] = new double[counts.length]; Arrays.fill(beta,  param.getBeta());
        final double gamma[] = new double[counts.length]; Arrays.fill(gamma, param.getGamma());
        
        /*
         * output data
         */
        //FolkRankResult result = new StandardFolkRankResult();
        final FolkRankResult result = new APRFolkRankResult(); // TODO: choose correct one
        
        /*
         * allocate memory for weight vectors
         */
        for (int dim = 0; dim < weights.length; dim++) {
            newWeights[dim] = new double[counts[dim].length];
            spread[dim]     = new double[counts[dim].length];
            weights[dim]    = new double[counts[dim].length];
        }
        
        /*
         * initialize weights, random surfer (pWeights), preference sums from the given preference pref
         */
        
        param.getWeightInitializationStrategy().initalizeWeights(pref, weights, pWeights, pWeightsSum, counts);

        // depending on the strategy it might be necessary to adapt alpha beta and gamma dimension wise
        param.getWeightInitializationStrategy().adaptAlphaBetaGamma(alpha, beta, gamma, prefNodes, pWeights);
       
//        HelperMethods.printWeightsSums(weights, "  pre ");
        
        /* ********************************************************************
         * main loop
         * ********************************************************************/
        int iter = 0;
        double delta = Double.MAX_VALUE;
        while (iter < param.getMaxIter() && delta > param.getEpsilon()) {
            iter++;

            /*
             * initalize new weights with zero
             */
            for (double[] newWeightsDim:newWeights) {
                Arrays.fill(newWeightsDim, 0.0);
            }

            /*
             * initalize spread
             */
            for (int dim = 0; dim < spread.length; dim++) {
                for (int node = 0; node < spread[dim].length; node++) {
                	if (counts[dim][node]>0) {
                		spread[dim][node] = weights[dim][node] / counts[dim][node];
                	} else {
                		spread[dim][node] = 0;
                	}
                }
            }

            /*
             * spread weight (A*w)
             */
            for (int fact[]: facts) {
                for (int dim = 0; dim < newWeights.length; dim++) {
                	// missing values can spread no weight
                	if (fact[dim] == FolkRankData.MISSING_VALUE) continue;
                    /*
                     * newWeights[dim][fact[dim]] gets weight from all other
                     * nodes at this hyperedge
                     */
                    for (int dimAdd = 0; dimAdd < newWeights.length; dimAdd++) {
                    	/*
                    	 * get weight from surrounding nodes (do not spread to self, and to missing values)
                    	 */
                    	if (dim == dimAdd || fact[dimAdd] == FolkRankData.MISSING_VALUE) continue;
                    	newWeights[dim][fact[dim]] += spread[dimAdd][fact[dimAdd]];
                    }
                }
            }

            /*
             * add Preference (p)
             * currently: newWeight = A* weight
             * goal: newWeight[d][i] = alpha*weight[d][i] + beta*newWeight[d][i] + gamma*( basepref[d] + individualPref[d][i]/normFactor[d] )
             * trick: newWeight[d][i] = alpha*weight[d][i] + beta*( newWeight[d][i] + gamma/beta*individualPref[d][i]/normFactor[d] ) + \gamma*basepref[d]
             * therefore we first calculate                         newWeight[d][i] + gamma/beta*individualPref[d][i]/normFactor[d]
             * for items that have no extra preference, this is simply newWeight[d][i]
             * this allows us to use the prefValues directly (iteration through the prefValues) instead of asking for each Item whether it is a prefItem or not 
             */
            delta = 0.0;
            double newWeight;
            for (int dim = 0; dim < weights.length; dim++) {
                
                /*
                 * spread preference
                 */
                if (prefValues != null) {
                    for (int node = 0; node < prefNodes[dim].length; node++) {
                        /*
                         * newWeight[d][i] + gamma/beta*individualPref[d][i]/normFactor[d]
                         */
                        double prefSpread = (gamma[dim] / beta[dim]) * (prefValues[dim][node] / pWeightsSum[dim]);
                        newWeights[dim][prefNodes[dim][node]] += prefSpread;
                    }
                }                
                
                /*
                 * accumulate old weight, new weight and random surfer into
                 * new weight
                 */
                double sumOld  = 0;
                double sumNew  = 0;
                double sumPref = 0;
                for (int node = 0; node < weights[dim].length; node++) {
                    
                    double oldW = alpha[dim] * weights[dim][node];
                    double newW = beta[dim]  * newWeights[dim][node];
                    double prefW;
                    if (counts[dim][node]>0) {
                    	prefW = gamma[dim] * pWeights[dim];
                    } else {
                    	prefW = 0;
                    }
                    
                    sumOld  += oldW;
                    sumNew  += newW;
                    sumPref += prefW;
                    
                    newWeight = oldW + 
                                newW + 
                                prefW; 
                    
                    /*
                     * compute weight change
                     */
                    delta += Math.abs(weights[dim][node] - newWeight);
                    
                    weights[dim][node] = newWeight;
                }
            }
//            System.out.println(delta +" "+ param.getEpsilon());
//            System.out.println(iter + " "+ param.getMaxIter());
           
            result.addError(delta);
            
        } // main loop
        
//        HelperMethods.printWeightsSums(weights, "  post");
//        
//        for (int dim = 0; dim < weights.length; dim++) {
//            
//            /*
//             * spread preference
//             */
//            if (prefValues != null) {
//                for (int node = 0; node < prefNodes[dim].length; node++) {
//                    /*
//                     * TODO: explain
//                     */
//                    double prefSpread = (gamma[dim] / beta[dim]) * (prefValues[dim][node] / pWeightsSum[dim]); 
//                    System.out.println("ps: " + prefSpread + " = (" + gamma + " / " + beta + ") * (" + prefValues[dim][node] + " / " + pWeightsSum[dim] + ")");
//                }
//            }    
//        }
        
        result.setWeights(weights);
        
        return result;
    }


	public FolkRankParam getParam() {
		return param;
	}

	public void setParam(FolkRankParam param) {
		this.param = param;
	}

	public FolkRankResult getBaselineResult() {
		return baselineResult;
	}

	public void setBaselineResult(FolkRankResult baselineResult) {
		this.baselineResult = baselineResult;
	}

}

