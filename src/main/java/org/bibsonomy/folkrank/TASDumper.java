package org.bibsonomy.folkrank;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Properties;

import org.bibsonomy.folkrank.data.FolkRankData;
import org.bibsonomy.folkrank.io.DBFactReader;
import org.bibsonomy.folkrank.io.FactReader;
import org.bibsonomy.folkrank.io.FactReadingException;
import org.bibsonomy.folkrank.io.FactWriter;
import org.bibsonomy.folkrank.io.FactWritingException;
import org.bibsonomy.folkrank.io.StringFileFactWriter;
import org.bibsonomy.folkrank.process.FactPreprocessor;
import org.bibsonomy.folkrank.process.FactReaderFactPreprocessor;

public class TASDumper {


    public static void main(String[] args) throws FactReadingException, InterruptedException, FactWritingException, SQLException, FileNotFoundException, IOException {


        if (args.length < 1) {
            System.err.println("usage:");
            System.err.println("java " + TASDumper.class.getName() + " db.properties > tags.txt");
            System.exit(1);
        }
        Properties prop = new Properties();
        prop.load(new FileInputStream(args[0]));


        FactReader<String> reader;
        FactWriter writer;
        FactPreprocessor prep;
        FolkRankData facts;

        /*
         * normales Ranking
         */
        @SuppressWarnings("unused")
        String query = "(SELECT LOWER(t.tag_name), t.user_name, CONCAT(t.content_type,b.book_url_hash) AS resource " +
        "  FROM tas t " +
        "    JOIN bookmark b USING (content_id) " +
        "  WHERE t.group = 0 AND t.content_type = 1 AND t.user_name != 'dblp' AND t.tag_name != 'imported' AND t.tag_name != 'system:imported' LIMIT 100000000)" +
        "UNION ALL" +
        "(SELECT LOWER(t.tag_name), t.user_name, CONCAT(t.content_type,b.simhash1) AS resource " +
        "  FROM tas t " +
        "    JOIN bibtex b USING (content_id) " +
        "  WHERE t.group = 0 AND t.content_type = 2 AND t.user_name != 'dblp' AND t.tag_name != 'imported' AND t.tag_name != 'system:imported' LIMIT 100000000)";

        /*
         * HIER FLICKR-Ranking: Content_ID steht mit dran!
         */
        @SuppressWarnings("unused")
        String query1 = "(SELECT LOWER(t.tag_name), t.user_name, CONCAT(t.content_type,b.book_url_hash, '_', t.content_id) AS resource " +
        "  FROM tas t " +
        "    JOIN bookmark b USING (content_id) " +
        "  WHERE t.group = 0 AND t.content_type = 1 AND t.user_name != 'dblp' AND t.tag_name != 'imported' AND t.tag_name != 'system:imported' LIMIT 100000000)" +
        "UNION ALL" +
        "(SELECT LOWER(t.tag_name), t.user_name, CONCAT(t.content_type,b.simhash1, '_', t.content_id) AS resource " +
        "  FROM tas t " +
        "    JOIN bibtex b USING (content_id) " +
        "  WHERE t.group = 0 AND t.content_type = 2 AND t.user_name != 'dblp' AND t.tag_name != 'imported' AND t.tag_name != 'system:imported' LIMIT 100000000)";

        // read from data base
        reader = new DBFactReader(null, null, 3);

        // preprocess
        prep = new FactReaderFactPreprocessor(reader);

        prep.process();
        
        // extract array
        facts = prep.getFolkRankData();

        // write as string to file
        writer = new StringFileFactWriter(System.out, prop.getProperty("out.delim"));
        writer.write(facts);
    }
}
