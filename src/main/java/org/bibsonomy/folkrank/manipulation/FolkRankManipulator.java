package org.bibsonomy.folkrank.manipulation;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.bibsonomy.folkrank.FolkRank;
import org.bibsonomy.folkrank.data.FolkRankData;
import org.bibsonomy.folkrank.data.FolkRankParam;
import org.bibsonomy.folkrank.data.FolkRankPref;
import org.bibsonomy.folkrank.data.FolkRankResult;
import org.bibsonomy.folkrank.io.FactReadingException;
import org.bibsonomy.folkrank.io.FileFactReader;
import org.bibsonomy.folkrank.process.FactPreprocessor;
import org.bibsonomy.folkrank.process.ManipulableFactReaderFactPreprocessor;
import org.bibsonomy.folkrank.strategy.UnitedWeightInitializationStrategy;

public class FolkRankManipulator {

	private static final Logger log = Logger.getLogger(FolkRankManipulator.class);

	private static final double COMMON_BASE_PREF_WEIGHT = 0;
	private static final String TRAIN_FILE_DELIM = "\t";
	private static final String RESULT_FILE_DELIM = "\t";
	private static final String MANIPULATIVE_USER_PREFIX = "#######";
	private static final String MANIPULATIVE_RESOURCE = "##MAINPULATIVE_RESOURCE##";
	/*
	 *  These three parameters control the order of the dimensions 
	 *  (e.g. the dimension that contains the users is Udim)
	 *  The input file for training data must match this order
	 */
	private static final short Udim = 0;
	private static final short Rdim = 1;
	private static final short Tdim = 2;

	public static void main(String[] args) throws IOException, FactReadingException {
		/*
		 * extract command line arguments
		 */
		if (args.length < 4) {
			System.err.println("usage:");
			System.err.println("java " + FolkRankManipulator.class.getName() + " factFile inputUsers outPutFileSuffix");
			System.err.println("  -- computes for users the weigths to all resources --");
			System.err.println("  where ");
			System.err.println("  factFileName     is the training file (facts)");
			System.err.println("  outPutFileName is a file which will be written and contains for each user the resources with weights as csv");
			System.err.println("  tagChoice =1 means use the most popular tags, tagChoice =0 means use random Tags");
			System.err.println("  tags means the number of tags to use for each resource");
			System.err.println("  resources means the number of user accounts to use for manipulation");
			System.exit(1);
		}
		int argCtr = 0;
		final String factFileName = args[argCtr++];
		final String outPutFileName = args[argCtr++];
		final int tagChoice = Integer.parseInt(args[argCtr]);
		final int numTagsPerResource = Integer.parseInt(args[argCtr++]);
		final int numUserAccounts = Integer.parseInt(args[argCtr++]);
		/*
		 * read training data and init folkrank data
		 */
		System.out.println("reading facts");
		List<String[]> manipulatedFacts = new ArrayList<String[]>();
		for (int u=0; u<numUserAccounts; u++) {
			String userNameString = MANIPULATIVE_USER_PREFIX+u;
			Set<String> tagSet;
			if (tagChoice==0) {
				tagSet = selectMostPopularTags();
			} else if (tagChoice ==1) {
				tagSet = selectRandomTags();
			} else {
				throw new IllegalArgumentException("wrong tagchoice");
			}
			for (String tag: tagSet) {
				String[] fact = new String[3];
				fact[Udim] = userNameString;
				fact[Rdim] = MANIPULATIVE_RESOURCE;
				fact[Tdim] = tag;
			}
		}
		// tODO fill those facts!
		
		final FactPreprocessor processor = new ManipulableFactReaderFactPreprocessor(new FileFactReader(factFileName, TRAIN_FILE_DELIM), true, manipulatedFacts);
		processor.process();
		FolkRankData frData = processor.getFolkRankData();
		int manipulativeResource = frData.getNodeMap().getInverseMapping(Rdim, MANIPULATIVE_RESOURCE);

		int numUsers = frData.getCounts()[Udim].length;

		/*
		 * initialize FolkRank params
		 */
		final FolkRankParam param = new FolkRankParam();
		final FolkRankParam baseParam = new FolkRankParam();
		baseParam.setAlphaBetaGamma(0, 1, 0);
		param.setAlphaBetaGamma(0, 0.7, 0.3);
		param.setWeightInitializationStrategy(new UnitedWeightInitializationStrategy());
		/*
		 * all nodes get 1 unit preference
		 */
		final FolkRankPref pref = new FolkRankPref(new double[] {COMMON_BASE_PREF_WEIGHT, COMMON_BASE_PREF_WEIGHT, COMMON_BASE_PREF_WEIGHT});

		/*
		 * init FolkRank with test independent parameters
		 */
		final FolkRank folk = new FolkRank(baseParam, param);


		final BufferedWriter outWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outPutFileName), "UTF-8"));
		String line = null;
		/*
		 * iterate over the (user, resource)-pairs
		 */
		for (int u=0; u<frData.getNodeMap().getMapping(Udim).length; u++) {

			/*
			 * read in user and resourcehash
			 */
			String userId = frData.getNodeMap().getMapping(Udim, u);
			if (userId.startsWith(MANIPULATIVE_USER_PREFIX)) {
				// we do not count the ratings for the manipulative users
				continue;
			}

			final int[][] prefItems = new int[][]{{},{},{}};
			final double[][] prefValues = new double[][]{{},{},{}};
			pref.setPreference(prefItems, prefValues);
			prefItems[Udim]  = new int[]{u};
			prefValues[Udim] = new double[]{numUsers};

			/*
			 * set preference and compute
			 */
			pref.setPreference(prefItems, prefValues);

			log.info("starting folkrank computation for user " + userId);
			final FolkRankResult result = folk.computeFolkRank(frData, pref);

			log.info("finished folkrank computation for user " + userId);
			/*
			 * Prepare Writer for Output of the top k tags for each test (user-resource)-pair
			 */

			int count=0;
			for (int item=0; item<result.getWeights()[Rdim].length; item ++) {
				count++;
				if (item == manipulativeResource) {
					break;
				}
			}
			// write tag id (in output notation: starting with 1)
			outWriter.write(userId + RESULT_FILE_DELIM + count);
			outWriter.newLine();

			// write user id and computation error to stderr
			System.out.println(u + ": " + result.getErrors());
		}
		outWriter.close();

	}
	
	
	private static Set<String> selectMostPopularTags() {
		return null;
	}
	
	private static Set<String> selectRandomTags() {
		return null;
	}
	
}

