package org.bibsonomy.folkrank.process;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.bibsonomy.folkrank.data.DefaultNodeMap;
import org.bibsonomy.folkrank.data.FolkRankData;
import org.bibsonomy.folkrank.io.FactReader;
import org.bibsonomy.folkrank.io.FactReadingException;

/** The FactReaderPreprocessor reads the facts from the FactReader and
 * returns a FolkRankData object. For more information on how this is done
 * have a look at the {@link #process()} method.  
 * 
 * @author rja
 */
public class ManipulableFactReaderFactPreprocessor extends FactReaderFactPreprocessor {

	private List<String[]> manipulatedFacts;

    /** Initialize the preprocessor with a reader.
     *  
     * @param reader - the reader which supplies the preprocessor with facts.
     */
    public ManipulableFactReaderFactPreprocessor (final FactReader<String> reader, final List<String[]> manipulatedFacts) {
        super(reader);
        this.manipulatedFacts = manipulatedFacts;
    }

    /**
     * @param reader
     * @param storeInverseMapping - if <code>true</code>, the mapping string to int is stored in the facts
     */
    public ManipulableFactReaderFactPreprocessor (final FactReader<String> reader, final boolean storeInverseMapping, final List<String[]> manipulatedFacts) {
    	super(reader, storeInverseMapping);
    	this.manipulatedFacts = manipulatedFacts;
    }

    
    @Override
    protected void addFactPlugin(final Map<String, Integer>[] mapping, final int noOfDimensions, final List<int[]> factList, final int[] noOfItemsPerDimension) {
    	for (String[] manipulatedFact: this.manipulatedFacts) {
    		this.addFact(mapping, noOfDimensions, factList, noOfItemsPerDimension, manipulatedFact);
    	}
    }
}
