package org.bibsonomy.folkrank.process;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.HashMap;

import org.bibsonomy.folkrank.data.DefaultNodeMap;
import org.bibsonomy.folkrank.data.FolkRankData;
import org.bibsonomy.folkrank.data.NodeMap;

/** Reads facts from a file and stores them in a {@link FolkRankData} object.
 * The facts are supposed to be in integer representation and for each dimension
 * there exists a file which maps each item back to a string. The numbering of 
 * the items starts with "1" and in the mapping file each string is mapped to 
 * its line number.
 * 
 * @author rja
 */
public class IntegerFileFactPreprocessor implements FactPreprocessor {

	private FolkRankData facts = null;
	private HashMap<String, Integer>[] stringPrefItems = null;
	private int[][] intPrefItems = null;
	private String factsFileName = null;
	private String[] mappingFileNames = null; // one file for each dimension

	private static final int OFFSET = 1; // TODO: comment (see writer!) WARNING: We can not simply change the offset as in other places we implicitly assume, that it is 1

	/**
	 * @param factsFileName
	 * @param mappingFileNames
	 */
	public IntegerFileFactPreprocessor (String factsFileName, String[] mappingFileNames) {
		this.factsFileName = factsFileName;
		this.mappingFileNames = mappingFileNames;
	}

	/**
	 * @see org.bibsonomy.folkrank.process.FactPreprocessor#getFolkRankData()
	 */
	public FolkRankData getFolkRankData() {
		return facts;
	}

	/** <strong>TODO</strong>: check if the logic behind this method is properly
	 * implemented.
	 * @see org.bibsonomy.folkrank.process.FactPreprocessor#getPrefItems()
	 */
	public int[][] getPrefItems() {
		return intPrefItems;
	}

	/**
	 * @see org.bibsonomy.folkrank.process.FactPreprocessor#process()
	 */
	public void process() {
		int noOfDimensions = mappingFileNames.length;
		BufferedReader reader;

		try {
			/*
			 * read over file to get number of facts
			 */
			reader = new BufferedReader(new InputStreamReader(new FileInputStream(factsFileName), "UTF-8"));
			int noOfFacts = 0;
			while (reader.readLine() != null) {
				noOfFacts++;
			}
			reader.close();

			/*
			 * get number of items for every dimension
			 */
			int noOfItemsPerDimension[] = new int[mappingFileNames.length];
			Arrays.fill(noOfItemsPerDimension, 0); // initialize with zero
			for (int dim = 0; dim < mappingFileNames.length; dim++) {
				reader = new BufferedReader(new InputStreamReader(new FileInputStream(mappingFileNames[dim]), "UTF-8"));
				while (reader.readLine() != null) {
					noOfItemsPerDimension[dim]++; // every line is one item
				}
				reader.close();
			}

			/*
			 * read facts from file into array
			 */
			facts = new FolkRankData(noOfFacts, noOfItemsPerDimension, new DefaultNodeMap(noOfItemsPerDimension));
			String line;

			reader = new BufferedReader(new InputStreamReader(new FileInputStream(factsFileName), "UTF-8"));
			int factId = 0;
			while ((line = reader.readLine()) != null) {
				int[] intFact = new int[noOfDimensions];
				String[] stringFact = line.split(" ");
				for (int dim = 0; dim < intFact.length; dim++) {
					intFact[dim] = Integer.parseInt(stringFact[dim]) - OFFSET;
				}
				facts.setFact(factId, intFact);
				factId++;
			}
			reader.close();

			/*
			 * read mappings from file into array
			 */
			final NodeMap nodeMap = facts.getNodeMap();
			for (int dim = 0; dim < noOfDimensions; dim++) {
				reader = new BufferedReader(new InputStreamReader(new FileInputStream(mappingFileNames[dim]), "UTF-8"));
				noOfItemsPerDimension[dim] = 0;
				while ((line = reader.readLine()) != null) {
					String item = line.trim();
					/*
					 * remember mapping (here we implicitly assume, that the OFFSET is 1, otherwise noOfItemsPerDimension[dim] would contain the wrong key)
					 */
					nodeMap.addMapping(dim, noOfItemsPerDimension[dim], item);
					/*
					 * check for preference items
					 */
					if (stringPrefItems != null && 
							stringPrefItems[dim] != null && 
							stringPrefItems[dim].containsKey(item)) {
						/*
						 * item found --> remember its number
						 */
						intPrefItems[dim][stringPrefItems[dim].get(item)] = noOfItemsPerDimension[dim];
					}
					noOfItemsPerDimension[dim]++;
				}
				reader.close();
			}

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/**
	 * @see org.bibsonomy.folkrank.process.FactPreprocessor#setPrefItems(java.lang.String[][])
	 */
	@SuppressWarnings("unchecked")
	public void setPrefItems(String[][] prefItems) {
		/*
		 * Put strings of items into HashMap for faster access during file 
		 * reading.
		 */
		stringPrefItems = new HashMap[prefItems.length];
		intPrefItems = new int[prefItems.length][];
		for (int dim = 0; dim < prefItems.length; dim++) {
			/*
			 * initialize map and array
			 */
			stringPrefItems[dim] = new HashMap<String, Integer>();
			intPrefItems[dim] = new int[prefItems[dim].length];
			for (int item = 0; item < prefItems[dim].length; item++) {
				stringPrefItems[dim].put(prefItems[dim][item], item);
			}
		}
	}

}
