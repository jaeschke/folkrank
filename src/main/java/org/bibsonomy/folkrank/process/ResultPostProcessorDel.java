package org.bibsonomy.folkrank.process;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Comparator;

import org.bibsonomy.folkrank.data.FolkRankData;
import org.bibsonomy.folkrank.data.FolkRankResult;
import org.bibsonomy.folkrank.data.NodeMap;

/** This Class contains some static methods to postprocess FolkRank results.
 * 
 * @author rja
 */
public class ResultPostProcessorDel {

	/** Sorts the weights of each dimension.
	 * 
	 * @param result - A result from a FolkRank computation, containing the 
	 * final weight vector.
	 * @return An array for each dimension which gives a permutation which 
	 * allows ordering the weight array for that dimension by size.
	 */
	public static Integer[][] sort(FolkRankResult result) {
		double[][] weights = result.getWeights();
		Integer[][] orders = new Integer[weights.length][];

		for (int dim = 0; dim < orders.length; dim++) {
			/*
			 * initialize order array for dimension dim
			 */
			orders[dim] = new Integer[weights[dim].length];
			for (int item = 0; item < orders[dim].length; item ++) {
				orders[dim][item] = item;
			}
			/*
			 * sort
			 */
			Arrays.sort(orders[dim], new IntArrayComparator(weights[dim]));
		}
		return orders;
	}


	/** Writes the sorted folkrank weight vectors into files.
	 * 
	 * @param facts
	 * @param result
	 * @param orders
	 * @param filename
	 * @throws NoSuchAlgorithmException 
	 */
	public static <V> void analyse(FolkRankData facts, FolkRankResult result, Integer[][] orders, String filename, String tag) throws NoSuchAlgorithmException {
		final double[][] weights = result.getWeights();
		final NodeMap nodeMap = facts.getNodeMap();

		try {


			final FileOutputStream fos = new FileOutputStream(filename, true); 
			final BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(fos)); 



			writer.write("\nResults for " + tag + "\n"); 

			for (int dim=0; dim<orders.length;dim++){
				final String[] mapping = nodeMap.getMapping(dim);
				writer.write("DIMENSION " + dim + "\n");

				for (int item = 0; item < weights[dim].length; item++) {
					writer.write(weights[dim][orders[dim][item]] + "  " + mapping[orders[dim][item]] + "\n");
					if (item == 1000){
						break; 
					}
				} 
			}
			writer.close();


		} catch (IOException e) {
			e.printStackTrace();
		} 

	}


	/** This class is used to compare the double values in the weight vector and
	 * sort them in descending order.
	 *  
	 * @author rja
	 */
	private static class IntArrayComparator implements Comparator<Integer>  {

		private final double[] array;

		/** Construct the comparator by giving the array with the values to 
		 * sort.
		 * 
		 * @param array - an array which values should be sorted
		 */
		public IntArrayComparator (double[] array) {
			this.array = array;
		}

		/** Compare array[o1] with array[i2] 
		 * @param o1 
		 * @param o2 
		 * @return -1 if o2 lt o1, +1 if o2 gt o1, 0 otherwise
		 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
		 */
		public int compare(Integer o1, Integer o2) {
			/*
			 * absteigend sortiern
			 */
			return (int) Math.signum(array[o2] - array[o1]);
		}
	}

}
