package org.bibsonomy.folkrank.process;

import java.util.Arrays;
import java.util.Map;

import org.apache.log4j.Logger;
import org.bibsonomy.folkrank.data.DefaultNodeMap;
import org.bibsonomy.folkrank.data.FolkRankData;
import org.bibsonomy.folkrank.io.FactReader;
import org.bibsonomy.folkrank.io.FactReadingException;

/** Reads facts from a {@link FactReader} and saves them in {@link FolkRankData}.
 * The difference to the @link {@link FactReaderFactPreprocessor} is that this
 * preprocessor first reads all facts the reader delivers to count how many 
 * facts there are and then resets the reader to read the facts again. 
 * <br>
 * The advantage is, that this preprocessor needs much less memory than since it
 * can write the facts in the second reading process directly into an array in 
 * main memory. This is not only faster than first reading all facts into a list 
 * and then copying that list into an array, it also needs much less main 
 * memory.
 * <br>
 * The disadvantage is that the facts are read twice and there is no guarantee
 * that the number of facts did not change in between. Therefore it may happen
 * that using this preprocessor may fail, especially when reading from a 
 * database.  
 * <br>
 * Therefore this preprocessor can only be used with readers which safely allow
 * resetting and guarantee, that a second read returns the same result as the 
 * first read.
 * 
 * @author rja
 */
public class MemorySavingFactReaderFactPreprocessor extends AbstractFactPreprocessor {

	private static final Logger log = Logger.getLogger(MemorySavingFactReaderFactPreprocessor.class);

	private int numberOfFacts = -1; // not initialized
	private int[] nuOfItemsPerDimension; 

	/** Initialize the preprocessor with a reader.
	 * @param reader
	 */
	public MemorySavingFactReaderFactPreprocessor (final FactReader<String> reader) {
		this.reader = reader;
	}

	/**
	 * @param reader
	 * @param storeInverseMapping - if <code>true</code>, the mapping string to int is stored in the facts
	 */
	public MemorySavingFactReaderFactPreprocessor (final FactReader<String> reader, final boolean storeInverseMapping) {
		this(reader);
		this.storeInverseMapping = storeInverseMapping;
	}

	/**
	 * This constructor can be given the known number of facts. Thus, we don't need to read
	 * the facts file twice.
	 * 
	 * @param reader
	 * @param numberOfFacts - the number of facts
	 * @param storeInverseMapping - if <code>true</code>, the mapping string to int is stored in the facts
	 */
	public MemorySavingFactReaderFactPreprocessor (final FactReader<String> reader, final int numberOfFacts, final int[] noOfItemsPerDimension, final boolean storeInverseMapping) {
		this(reader);
		this.nuOfItemsPerDimension = noOfItemsPerDimension;
		this.numberOfFacts = numberOfFacts;
		this.storeInverseMapping = storeInverseMapping;
	}

	/**
	 * Counts the numbers of facts and items per dimension and also reads in 
	 * the String-> Integer mapping. Resets the reader afterwards. 
	 * 
	 * @param mapping
	 * @throws FactReadingException
	 */
	private void countFacts(final Map<String, Integer>[] mapping) throws FactReadingException {
		/*
		 * Initialize mapping counters (save current integer for each 
		 * dimension).
		 */
		nuOfItemsPerDimension = new int[mapping.length];
		Arrays.fill(nuOfItemsPerDimension, 0);

		/*
		 * read over reader to get number of facts
		 */
		numberOfFacts = 0;
		while (reader.hasNext()) {
			numberOfFacts++;

			/*
			 * for every fact
			 */
			final String[] stringFact = reader.getFact();

			for (int dim = 0; dim < stringFact.length; dim++) {
				/*
				 * get item for this dimension
				 */
				final String item = stringFact[dim];
				/*
				 * map new item
				 */
				if (!mapping[dim].containsKey(item)) {
					mapping[dim].put(item, nuOfItemsPerDimension[dim]);
					nuOfItemsPerDimension[dim]++;
				}
			}
		}

		log.debug("read " + numberOfFacts + " facts, will now reset reader");

		/*
		 * Reset the reader.
		 */
		reader.reset();

	}

	/** Process the data the reader returns. This is done by doing the following 
	 * steps:
	 * <ul>
	 * <li>initalize a map to map strings to integers for every mode</li>
	 * <li>iterate over the fact list from the reader and 
	 *   <ul>
	 *   <li>add a mapping (string to integer) to the map, if neccessary</li>
	 *   <li>count the number of facts</li>
	 *   </ul>
	 * </li>
	 * <li>reset the reader
	 * <li>again, iterate over the fact list from the reader and<String>
	 *   <ul>
	 *   <li>add fact to integer fact list</li>
	 *   </ul>
	 * </li>
	 * <li>generate inverse mapping (integer to tring)</li>
	 * </ul>
	 */
	@Override
	protected void processFacts(final Map<String, Integer>[] mapping) throws FactReadingException {
		
		if (numberOfFacts == -1) {
			/*
			 * the number of facts is not given - we must count first
			 */
			countFacts(mapping);
			readFacts(mapping);
		} else {
			/*
			 * the number of facts is known - we only need to read the facts
			 * file once
			 */
			readAndMapFacts(mapping);
		}
	}

	/**
	 * Reads the facts into an array when the number of facts and items is known. Also
	 * creates the string->int mapping.
	 * 
	 * @param mapping
	 * @throws FactReadingException
	 */
	private void readAndMapFacts(final Map<String, Integer>[] mapping) throws FactReadingException {
		final int noOfDimensions = mapping.length;
		final int[] noOfItemsPerDimensionLocal = new int[mapping.length];
		Arrays.fill(noOfItemsPerDimensionLocal, 0);
		/*
		 * allocate memory space for facts
		 */
		facts = new FolkRankData(numberOfFacts, nuOfItemsPerDimension, new DefaultNodeMap(nuOfItemsPerDimension));

		log.debug("allocated memory for facts, will now read facts into memory");

		int factId = 0;
		while (reader.hasNext()) {
			/*
			 * for every fact
			 */
			final String[] stringFact = reader.getFact();
			int[] intFact = new int[noOfDimensions];

			for (int dim = 0; dim < stringFact.length; dim++) {
				/*
				 * get item for this dimension
				 */
				final String item = stringFact[dim];
				/*
				 * map new item
				 */
				if (!mapping[dim].containsKey(item)) {
					mapping[dim].put(item, noOfItemsPerDimensionLocal[dim]);
					noOfItemsPerDimensionLocal[dim]++;
				}
				/*
				 * save item as integer in fact
				 */
				intFact[dim] = mapping[dim].get(item);
			}
			/*
			 * put fact into array
			 */
			facts.setFact(factId, intFact);
			factId++;
		}
	}
	
	private void readFacts(final Map<String, Integer>[] mapping) throws FactReadingException {
		final int noOfDimensions = mapping.length;

		/*
		 * allocate memory space for facts
		 */
		facts = new FolkRankData(numberOfFacts, nuOfItemsPerDimension, new DefaultNodeMap(nuOfItemsPerDimension));

		log.debug("allocated memory for facts, will now read facts into memory");

		int factId = 0;
		while (reader.hasNext()) {
			/*
			 * for every fact
			 */
			final String[] stringFact = reader.getFact();
			int[] intFact = new int[noOfDimensions];

			for (int dim = 0; dim < stringFact.length; dim++) {
				/*
				 * save item as integer in fact
				 */
				intFact[dim] = mapping[dim].get(stringFact[dim]);
			}

			/*
			 * put fact into array
			 */
			facts.setFact(factId, intFact);
			factId++;
		}
	}

}
