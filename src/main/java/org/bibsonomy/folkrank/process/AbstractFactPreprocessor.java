package org.bibsonomy.folkrank.process;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.bibsonomy.folkrank.data.FolkRankData;
import org.bibsonomy.folkrank.data.NodeMap;
import org.bibsonomy.folkrank.io.FactReader;
import org.bibsonomy.folkrank.io.FactReadingException;

/** The FactPreprocessor reads the facts from the FactReader and
 * returns a FolkRankData object. For more information on how this is done
 * have a look at the {@link #process()} method.  
 * 
 * @author rja
 */
public abstract class AbstractFactPreprocessor implements FactPreprocessor {

    protected FolkRankData facts = null;
    protected FactReader<String> reader;
    protected String[][] stringPrefItems = null;
    protected int[][] intPrefItems = null;
    protected boolean storeInverseMapping = false;
    
    /** Process the data the reader returns. This is done by doing the following 
     * steps:
     * <ul>
     * <li>initalize a map to map strings to integers for every mode</li>
     * <li>initalize a list to store the facts as integers</li>
     * <li>iterate over the fact list from the reader and 
     *   <ul>
     *   <li>add a mapping (string to integer) to the map, if neccessary</li>
     *   <li>add the fact to integer fact list</li>
     *   </ul>
     * </li>
     * <li>copy integer fact list into an array</li>
     * <li>generate inverse mapping (integer to tring)</li>
     * </ul>
     * 
     * @see org.semanticdesktop.nepomuk.comp.folkpeer.folkrank.process.FactPreprocessor#process()
     */
    public void process() {
        try {
            final int noOfDimensions = reader.getNoOfDimensions();
            
            /*
             * Initialize the map for the string->integer mapping.
             * This map stores for each dimension, which integer an item gets 
             * assigned.
             */
            @SuppressWarnings("unchecked")
            final Map<String, Integer> [] mapping = new HashMap[noOfDimensions];
            for (int dim = 0; dim < mapping.length; dim++) {
                mapping[dim] = new HashMap<String, Integer>();
            }
            
            /*
             * the facts are read
             */
            processFacts(mapping);
         
            /*
             * map preference items strings to integers  
             */
            if (stringPrefItems != null) {
                intPrefItems = new int[stringPrefItems.length][];
                for (int dim = 0; dim < stringPrefItems.length; dim++) {
                    intPrefItems[dim] = new int[stringPrefItems[dim].length];
                    for (int item = 0; item < stringPrefItems[dim].length; item++) {
                    	final String prefItem = stringPrefItems[dim][item];
                    	/*
                    	 * if we cannot find the item, we set it to -1.
                    	 */
						if (mapping[dim].containsKey(prefItem)) {
                    		intPrefItems[dim][item] = mapping[dim].get(prefItem);
                    	} else {
                    		intPrefItems[dim][item] = -1;
                    	}
                    }
                }
            }
            
            /*
             * invert mapping and put it into array 
             */
            final NodeMap nodeMap = facts.getNodeMap();
            for (int dim = 0; dim < mapping.length; dim++) {
			    final Iterator<String> it = mapping[dim].keySet().iterator();
			    while (it.hasNext()) {
			        final String key = it.next();
			        nodeMap.addMapping(dim, mapping[dim].get(key), key);
			        // delete mapping, if neccessary
			        if (!storeInverseMapping) it.remove();
			    }
			    /*
			     * store mapping, if neccessary
			     */
			    if (this.storeInverseMapping) {
			    	nodeMap.addInverseMapping(dim, mapping[dim]);
			    } else {
			        mapping[dim].clear();
			    }
			}
            
        } catch (FactReadingException e) {
            /*
             * TODO: implement proper exception handling
             */
            e.printStackTrace();
        }
    }
    
    
    /**
     * Must read the facts from the reader into {@link #facts} and additionally 
     * fill the String -> Integer mapping.
     * 
     * @param mapping
     * @throws FactReadingException
     */
    protected abstract void processFacts(final Map<String, Integer> [] mapping) throws FactReadingException;
    
    /** Returns the filled data for the FolkRank.
     * @see FactPreprocessor#getFolkRankData()
     */
    public FolkRankData getFolkRankData() {
        return facts;
    }

    /** Sets the preference items in string representation such that a 
     * subsequent call to {@link #process()} will map them to integers.
     * @see FactPreprocessor#setPrefItems(java.lang.String[][])
     */
    public void setPrefItems(String[][] prefItems) {
        this.stringPrefItems = prefItems;
    }

    /** Returns the integer representation of the preference items.
     * @see .FactPreprocessor#getPrefItems()
     */
    public int[][] getPrefItems() {
        return intPrefItems;
    }

	public boolean isStoreInverseMapping() {
		return storeInverseMapping;
	}

	public void setStoreInverseMapping(boolean storeInverseMapping) {
		this.storeInverseMapping = storeInverseMapping;
	}
}
