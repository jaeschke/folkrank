package org.bibsonomy.folkrank.process;

import org.bibsonomy.folkrank.data.FolkRankData;
import org.bibsonomy.folkrank.data.FolkRankParam;

/** A FactPreprocessor reads facts from a source (for example a FactReader) and
 * creates a FolkRankData object. 
 * 
 * Since during the FolkRank computation all data is represented in integers 
 * (and there exists only the mapping back to strings in memory) there must be 
 * an option to set preference for items given by their strings. Therefore the 
 * interface demands the method setPrefItems which accepts items as strings and 
 * the method getPrefItems which returns them in integer representation. 
 *  
 * @author rja
 */
public interface FactPreprocessor {

    /*
     * NOTE: the methods here are given in the order they should typically be
     * called. Although some preprocessors might allow different call order.
     */
    
    
    /** Gives the preprocessor items for each dimension which the preprocessor
     * shall map to their integer representation during processing. The 
     * resulting integers can be accessed with getPrefItems().
     * 
     * @param prefItems - an array of strings for each dimension. Each string
     * represents an item occuring in the facts and which should be mapped to
     * an integer in order to give it preference. 
     */
    public void setPrefItems (String [][] prefItems);

    /** Calling this method starts the preprocessing which includes reading the
     * facts, mapping them into an array of integers, saving the integer to 
     * string mappings and saving the integers for the prefItems.
     */
    public void process ();
    
    /** Returns the complete input data neccessary for the FolkRank computation.
     * 
     * @return A FolkRankData object which contains the data for the 
     * computation.
     */
    public FolkRankData getFolkRankData ();

    /** Returns an integer array where the numbers in each dimension represent
     * the items given with setPrefItems(). This array can be used to set the
     * preference items in {@link FolkRankParam.setPreference}. 
     * @return - for each dimension an array of integers representing the items
     * given as strings with setPrefItems.
     */
    public int[][] getPrefItems ();
}
