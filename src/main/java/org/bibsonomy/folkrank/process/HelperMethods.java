package org.bibsonomy.folkrank.process;

import java.util.Arrays;
import java.util.SortedSet;
import java.util.TreeSet;

import org.bibsonomy.folkrank.data.FolkRankData;
import org.bibsonomy.folkrank.data.FolkRankResult;
import org.bibsonomy.folkrank.data.ItemWithWeight;

public class HelperMethods {

    /** Returns the k highest ranked items for each dimension. Only items with 
     * positive weight are returned (!), including the item which has been given 
     * preference weight. 
     * 
     * @param facts - the facts used for ranking. Needed to map back from internal
     * ids to Strings.
     * @param result - the ranking result.
     * @param k - the number of highest ranked items to return for each dimension. 
     * @return - the highest ranked items.
     */
    public static SortedSet<ItemWithWeight<String>>[] getTopK (final FolkRankData facts, final FolkRankResult result, final int k) {
        return getTopK(facts, result.getWeights(), k);
    }


	/**
	 * See {@link #getTopK(FolkRankData, FolkRankResult, int)}
	 * 
	 * @param facts
	 * @param weights
	 * @param k
	 * @return
	 */
	public static SortedSet<ItemWithWeight<String>>[] getTopK(final FolkRankData facts, final double[][] weights, final int k) {
        @SuppressWarnings(value = { "unchecked" })
        final SortedSet<ItemWithWeight<String>>[] topK = new SortedSet[weights.length];
        for (int dim = 0; dim < weights.length; dim++) {
        	topK[dim] = getTopK(facts, weights, k, dim);
        }
		return topK;
	}


	/**
	 * Computes the top k items for the given dimension. Only items with 
     * positive weight are returned (!), including the item which has been given 
     * preference weight. See {@link #getTopK(FolkRankData, FolkRankResult, int)}
	 * 
	 * @param facts
	 * @param result
	 * @param k
	 * @param dim
	 * @return
	 */
	public static SortedSet<ItemWithWeight<String>> getTopK(final FolkRankData facts, final double[][] weights, final int k, int dim) {
		return getTopK(facts.getNodeMap().getMapping(dim), weights[dim], k);
	}
	
	public static SortedSet<ItemWithWeight<String>> getTopK (final FolkRankData facts, final FolkRankResult result, final int k, int dim) {
		return getTopK(facts.getNodeMap().getMapping(dim), result.getWeights()[dim], k);
	}
	
	
	/**
	 * Computes the top k items for the given dimension. Only items with 
     * positive weight are returned (!), including the item which has been given 
     * preference weight. See {@link #getTopK(FolkRankData, FolkRankResult, int)}
	 * 
	 * @param facts
	 * @param weights
	 * @param k
	 * @param dim
	 * @return
	 */
	public static <V> SortedSet<ItemWithWeight<V>> getTopK(final V[] mapping, final double[] weights, final int k) {
		final SortedSet<ItemWithWeight<V>> topKForDim = new TreeSet<ItemWithWeight<V>>();
		double minWeight = 0; // consider only items with positive weight 
		for (int item = 0; item < weights.length; item++) {
		    double currWeight = weights[item];
		    if (currWeight >= minWeight) {
		        /* new weight to consider found */
		    	topKForDim.add(new ItemWithWeight<V>(mapping[item], currWeight));
		        if (topKForDim.size() > k) {
		            /*
		             * new best weight, since we have more than k items in set
		             * --> remove worst item and remember weight of next best
				     *     item
		             */
		            final ItemWithWeight<V> last = topKForDim.last();
		            topKForDim.remove(last);
		            minWeight = topKForDim.last().weight;
		        }
		    }
		}
		return topKForDim;
	}
	
	
	public static void printWeightsSums(final double[][] weights, final String step) {
		/*
         * print sums of weight vectors
         */
        double sum = 0;
		double sums[] = new double[weights.length];
        for (int dim = 0; dim < sums.length; dim++) {
        	sums[dim] = getSum(weights[dim]);
        	sum += sums[dim];
        }
        System.out.println(step + ": " + Arrays.toString(sums ) + " = " + sum + " (" + weights + ")");
	}
	
	public static double getSum(final double[] a) {
		double sum = 0;
		for (int i = 0; i < a.length; i++) {
			sum += a[i];
		}
		return sum;
	}
}
