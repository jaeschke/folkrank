package org.bibsonomy.folkrank.process;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.bibsonomy.folkrank.data.DefaultNodeMap;
import org.bibsonomy.folkrank.data.FolkRankData;
import org.bibsonomy.folkrank.io.FactReader;
import org.bibsonomy.folkrank.io.FactReadingException;

/** The FactReaderPreprocessor reads the facts from the FactReader and
 * returns a FolkRankData object. For more information on how this is done
 * have a look at the {@link #process()} method.  
 * 
 * @author rja
 */
public class FactReaderFactPreprocessor extends AbstractFactPreprocessor {


    /** Initialize the preprocessor with a reader.
     *  
     * @param reader - the reader which supplies the preprocessor with facts.
     */
    public FactReaderFactPreprocessor (final FactReader<String> reader) {
        this.reader = reader;
    }

    /**
     * @param reader
     * @param storeInverseMapping - if <code>true</code>, the mapping string to int is stored in the facts
     */
    public FactReaderFactPreprocessor (final FactReader<String> reader, final boolean storeInverseMapping) {
    	this(reader);
    	this.storeInverseMapping = storeInverseMapping;
    }

    
	@Override
	protected void processFacts(final Map<String, Integer>[] mapping) throws FactReadingException {
		final int noOfDimensions = mapping.length;
		
        /*
         * Initialize integer fact list. This list is used to temporarily 
         * store the fact list in memory before copying it into an array.
         * 
         * The reason for doing this is, that the number of facts is unknown
         * and it may not be possible to read the same facts twice by using
         * reset() from the fact reader. 
         * 
         */
        final List<int[]> factList = new LinkedList<int []>();

        /*
         * Initialize mapping counters (save current integer for each 
         * dimension).
         */
        final int noOfItemsPerDimension[] = new int[noOfDimensions];
        Arrays.fill(noOfItemsPerDimension, 0);

        /*
         * generate mapping and integer fact list
         */
        while (reader.hasNext()) {
            /*
             * for every fact
             */
            final String[] stringFact = reader.getFact();
            addFact(mapping, noOfDimensions, factList, noOfItemsPerDimension, stringFact);
        }
        reader.close();
        addFactPlugin(mapping, noOfDimensions, factList, noOfItemsPerDimension);
        
        /*
         * copy fact list into array
         */            
        int noOfFacts = factList.size();
        this.facts = new FolkRankData(noOfFacts, noOfItemsPerDimension, new DefaultNodeMap(noOfItemsPerDimension));
        for (int factId = 0; factId < noOfFacts; factId++) {
            this.facts.setFact(factId, factList.remove(0));
        }
        factList.clear();
	}

	protected void addFact(final Map<String, Integer>[] mapping, final int noOfDimensions, final List<int[]> factList, final int[] noOfItemsPerDimension, final String[] stringFact) {
		int[] intFact = new int[noOfDimensions];
		
		for (int dim = 0; dim < stringFact.length; dim++) {
		    /*
		     * get item for this dimension
		     */
		    final String item = stringFact[dim];
		    /*
		     * map new item
		     */
		    if (!mapping[dim].containsKey(item)) {
		        mapping[dim].put(item, noOfItemsPerDimension[dim]);
		        noOfItemsPerDimension[dim]++;
		    }
		    /*
		     * save new item in fact
		     */
		    intFact[dim] = mapping[dim].get(item);
		}
		/*
		 * store fact
		 */
		factList.add(intFact);
	}

	protected void addFactPlugin(final Map<String, Integer>[] mapping, final int noOfDimensions, final List<int[]> factList, final int[] noOfItemsPerDimension) {
		// not implemented
	}
	
	
}
