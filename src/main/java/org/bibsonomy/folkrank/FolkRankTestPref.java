package org.bibsonomy.folkrank;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;

import org.bibsonomy.folkrank.data.FolkRankData;
import org.bibsonomy.folkrank.data.FolkRankParam;
import org.bibsonomy.folkrank.data.FolkRankPref;
import org.bibsonomy.folkrank.data.FolkRankResult;
import org.bibsonomy.folkrank.io.FactReader;
import org.bibsonomy.folkrank.io.FactReadingException;
import org.bibsonomy.folkrank.io.FactWritingException;
import org.bibsonomy.folkrank.io.FileFactReader;
import org.bibsonomy.folkrank.process.FactPreprocessor;
import org.bibsonomy.folkrank.process.MemorySavingFactReaderFactPreprocessor;
import org.bibsonomy.folkrank.process.ResultPostProcessorDel;
import org.bibsonomy.folkrank.strategy.SeparatedWeightInitializationStrategy;

public class FolkRankTestPref{

	public static void main(String[] args) throws FactReadingException,
			InterruptedException, FactWritingException, IOException,
			ParseException, NoSuchAlgorithmException {

		FactPreprocessor prep;
		FolkRankData facts;
		FolkRankParam param;
		FolkRankPref pref;
		FolkRankResult result;
		Integer[][] orders;
		Collection<FactReader<String>> readers;
		  

		String propertyFile = args[0];
	
		//get Properties from property file
		BufferedReader bufProp = new BufferedReader(new FileReader(new File(propertyFile)));
		
		String dir = bufProp.readLine().split(" ")[1]; 
		String inputFile = bufProp.readLine().split(" ")[1];
		String outputFile = bufProp.readLine().split(" ")[1]; 
		String tagFile = bufProp.readLine().split(" ")[1]; 
		
		int dim = Integer.parseInt(bufProp.readLine().split(" ")[1]); 
		String strategy = bufProp.readLine().split(" ")[1]; 
		
		//get Tags
		BufferedReader buf = new BufferedReader(new FileReader(new File(dir+"/"+tagFile)));
	
		HashMap<String, Integer> tags = new HashMap<String, Integer>();
		while (buf.ready()) {
	
			tags.put(buf.readLine(),1);
	
		}
		
		
		long start = System.currentTimeMillis();

		// Start Folkrank
		System.out.println("Read File Start " + pastTime(start));

		// read data from fact file
		FactReader<String> readerFact = new FileFactReader(dir + "/" + inputFile, "|#|");

		readers = new HashSet<FactReader<String>>();
		readers.add(readerFact);

		// Preprocess data

		// String filename = dir + "/_facts";

		prep = new MemorySavingFactReaderFactPreprocessor(readerFact);
		

		while (buf.ready()) {

			tags.put(buf.readLine(),1);
	
		}
		buf.close();
	    
		// write the results of 200 tags into one file
		
		int tagcounter = 0; 
	
		//TODO: give preferences to several items
		for (String tag: tags.keySet()) {
			
			
			System.out.println(tagcounter + " Lauf für " + tag); 
			System.out.println("Start: " + pastTime(start)); 
		

			prep.setPrefItems(new String[][] { new String[] {}, new String[] {tag},
					new String[] {}, new String[]{} });
			//TODO: ungeschickt für mehrere Läufe, man muss preferenzen auch wieder neu hinzufügen, löschen können, 
			// dann kann man einmal nur aufrufen.
			prep.process();
			

			System.out.println("GET " + pastTime(start));
			facts = prep.getFolkRankData();
			
	
			pref = new FolkRankPref(new double[] { 1.0, 1.0, 1.0, 1.0 });

			param = new FolkRankParam();
			param.setAlphaBetaGamma(0.0, 0.85, 0.15);
			param.setStopCondition(10e-5, 50);
			
			System.out.println("Get counts: " + facts.getCounts()[0].length); 

			//TODO: set preferences flexibel
			pref.setPreference(prep.getPrefItems(), new double[][] {
				new double[] {}, new double[] {facts.getCounts()[0][0]}, 
					new double[] {}, new double[] {} });

			param
					.setWeightInitializationStrategy(new SeparatedWeightInitializationStrategy());

			System.out.println("Preprocess " + pastTime(start));

			// Start FolkRank

			FolkRank folk = new FolkRank(param);

			result = folk.computeFolkRank(facts, pref);

			System.out.println("FolkRank Comp" + pastTime(start));

			orders = ResultPostProcessorDel.sort(result);

			ResultPostProcessorDel.analyse(facts, result, orders, 
					dir +"/" + outputFile, tag);

			System.out.println("FIN " + pastTime(start));

			for (int i = 0; i < result.getErrors().size(); i++) {
				System.out.println(i + " : " + result.getErrors().get(i));
			}
			
			tagcounter++; 
			
		
		}
		
		System.out.println("Final" + pastTime(start)); 

	}
	


	private static long pastTime(long start) {
		return (System.currentTimeMillis() - start) / 1000;
	}

}