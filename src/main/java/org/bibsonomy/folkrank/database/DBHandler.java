package org.bibsonomy.folkrank.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import org.apache.log4j.Logger;


/**
 * @author rja
 */
public class DBHandler {

    /*
     * names of tables for rankings and weights
     */
    private static final String TABLE_NAME_RANKINGS_OLD = "rankings_old";
    public  static final String TABLE_NAME_RANKINGS_NEW = "rankings_new";
    private static final String TABLE_NAME_RANKINGS     = "rankings";

    private static final String TABLE_NAME_WEIGHTS_OLD = "weights_old";
    public  static final String TABLE_NAME_WEIGHTS_NEW = "weights_new";
    private static final String TABLE_NAME_WEIGHTS     = "weights";
    

    private static final Logger log = Logger.getLogger(DBHandler.class);

    private static Properties prop;
    private static DBHandler instance = null;

    public static void setProperties (Properties properties) {
        prop = properties;
    }
   
    protected DBHandler() {
    	// singleton
    }

    public static DBHandler getInstance () throws SQLException {
        if (instance == null) {
            /*
             * initialize and configure
             */
            instance = new DBHandler();
            instance.initTables();
        }
        return instance;
    }

    protected void initTables() throws SQLException {
        /*
         * configure database connection
         */
        try {
            Class.forName (prop.getProperty("db.driver")).newInstance();
        } catch (InstantiationException e1) {
            log.fatal(e1);
        } catch (IllegalAccessException e1) {
            log.fatal(e1);
        } catch (ClassNotFoundException e1) {
            log.fatal(e1);
        }
        /*
         * create tables, if neccessary
         */
        final Statement statement = getMasterConnection().createStatement();
 
        /*
            s.executeUpdate("DROP TABLE weights");
            s.executeUpdate("DROP TABLE rankings");
            s.executeUpdate("DROP TABLE tas");
            s.executeUpdate("DROP TABLE ids");
            s.executeUpdate("DROP TABLE ranking_queue");
         */
        log.debug("creating table " + TABLE_NAME_RANKINGS_NEW);
        try {
            statement.executeUpdate("CREATE TABLE " + TABLE_NAME_RANKINGS_NEW + " (" +
                "id int NOT NULL," +
                "time timestamp NOT NULL default CURRENT_TIMESTAMP," +
                "alpha double default NULL," +
                "beta double default NULL," +
                "gamma double default NULL," +
                "basepref double default NULL," +
                "dim int default NULL," +
                "item varchar(255) default NULL," +
                "itemtype int default NULL," +
                "itempref double default NULL," +
                "delta double default NULL," + 
                "iter int default NULL," +
            "PRIMARY KEY  (id))");

            log.debug("creating index on table " + TABLE_NAME_RANKINGS_NEW);
            statement.executeUpdate("CREATE INDEX dim_item ON " + TABLE_NAME_RANKINGS_NEW + " (dim,item)");
        } catch (SQLException e) {
            log.warn(e);
        }

        log.debug("creating table " + TABLE_NAME_WEIGHTS_NEW);
        try {
            statement.executeUpdate("CREATE TABLE " + TABLE_NAME_WEIGHTS_NEW + " (" + 
                "id int default NULL," + 
                "weight double default NULL," + 
                "dim int default NULL," + 
                "item varchar(255) default NULL," + 
                "itemtype int default NULL," + 
                " FOREIGN KEY (id) REFERENCES " + TABLE_NAME_RANKINGS_NEW + "(id) ON DELETE CASCADE )");

            log.debug("creating index on table " + TABLE_NAME_WEIGHTS_NEW);
            statement.executeUpdate("CREATE INDEX id ON " + TABLE_NAME_WEIGHTS_NEW + " (id)"); 
            statement.executeUpdate("CREATE INDEX dim ON " + TABLE_NAME_WEIGHTS_NEW + " (dim)"); 
        } catch (SQLException e) {
            log.warn(e);
        }

        try {
            log.debug("creating table ranking queue");
            statement.executeUpdate("CREATE TABLE ranking_queue (id int, dim int default 0, item varchar(255), date timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP, PRIMARY KEY (dim, item))");
            log.debug("creating index on table ranking_queue");
            statement.executeUpdate("CREATE INDEX date_idx ON ranking_queue (date)");
        } catch (SQLException e) {
            log.warn(e);
        }

    }

    
    /** Turns on the newly computes ranking/weight tables and deletes the old ones.
     * 
     * @throws SQLException
     */
    public void activateNewRanking () throws SQLException {
        
        final Connection conn = getMasterConnection();
        final Statement stmt = conn.createStatement();

        /*
         * turn auto commit of (do this as one transaction!) 
         */
        conn.setAutoCommit(false);

        try {
        	/*
        	 * first: we check, if tables with new rankings exist. if not: stop.
        	 */
        	if (!stmt.executeQuery("SHOW TABLES LIKE '" + TABLE_NAME_RANKINGS_NEW + "';").next()) {
        		/*
        		 * no table found with name TABLE_NAME_RANKINGS_NEW --> exit
        		 */
        		log.info("No table named " + TABLE_NAME_RANKINGS_NEW + " found. Exiting.");
        		return;
        	}
        	log.info("Renaming tables.");
        	
            /*
             * rename weights table 
             * NOTE: order is important: first change/delete weights table, then
             * rankings table, because the rankings table has a foreign key in
             * the weights table.
             */
            try {
                stmt.executeUpdate("RENAME TABLE " + TABLE_NAME_WEIGHTS + " TO " + TABLE_NAME_WEIGHTS_OLD);
            } catch (SQLException e) {
                log.warn("Could not rename table " + TABLE_NAME_WEIGHTS + " to " + TABLE_NAME_WEIGHTS_OLD + ": " + e);
            }
            stmt.executeUpdate("RENAME TABLE " + TABLE_NAME_WEIGHTS_NEW + " TO " + TABLE_NAME_WEIGHTS);

            /*
             * rename rankings table
             */
            try {
                stmt.executeUpdate("RENAME TABLE " + TABLE_NAME_RANKINGS + " TO " + TABLE_NAME_RANKINGS_OLD);
            } catch (SQLException e) {
                log.warn("Could not rename table " + TABLE_NAME_RANKINGS + " to " + TABLE_NAME_RANKINGS_OLD + ": " + e);
            }
            stmt.executeUpdate("RENAME TABLE " + TABLE_NAME_RANKINGS_NEW + " TO " + TABLE_NAME_RANKINGS);

            /*
             * commit changes
             */
            conn.commit();
        } catch (final SQLException e) {
            conn.rollback();
            log.fatal(e);
        }
        /*
         * delete old tables
         */
        try {
            stmt.executeUpdate("DROP TABLE " + TABLE_NAME_WEIGHTS_OLD);
        } catch (SQLException e) {
            log.warn("Could not drop table " + TABLE_NAME_WEIGHTS_OLD + ": " + e);
        }
        try {
            stmt.executeUpdate("DROP TABLE " + TABLE_NAME_RANKINGS_OLD);
        } catch (SQLException e) {
            log.warn("Could not drop table " + TABLE_NAME_RANKINGS_OLD + ": " + e);
        }
        
        conn.commit();
        
        stmt.close();
    }
    
    /** Returns the database connection to the master . This Connection is used either for 
     * reading and writing data or only for writing data (when {@link #getSlaveConnection()} 
     * is used for reading data).
     * 
     * @return
     * @throws SQLException
     */
    public Connection getMasterConnection() throws SQLException {
        return DriverManager.getConnection (prop.getProperty("db.master.url"), prop.getProperty("db.master.user"), prop.getProperty("db.master.pass"));
    }
    
    
    /** Returns a connection to the database which holds the job queue
     * @return
     * @throws SQLException
     */
    public Connection getJobQueueConnection() throws SQLException {
        return DriverManager.getConnection (prop.getProperty("db.jobqueue.url"), prop.getProperty("db.jobqueue.user"), prop.getProperty("db.jobqueue.pass"));
    }
    
    /** Returns a database connection to the slave. This connection is used to read data.
     * DO NOT write to this connection!
     * 
     * @return
     * @throws SQLException
     */
    public Connection getSlaveConnection() throws SQLException {
        final Connection connection = DriverManager.getConnection (prop.getProperty("db.slave.url"), prop.getProperty("db.slave.user"), prop.getProperty("db.slave.pass"));
        /*
         * set connection to be read-only
         */
        connection.setReadOnly(true);
		return connection;
    }

}
