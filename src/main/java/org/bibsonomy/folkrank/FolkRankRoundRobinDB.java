package org.bibsonomy.folkrank;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.bibsonomy.folkrank.data.FolkRankData;
import org.bibsonomy.folkrank.data.FolkRankParam;
import org.bibsonomy.folkrank.data.FolkRankPref;
import org.bibsonomy.folkrank.data.FolkRankResult;
import org.bibsonomy.folkrank.data.NodeMap;
import org.bibsonomy.folkrank.database.DBHandler;
import org.bibsonomy.folkrank.io.DBFactReader;
import org.bibsonomy.folkrank.io.DBJobQueue;
import org.bibsonomy.folkrank.io.DBRankWriter;
import org.bibsonomy.folkrank.io.FactReader;
import org.bibsonomy.folkrank.io.FactReadingException;
import org.bibsonomy.folkrank.io.DBJobQueue.RankingJob;
import org.bibsonomy.folkrank.process.FactPreprocessor;
import org.bibsonomy.folkrank.process.FactReaderFactPreprocessor;
import org.bibsonomy.folkrank.strategy.SeparatedWeightInitializationStrategy;
import org.bibsonomy.folkrank.strategy.WeightInitializationStrategy;

public class FolkRankRoundRobinDB implements Runnable {

    private static final Logger log = Logger.getLogger(FolkRankRoundRobinDB.class);

    private FactReader<String> reader;
    private FactPreprocessor prep;
    private FolkRankData facts;
    private final FolkRankParam param;
    private final FolkRankPref pref;
    private FolkRankResult result;
    private final FolkRank folk;
    private final DBJobQueue queue;
    private final DBRankWriter rwriter;

    private final Properties prop;
    
    private int k = 20;
    private int maxNoOfRuns = 0; // 0 = infinity
    private int milliSecondsBetweenRuns = 0;
    private int milliSecondsBetweenComputations = 0;

    public FolkRankRoundRobinDB (Properties prop) throws FactReadingException, SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
        this.prop = prop;

        DBHandler.setProperties(prop);

        /*
         * configure FolkRrank
         */
        log.debug("configuring folkrank parameters");
        pref = new FolkRankPref(new double[] {1.0, 1.0, 1.0}); // pref for each item

        param = new FolkRankParam();
        param.setAlphaBetaGamma(getDouble("folkrank.alpha"), getDouble("folkrank.beta"), getDouble("folkrank.gamma"));
        param.setStopCondition(getDouble("folkrank.epsilon"), getInt("folkrank.maxIter"));

        log.debug("initializing weight initializing strategy");
        WeightInitializationStrategy strategy = null;       
        try {
            strategy = (WeightInitializationStrategy) Class.forName(prop.getProperty("folkrank.strategy")).newInstance();
        } catch (Exception e) {
            /*
             * TODO: implement proper exception handling
             */
            log.warn("initialization of weight initialization strategy failed, " +
                "setting " + 
                SeparatedWeightInitializationStrategy.class.getName() + 
            " as default.");
            strategy = new SeparatedWeightInitializationStrategy();
        }
        param.setWeightInitializationStrategy(strategy);

        /*
         * instantiating folkrank
         */
        log.debug("instantiating folkrank");
        folk = new FolkRank(param);


        /*
         * rank writer
         */
        log.debug("initializing rank writer");

        rwriter = new DBRankWriter();

        /*
         * initialize job queue
         */
        log.debug("initializing job queue reader/writer");
        queue = new DBJobQueue();

        /*
         * initialize: 
         * - number of top-k items to compute, 
         * - maximal number of runs
         * - waiting time between two computations
         * - waiting time between two runs 
         */
        try {
            k                              = getInt("folkrank.k");
            maxNoOfRuns                    = getInt("folkrank.maxNoOfRuns");
            milliSecondsBetweenRuns         = getInt("folkrank.milliSecondsBetweenRuns");
            milliSecondsBetweenComputations = getInt("folkrank.milliSecondsBetweenComputations");
        } catch (NumberFormatException e) {
            log.fatal("could not get k (number of rankings to save) or maxNoOfRuns: " + e);
        }
    }

    /** Main method to run folkrank.
     * 
     * @param args - args[0] must contain the path to the properties file for 
     * configuring the folkrank runs.
     * @throws ClassNotFoundException 
     * @throws IllegalAccessException 
     * @throws InstantiationException 
     * @throws SQLException 
     * @throws FactReadingException 
     * @throws IOException 
     * @throws FileNotFoundException 
     */
    public static void main(String[] args) throws FactReadingException, SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException, FileNotFoundException, IOException {
        if (args.length < 1) {
            System.out.println("usage:");
            System.out.println("  java " + FolkRankRoundRobinDB.class.getName() + " folkrank.properties");
            System.exit(1);
        }
        PropertyConfigurator.configureAndWatch( "src/main/resources/org/bibsonomy/folkrank/log4j.properties", 60*1000 );
        
        Properties prop = new Properties();
        prop.load(new FileInputStream(args[0]));
        FolkRankRoundRobinDB rr = new FolkRankRoundRobinDB(prop);
        rr.run();
    }


    /** Infinite loop which runs FolkRank calculation.
     * @see java.lang.Runnable#run()
     */
    public void run() {
        int noOfRuns = 0; 
        while (maxNoOfRuns == 0 || noOfRuns < maxNoOfRuns) {
            /*
             * one run over all items in job queue
             */

            /* *****************************************************************
             * actual computation
             * ****************************************************************/
            
            log.info("doing computation");
            noOfRuns++;
            oneRun();
            log.info("finished computation");

            /*
             * waiting time between runs
             */
            try {
                Thread.sleep(milliSecondsBetweenRuns);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * This methods does one run of folkrank over the whole job queue. This means,
     * for each job in the job queue, one folkrank computation is done.
     */
    public void oneRun() {

        try {
            /*
             * configure input database connection
             */
            log.debug("configuring database connection for data input");
            final Connection connection = DBHandler.getInstance().getMasterConnection();

            /*
             * disable transactions to speed up reading
             */
            connection.setTransactionIsolation(Connection.TRANSACTION_READ_UNCOMMITTED);

            reader = new DBFactReader(connection, prop.getProperty("db.query.input"), 3);

            /*
             * generate preprocessor for facts, preprocess, extract facts
             */
            log.debug("preprocessing facts (i.e., reading data)");
            prep = new FactReaderFactPreprocessor(reader);
            prep.process(); 
            facts = prep.getFolkRankData();


            /*
             * fill queue
             */
            if (queue.isEmpty() && facts.getFacts().length > 0) {
                log.debug("filling job queue");
                fillJobQueue();
            }
            log.debug("processing job queue");

            processJobQueue();
            
        } catch (SQLException e) {
            log.fatal(e);
        } catch (NumberFormatException e) {
            log.fatal(e);
        } catch (FactReadingException e) {
            log.fatal(e);
        }

    }

    /** Adds for each item in each dimension a ranking job to the job queue.
     * 
     * @throws SQLException
     */
    private void fillJobQueue () throws SQLException {
        final NodeMap nodeMap = facts.getNodeMap();
        for (int dim = 0; dim < facts.getDims(); dim++) {
            int counter = 0;
            for (final String item: nodeMap.getMapping(dim)) {
                counter++;
                queue.add(new DBJobQueue.RankingJob(dim, item, 0)); // FIXME
            }
            log.info("added " + counter + " items in dimension " + dim + " to ranking queue");
        }
    }


    private void processJobQueue () throws SQLException {

    	final NodeMap nodeMap = facts.getNodeMap();
        final int[][] prefItems = new int[facts.getDims()][];
        final double[][] prefValues = new double[facts.getDims()][];
        pref.setPreference(prefItems, prefValues);

        RankingJob job = queue.getNextJob();
        while (job != null) {

            /*
             * process job
             */
            /*
             * get item id
             * since we don't have a mapping string -> integer, we have to do a
             * search over all strings to find the correct integer. TODO: 
             * improve this!
             */
            int itemId = -1;
            int itemDim = job.dim;
            boolean found = false;
            final String[] mapping = nodeMap.getMapping(itemDim);
            for (int item = 0; item < mapping.length; item++) {
                if (mapping[item].equals(job.item)) {
                    itemId = item;
                    found = true;
                    break;
                }
            }
            if (!found) {
                log.fatal("could not find no. for item " + job.item + " in dim " + itemDim + ".");
                log.fatal("mapping[itemDim].length = " + mapping.length);
                return;
            }
            /*
             * set preference
             */
            for (int dim = 0; dim < facts.getDims(); dim++) {
                if (dim != itemDim) {
                    /*
                     * give zero preference to each entry ...
                     */
                    prefItems[dim] = new int[]{};
                    prefValues[dim] = new double[]{};
                } else {
                    /*
                     * ... and much preference to item for job
                     * (much = no. of items in this dimension)
                     */
                    prefItems[dim] = new int[]{itemId};
                    prefValues[dim] = new double[]{mapping.length};
                }
            }
            /*
             * calculate ranking
             */
            result = folk.computeFolkRank(facts, pref);
            /*
             * save ranking
             */
            try {
                rwriter.writeTopK(facts, result, pref, param, k);
            } catch (SQLException e) {
                log.fatal("could not writing ranking result: " + e);
            }
            /*
             * delete finished job
             */
            queue.remove(job);
            /*
             * get next job
             */
            job = queue.getNextJob();

            /*
             * waiting time between computations
             */
            try {
                Thread.sleep(milliSecondsBetweenComputations);
            } catch (InterruptedException e) {
                log.fatal(e);
            }
            
        }
        
        /*
         * we're finished: release new rankings by renaming the ranking/weight tables
         */
        DBHandler.getInstance().activateNewRanking();
    }


    /** Extract a double property.
     * @param propName
     * @return A double value contained in propName.
     */
    private double getDouble(String propName) {
        return Double.parseDouble(prop.getProperty(propName).trim());
    }

    /** Extract an integer property.
     * @param propName
     * @return An integer value contained in propName.
     */
    private int getInt(String propName) {
        return Integer.parseInt(prop.getProperty(propName).trim());
    }

}
