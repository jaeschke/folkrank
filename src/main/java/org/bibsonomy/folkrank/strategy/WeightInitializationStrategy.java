package org.bibsonomy.folkrank.strategy;

import org.bibsonomy.folkrank.data.FolkRankPref;

/** A weight initialization strategy is used to initialize weight and preference 
 * vectors in a FolkRank computation. 
 * 
 * @author rja
 */
public interface WeightInitializationStrategy {

    /** Initializes the vectors weights, prefWeights and prefWeightsNormFactors 
     * with the help of the given parameters param. The weights and the prefWeights will already be normalized according to the strategy. 
     * The prefWeighsNormFactors will contain the value that must be used to normalize the additional preferences.
     * Note that the memory for all vectors must be allocated before calling 
     * initializeWeights!
     * 
     * @param pref - The preference to be used for initialization. 
     * @param weights - The output vector - "real" weight vector used for the 
     * FolkRank computation; initialized in this method.
     * @param prefWeights - The output vector which specifies for every 
     * dimension how many preference weight <em>each item</em> in that dimension 
     * gets.
     * @param prefWeightsNormFactors - The output vector which specifies for 
     * every dimension the value the additional preferences have to be divided 
     * with such that everything sums up to one.  
     * @param counts TODO
     */
    public abstract void initalizeWeights(
        FolkRankPref pref,
        double[][] weights,
        double[] prefWeights,
        double[] prefWeightsNormFactors, int[][] counts);

    
    /**
     * Depending on the WeightInitializatinoStrategy it can be necessary to adapt the params dimension-wise
     * @param alpha
     * @param beta
     * @param gamma
     * @param prefNodes
     * @param pWeights
     */
    public abstract void adaptAlphaBetaGamma(double[] alpha, double[] beta, double[] gamma, final int[][] prefNodes, final double[] pWeights);
}