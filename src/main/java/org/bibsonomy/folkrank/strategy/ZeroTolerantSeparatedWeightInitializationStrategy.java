package org.bibsonomy.folkrank.strategy;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bibsonomy.folkrank.data.FolkRankPref;

public class ZeroTolerantSeparatedWeightInitializationStrategy extends SeparatedWeightInitializationStrategy {

	/**
	 * @see org.bibsonomy.folkrank.strategy.WeightInitializationStrategy#initalizeWeights(org.semanticdesktop.nepomuk.comp.folkpeer.folkrank.data.FolkRankParam, double[][], double[], double[], int[][])
	 */
	public synchronized void initalizeWeights(FolkRankPref pref, double[][] weights, double[] prefWeights, double[] prefWeightsNormFactors, int[][] counts) {
		/*
		 * check input arguments
		 */
		if (weights.length != prefWeights.length || weights.length != prefWeightsNormFactors.length) 
			throw new IllegalArgumentException("Sizes of first dimension of input parameters do not match.");

		double[] basePrefWeight = pref.getBasePrefWeight(); // basepref in each dimension
		int[][] prefItems = pref.getPrefItems(); // nodes that get extra Pref
		double[][] prefValues   = pref.getPrefValues();  // preference values for these nodes

		for (int dim = 0; dim < weights.length; dim++) {
			/*
			 * count null-values in this dimension
			 */
			if (counts == null || counts[dim] == null || counts[dim].length == 0) {
				throw new IllegalArgumentException("The "+ZeroTolerantSeparatedWeightInitializationStrategy.class+" depends on the counts in each dimension to reach a successfull normalization.");
			}
			List<Integer> zeroNodes = new ArrayList<Integer>();
			for (int i = 0; i<counts[dim].length; i++) {
				if (counts[dim][i]==0) {
					zeroNodes.add(i);
					if (prefItems!=null && prefItems[dim]!=null) {
						for (int p=0; p<prefItems[dim].length; p++) {
							if (prefItems[dim][p] == i) {
								// Pref was given to an item that is isolated
								throw new IllegalArgumentException("Isolated Nodes must not receive preference weights!");
							}
						}
					}
				}
			}
			/*
			 * Initialize weights (w_0):
			 * every item gets the reciprocal of the number of items in the 
			 * dimensions it belongs to.
			 * Thus the weights in each dimension sum up to one, the total sum is the number of dimensions
			 */
			Arrays.fill(weights[dim], 1.0 / (weights[dim].length-zeroNodes.size()));
			for (Integer zeroNode: zeroNodes) {
				weights[dim][zeroNode] = 0;
			}

			/*
			 * Initialize preferences (p)
			 * Calculate the sum of the actual preference weights.
			 */
			prefWeightsNormFactors[dim] = 0.0;
			if (prefValues != null) {
				for (double prefValue:prefValues[dim]) {
					prefWeightsNormFactors[dim] += prefValue;
				}
			}

			/*
			 * Calculate, how many additional weight is spread by the preference in this dimension = the total amount in this dimension
			 */
			// sum over all entries in p = weight each item gets * number of items + the summed weight of the preferenced items
			prefWeightsNormFactors[dim] = basePrefWeight[dim] * (weights[dim].length - zeroNodes.size()) + prefWeightsNormFactors[dim];

			/*
			 * initialize the preference weights each item gets (normalize it)
			 */
			if (basePrefWeight[dim] == 0.0) {
				/*
				 * prevent 0.0 / 0.0 = NaN
				 */
				prefWeights[dim] = 0.0;
			} else {
				prefWeights[dim] = basePrefWeight[dim] / prefWeightsNormFactors[dim];
			}
		}
	}

}
