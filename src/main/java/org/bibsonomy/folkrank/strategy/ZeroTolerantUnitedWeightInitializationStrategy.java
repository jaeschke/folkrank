package org.bibsonomy.folkrank.strategy;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bibsonomy.folkrank.data.FolkRankPref;

/** This strategy spreads one unit of weight over all dimensions. This means 
 * that the (inital) weights of all dimensions sum up to one (as do the 
 * prefWeights when the preference is included).
 * 
 * @author rja
 */
public class ZeroTolerantUnitedWeightInitializationStrategy extends UnitedWeightInitializationStrategy {
    /**
     * @see org.bibsonomy.folkrank.strategy.WeightInitializationStrategy#initalizeWeights(org.semanticdesktop.nepomuk.comp.folkpeer.folkrank.data.FolkRankParam, double[][], double[])
     */
    public void initalizeWeights(FolkRankPref pref, double[][] weights, double[] prefWeights, double[] prefWeightsNormFactors, int[][] counts) {
        /*
         * check input arguments
         */
        if (weights.length != prefWeights.length || weights.length != prefWeightsNormFactors.length) 
            throw new IllegalArgumentException("Sizes of first dimension of input parameters do not match.");

        double[] basePrefWeight = pref.getBasePrefWeight();
        for (int dim=0; dim<basePrefWeight.length-1; dim++) {
        	if (basePrefWeight[dim]!=basePrefWeight[dim+1]) {
        		// if there are different PrefWeights, the normalization factors etc. calculated below will fail to produce actually normalized results
        		throw new IllegalArgumentException("The UnitedWeightInitializationStrategy is currently only implemented for equal basePrefWeights over all dimensions.");
        	}
        }

        int[][] prefItems = pref.getPrefItems(); 
        double[][] prefValues   = pref.getPrefValues();
		List<Integer[]> zeroNodes = new ArrayList<Integer[]>();
        for (int dim = 0; dim < weights.length; dim++) {
			/*
			 * count null-values in this dimension
			 */
			if (counts == null || counts[dim] == null || counts[dim].length == 0) {
				throw new IllegalArgumentException("The "+ZeroTolerantUnitedWeightInitializationStrategy.class+" depends on the counts in each dimension to reach a successfull normalization.");
			}
			for (int i = 0; i<counts[dim].length; i++) {
				if (counts[dim][i]==0) {
					zeroNodes.add(new Integer[]{dim, i});
					if (prefItems!=null && prefItems[dim]!=null) {
						for (int p=0; p<prefItems[dim].length; p++) {
							if (prefItems[dim][p] == i) {
								// Pref was given to an item that is isolated
								throw new IllegalArgumentException("Isolated Nodes must not receive preference weights!");
							}
						}
					}
				}
			}
        }
        
        
        /*
         * Initialize preference (p) Step 1
         * calculate overall count of items and preference
         */
        int itemCount  = 0; // number of items over all dimensions
        double prefSum = 0.0; // the sum of the actual preferences  over all elements and all dimensions
        for (int dim = 0; dim < weights.length; dim++) {
            itemCount += weights[dim].length;

            if (prefValues != null) {
                for (double prefValue:prefValues[dim]) {
                    prefSum += prefValue;
                }
            }
        }
        
        for (int dim = 0; dim < weights.length; dim++) {
            /*
             * Initialize weights (w):
             * every item gets the reciprocal of the overall number of items.
             */
            Arrays.fill(weights[dim], 1.0 / (itemCount-zeroNodes.size()));
    		for (Integer[] zeroNode: zeroNodes) {
    			weights[zeroNode[0]][zeroNode[1]] = 0;
    		}

            
            /* 
             * Initialize preference (p) Step 2
             * Calculate, how many additional weight is spread by the preference
             * FIXME: This should only work, if the basePrefWeight is the same in every dimension 
             * Otherwise we normalize each dimension separately, and could not guarantee, that the preferences sum up to one
             * => should check and throw exception if basePrefWeiths are different 
             */
            // 							= basePref in this dim * number of Items in the folksonomy + actual preference in the folksonomy
            //prefWeightsNormFactors[dim] = Math.abs(basePrefWeight[dim]) * itemCount + prefSum;
            prefWeightsNormFactors[dim] = basePrefWeight[dim] * (itemCount-zeroNodes.size()) + prefSum;
            
            /*
             * initialize the preference weights each item gets
             */
            if (basePrefWeight[dim] == 0.0) {
                /*
                 * prevent 0.0 / 0.0 = NaN
                 */
                prefWeights[dim] = 0.0;
            } else {
                prefWeights[dim] = basePrefWeight[dim] / prefWeightsNormFactors[dim];
            }
        }
    }

}
