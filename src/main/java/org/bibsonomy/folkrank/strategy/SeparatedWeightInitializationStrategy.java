package org.bibsonomy.folkrank.strategy;

import java.util.Arrays;

import org.bibsonomy.folkrank.data.FolkRankPref;

/** This strategy spreads over every dimension one unit of weight. This means 
 * that the (initial) weights sum up to one for every dimension (as do the 
 * prefWeights, when the preference is included).
 *    
 * @author rja
 */
public class SeparatedWeightInitializationStrategy implements WeightInitializationStrategy {

	/**
	 * @see org.bibsonomy.folkrank.strategy.WeightInitializationStrategy#initalizeWeights(org.semanticdesktop.nepomuk.comp.folkpeer.folkrank.data.FolkRankParam, double[][], double[], double[], int[][])
	 */
	public synchronized void initalizeWeights(FolkRankPref pref, double[][] weights, double[] prefWeights, double[] prefWeightsNormFactors, int[][] counts) {
		/*
		 * check input arguments
		 */
		if (weights.length != prefWeights.length || weights.length != prefWeightsNormFactors.length) 
			throw new IllegalArgumentException("Sizes of first dimension of input parameters do not match.");

		double[] basePrefWeight = pref.getBasePrefWeight(); // basepref in each dimension
		double[][] prefValues   = pref.getPrefValues();  // preference values for these nodes

		for (int dim = 0; dim < weights.length; dim++) {
			/*
			 * Initialize weights (w_0):
			 * every item gets the reciprocal of the number of items in the 
			 * dimensions it belongs to.
			 * Thus the weights in each dimension sum up to one, the total sum is the number of dimensions
			 */
			Arrays.fill(weights[dim], 1.0 / weights[dim].length);

			/*
			 * Initialize preferences (p)
			 * Calculate the sum of the actual preference weights.
			 */
			prefWeightsNormFactors[dim] = 0.0;
			if (prefValues != null) {
				for (double prefValue:prefValues[dim]) {
					prefWeightsNormFactors[dim] += prefValue;
				}
			}

			/*
			 * Calculate, how many additional weight is spread by the preference in this dimension = the total amount in this dimension
			 */
			// sum over all entries in p = weight each item gets * number of items + the summed weight of the preferenced items
			prefWeightsNormFactors[dim] = basePrefWeight[dim] * weights[dim].length + prefWeightsNormFactors[dim];

			/*
			 * initialize the preference weights each item gets (normalize it)
			 */
			if (basePrefWeight[dim] == 0.0) {
				/*
				 * prevent 0.0 / 0.0 = NaN
				 */
				prefWeights[dim] = 0.0;
			} else {
				prefWeights[dim] = basePrefWeight[dim] / prefWeightsNormFactors[dim];
			}
		}
	}

	public void adaptAlphaBetaGamma(double[] alpha, double[] beta, double[] gamma, int[][] prefNodes, double[] pWeights) {
	     /*
         * TODO: is this correct?
         * If we have no preference items, we ensure normalized vectors by 
         * adding gamma to alpha and beta.
         */
		for (int dim = 0; dim < alpha.length; dim++) {
			if (prefNodes == null || prefNodes[dim] == null || prefNodes[dim].length == 0) {
				if (pWeights[dim] == 0) {
					// if in this dimension the preference vector p contains all zeros, we set gamma = 0 and enlarge alpha and beta proportionally
					final double sum = alpha[dim] + beta[dim];
					alpha[dim] += (alpha[dim] * gamma[dim]) / sum;
					beta[dim]  += (beta[dim]  * gamma[dim]) / sum;
				}
			}
		}
	}

}
