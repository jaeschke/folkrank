package org.bibsonomy.folkrank.usecases;

import java.io.IOException;
import java.util.SortedSet;
import java.util.TreeSet;

import org.bibsonomy.folkrank.FolkRank;
import org.bibsonomy.folkrank.data.FolkRankData;
import org.bibsonomy.folkrank.data.FolkRankParam;
import org.bibsonomy.folkrank.data.FolkRankPref;
import org.bibsonomy.folkrank.data.FolkRankResult;
import org.bibsonomy.folkrank.map.LeavePostOutDataMapper;
import org.bibsonomy.folkrank.process.IntegerFileFactPreprocessor;

public class SpeedTest {

    public static void main(String[] args) throws IOException {
        /*
         * extract command line arguments
         */
        if (args.length < 4) {
            System.err.println("usage:");
            System.err.println("java " + SpeedTest.class.getName() + " K firstUser lastUser factDir inputUsers outputUsers");
            System.err.println("  -- computes for users in [firstUser, lastUser] the top K tags for resource from file inputUsers --");
            System.err.println("  where ");
            System.err.println("  K           is the number of top tags to output");
            System.err.println("  user        user id");
            System.err.println("  res         res id");
            System.err.println("  factDir     is the directory where the fact, user, tag, res files are");
            System.exit(1);
        }
        int argCtr = 0;
        final int topK = Integer.parseInt(args[argCtr++]);
        final int usr  = Integer.parseInt(args[argCtr++]);
        final int res  = Integer.parseInt(args[argCtr++]);
        final String dir = args[argCtr++];
        final String[] mappingFileNames = new String[]{dir + "tag", dir + "user", dir + "res"};

        /*
         * read data
         */
        System.err.println("reading facts");
        IntegerFileFactPreprocessor prep = new IntegerFileFactPreprocessor(dir + "fact", mappingFileNames);
        prep.setPrefItems(new String[][]{new String[]{}, new String[]{}, new String[]{}});
        prep.process();
        FolkRankData data = prep.getFolkRankData();

        /*
         * initialize FolkRank params
         */
        FolkRankParam param = new FolkRankParam();
        FolkRankPref pref = new FolkRankPref(new double[] {1.0, 1.0, 1.0});
        final int usrCounts = data.getCounts()[LeavePostOutDataMapper.U].length;
        final int resCounts = data.getCounts()[LeavePostOutDataMapper.R].length;
        final double[][] prefWeights = new double[][]{new double[]{}, new double[]{usrCounts}, new double[]{resCounts}};


        /*
         * outside: data starts with 1 ... in folkrank it starts with 0
         */
        final int r = res - 1;
        final int u = usr - 1;

        /*
         * set preference and compute
         */
        pref.setPreference(new int[][]{new int[]{}, new int[]{u}, new int[]{r}}, prefWeights);

        FolkRank folk = new FolkRank(param);
        System.err.println("start FR:  " + System.currentTimeMillis());
        FolkRankResult result = folk.compute(data, pref);
        System.err.println("finish FR: " + System.currentTimeMillis());

        System.err.println("needed " + result.getErrors().size() + " iterations");
        
        /*
         * output top k tags
         */
        // write resource id (in output notation: starting with 1)
        SortedSet<ItemWithWeight> topKTags = getTopK(data, result.getWeights(), topK, 0);
        for (ItemWithWeight item: topKTags) {
            // write tag id (in output notation: starting with 1)
            System.out.print(Integer.toString((item.item + 1)));
            // write weight
            System.out.print("(" + item.weight + ") ");
        }


    }


    private static SortedSet<ItemWithWeight> getTopK (FolkRankData facts, double[][] weights, int k, int dim) {
        double minWeight;
        SortedSet<ItemWithWeight> set = new TreeSet<ItemWithWeight>();

        minWeight = -100; // consider only items with positive weight 
        for (int item = 0; item < weights[dim].length; item++) {
            double currWeight = weights[dim][item];
            if (currWeight > minWeight) {
                /* new weight to consider found */
                set.add(new ItemWithWeight(item, currWeight));
                if (set.size() > k) {
                    // new best weight, since we have more than k items in set
                    ItemWithWeight last = set.last();
                    set.remove(last);
                    minWeight = set.last().weight;
                }
            }
        }

        return set;
    }

    /** Internal class which is used to represent the top-k items together with
     * their weight in a set when finding them.
     * 
     * @author rja
     */
    private static class ItemWithWeight implements Comparable<ItemWithWeight> {
        /**
         * An int which represents the item.
         */
        public int item;
        /**
         * The weight of the item.
         */
        public double weight;
        /** The only available constructor. 
         *  
         * @param item - a string which represents the item.
         * @param weight - the weight of the item.
         */
        public ItemWithWeight(int item, double weight) {
            super();
            this.item = item;
            this.weight = weight;
        }
        /**
         * Disabled default constructor. 
         */
        private ItemWithWeight() {
            /*
             * do nothing, since this constructor is not useable.
             */ 
        }

        /** Returns the item.
         * @return - the item.
         */
        public int getItem() {
            return item;
        }
        /** Returns the weight of the item.
         * @return - the weight of the item.
         */
        public double getWeight() {
            return weight;
        }

        /**
         * @see java.lang.Object#equals(java.lang.Object)
         */
        @Override
        public boolean equals(Object obj) {
            if (! (obj instanceof ItemWithWeight)) {
                return false;
            }
            return equals((ItemWithWeight) obj);
        }

        /** Two items are equal if there string representations are equal. This 
         * is true, since only items of the same dimension should be compared.
         * 
         * @param other - the item to compare with this item.
         * @return - true if this.item == other.item.
         */
        private boolean equals (ItemWithWeight other) {
            return this.item == other.item;
        }

        /** Compares two items by their weight. 
         * @param o - the other item to compare with this item.
         * @return - 0 if they're equal, -1/+1 otherwise.
         * 
         * @see java.lang.Comparable#compareTo(java.lang.Object)
         */
        public int compareTo(ItemWithWeight o) {
            if (o == null) { throw new NullPointerException(); }
            int sgn = (int) Math.signum(o.weight - this.weight);
            if (sgn != 0) {
                return sgn;
            } else {
                return o.item - this.item;
            }
        }
        /**
         * @see java.lang.Object#hashCode()
         */
        @Override
		public int hashCode () {
            return item;
        }
    }

}
