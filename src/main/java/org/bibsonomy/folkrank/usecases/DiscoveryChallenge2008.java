package org.bibsonomy.folkrank.usecases;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.SortedSet;
import java.util.TreeSet;

import org.apache.log4j.Logger;
import org.bibsonomy.folkrank.FolkRank;
import org.bibsonomy.folkrank.data.FolkRankData;
import org.bibsonomy.folkrank.data.FolkRankParam;
import org.bibsonomy.folkrank.data.FolkRankPref;
import org.bibsonomy.folkrank.data.FolkRankResult;
import org.bibsonomy.folkrank.data.ItemWithWeight;
import org.bibsonomy.folkrank.io.FactReadingException;
import org.bibsonomy.folkrank.io.FileFactReader;
import org.bibsonomy.folkrank.process.FactPreprocessor;
import org.bibsonomy.folkrank.process.FactReaderFactPreprocessor;

/**
 * 
 * @author:  rja
 * @version: $Id$
 * $Author$
 * 
 */
public class DiscoveryChallenge2008 {

	private static final Logger log = Logger.getLogger(DiscoveryChallenge2008.class);

	private static final int TAG = 0; // tag dimension
	private static final int USR = 1; // user dimension
	private static final int RES = 2; // resource dimension
	private static final int MAX_NO_OF_TAGS_TO_RECOMMEND = 30; // maximal number of tags to recommend

	private final BufferedReader reader;
	private final BufferedWriter writer;
	private final String trainFileName;
	
	public static void main(String[] args) throws FactReadingException, IOException {

		if (args.length < 2) {
			System.err.println("usage:");
			System.err.println("  java " + DiscoveryChallenge2008.class.getName() + " train_tags.txt test_tags.txt");
			System.exit(1);
		}

		/**
		 * format of file:
		 * tag|#|user|#|resource\n
		 */
		final String trainFileName = args[0];
		/**
		 * format of file:
		 * contentId userId resource\n
		 */
		final String testFileName = args[1];
		/**
		 * format of file:
		 * contentId tag tag tag
		 */
		final String resultFileName = testFileName + "_folkrank.tags";

		/*<
		 * configure input/output
		 */
		final BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(testFileName), "UTF-8"));
		final BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(resultFileName), "UTF-8"));
		

		final DiscoveryChallenge2008 challenge = new DiscoveryChallenge2008(reader, writer, trainFileName);
		
		challenge.writeRecommendations();
	}
	
	public DiscoveryChallenge2008(final BufferedReader reader, final BufferedWriter writer, final String trainFileName) {
		this.reader = reader;
		this.writer = writer;
		this.trainFileName = trainFileName;
	}
	
	
	public void writeRecommendations() throws FactReadingException, IOException {
		/*
		 * read training data (and store inverse mapping!)
		 */
		final FactPreprocessor processor = new FactReaderFactPreprocessor(new FileFactReader(trainFileName, "|#|"), true);
		/*
		 * process facts
		 */
		log.info("processing facts");
		processor.process();
		/*
		 * get FolkRankData object
		 */
		final FolkRankData factsData = processor.getFolkRankData();
		/*
		 * get number of resources/users
		 */
		final int numberOfResources = factsData.getCounts()[RES].length;
		final int numberOfUsers     = factsData.getCounts()[USR].length;
		final int numberOfTags      = factsData.getCounts()[TAG].length;
		log.info("got " + numberOfTags + " tags, " + numberOfUsers + " users, and " + numberOfResources + " resources");
		/*
		 * initialize parameters for FolkRank
		 */
		final FolkRankParam param = new FolkRankParam();
		/*
		 * create a new FolkRank instance
		 */
		final FolkRank folkrank = new FolkRank(param);
		/*
		 * initialize preference
		 */
		final FolkRankPref pref = new FolkRankPref(new double[] {1.0, 1.0, 1.0});
		/*
		 * generate tag recommendations for each resource in the test data set
		 */
		String line;
		log.info("starting to read file containing user/resource pairs to generate recommendations for");
		while ((line = reader.readLine()) != null) {
			/*
			 * format:
			 * contentId userId resource\n
			 */
			final String[] parts = line.trim().split("\\s+");
			/*
			 * set preference
			 */
			final int[][] prefItems = new int[][]{{},{},{}};
			final double[][] prefValues = new double[][]{{},{},{}};
			pref.setPreference(prefItems, prefValues);
			/*
			 * get integer keys for user and set pref
			 */
			final Integer user = factsData.getNodeMap().getInverseMapping(USR, parts[1]);
			if (user != null) {
				prefItems[USR]  = new int[]{user};
				prefValues[USR] = new double[]{numberOfUsers};
			}
			/*
			 * get integer keys for resource and set pref
			 */
			final Integer resource = factsData.getNodeMap().getInverseMapping(RES, parts[2]);
			if (resource != null) {
				prefItems[RES]  = new int[]{resource};
				prefValues[RES] = new double[]{numberOfResources};
			}
			log.debug("found user " + parts[1] + " (= " + user + ") and resource " + parts[2] + " (= " + resource + ")");
			/*
			 * do computation
			 */
			final FolkRankResult result = folkrank.computeFolkRank(factsData, pref);
			/*
			 * sort result
			 */
			final SortedSet<ItemWithWeight<String>> sortedTags = sortTags(result, factsData.getNodeMap().getMapping(TAG));
			/*
			 * write result
			 */
			writeResult(parts[0], sortedTags);
		}
		reader.close();
		writer.close();
		log.info("finished");
	}
	
	
	/** Writes one line of results in the format
	 * <pre>
	 * contentId tag1 tag2 tag3 ...
	 * ...
	 * </pre>
	 * @param contentId
	 * @param sortedTags
	 * @throws IOException
	 */
	private void writeResult(final String contentId, final SortedSet<ItemWithWeight<String>> sortedTags) throws IOException {
		log.debug("writing result");
		writer.write(contentId);
		for (final ItemWithWeight<String> item: sortedTags) {
			writer.write(" " + item.item);
		}
		writer.write("\n");
	}

	
	/** Sorts all tags from the result and maps them back to strings.
	 * Returns only the top {@value #MAX_NO_OF_TAGS_TO_RECOMMEND} tags. 
	 * 
	 * @param result
	 * @param inverseMapping
	 * @return
	 */
	private SortedSet<ItemWithWeight<String>> sortTags(final FolkRankResult result, final String[] inverseMapping) {
		final SortedSet<ItemWithWeight<String>> set = new TreeSet<ItemWithWeight<String>>();
		final double[] tagWeights = result.getWeights()[TAG];
		double minWeight = -100;

		for (int item = 0; item < tagWeights.length; item++) {
			double currWeight = tagWeights[item];
			if (currWeight > minWeight) {
				final String tag = inverseMapping[item];
				set.add(new ItemWithWeight<String>(tag, currWeight));
				if (set.size() > MAX_NO_OF_TAGS_TO_RECOMMEND) {
					// new best weight, since we have more than k items in set
					final ItemWithWeight<String> last = set.last();
					set.remove(last);
					minWeight = set.last().weight;
				}
			}
		}

		return set;
	}

}

