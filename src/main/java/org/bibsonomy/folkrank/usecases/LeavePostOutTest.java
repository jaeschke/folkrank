package org.bibsonomy.folkrank.usecases;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.SortedSet;
import java.util.TreeSet;

import org.apache.log4j.Logger;
import org.bibsonomy.folkrank.FolkRank;
import org.bibsonomy.folkrank.data.FolkRankData;
import org.bibsonomy.folkrank.data.FolkRankParam;
import org.bibsonomy.folkrank.data.FolkRankPref;
import org.bibsonomy.folkrank.data.FolkRankResult;
import org.bibsonomy.folkrank.data.IntItemWithWeight;
import org.bibsonomy.folkrank.map.LeavePostOutDataMapper;
import org.bibsonomy.folkrank.process.IntegerFileFactPreprocessor;

public class LeavePostOutTest {

    private static final Logger log = Logger.getLogger(LeavePostOutTest.class);
    
    private static final double BASE_PREF_ZERO = 0;
    
    public static void main(String[] args) throws IOException {
        /*
         * extract command line arguments
         */
        if (args.length < 6) {
            System.err.println("usage:");
            System.err.println("java " + LeavePostOutTest.class.getName() + " K firstUser lastUser factDir inputUsers outputUsers");
            System.err.println("  -- computes for users in [firstUser, lastUser] the top K tags for resource from file inputUsers --");
            System.err.println("  where ");
            System.err.println("  K           is the number of top tags to output");
            System.err.println("  firstuser   is the first user for which to compute a ranking");
            System.err.println("  lastUser    is the last user for which to compute a ranking");
            System.err.println("  factDir     is the directory where the fact, user, tag, res files are");
            System.err.println("  inputUsers  is a file which contains for each user (line) one resource id");
            System.err.println("  outputUsers is a file which will be written and contains for each user (line) a resource id and the top K tags from folkrank");
            System.exit(1);
        }
        int argCtr = 0;
        final int topK = Integer.parseInt(args[argCtr++]);
        final int firstUser = Integer.parseInt(args[argCtr++]);
        final int lastUser  = Integer.parseInt(args[argCtr++]);
        final String dir = args[argCtr++];
        final String inputUsersFileName = args[argCtr++];
        final String outputUsersFileName = args[argCtr++];
        final String[] mappingFileNames = new String[]{dir + "tag", dir + "user", dir + "res"};
        
        /*
         * read data
         */
        System.err.println("reading facts");
        final IntegerFileFactPreprocessor prep = new IntegerFileFactPreprocessor(dir + "fact", mappingFileNames);
        prep.setPrefItems(new String[][]{new String[]{}, new String[]{}, new String[]{}});
        prep.process();
        final FolkRankData data = prep.getFolkRankData();
        
        /*
         * writes top k tags for each user
         */
        final BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outputUsersFileName), "UTF-8"));
        final BufferedWriter writerAPR = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outputUsersFileName + "_APR"), "UTF-8"));
        /*
         * reads resource id for each user
         */
        final BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(inputUsersFileName), "UTF-8"));

        /*
         * initialize FolkRank params
         */
        final FolkRankParam param = new FolkRankParam();
        /*
         * new baseline, rja, 2010-05-10 (adapted to original paper)
         */
        final FolkRankParam baseParam = new FolkRankParam();
        /*
         * we don't give any preference in the baseline
         */
        baseParam.setAlphaBetaGamma(0, 1, 0);
        /*
         * all nodes get zero preference, except ...
         */
        final FolkRankPref pref = new FolkRankPref(new double[] {BASE_PREF_ZERO, BASE_PREF_ZERO, BASE_PREF_ZERO});
        /*
         * ... the preference nodes, which get preference 1
         */
        final double[][] prefWeights = new double[][]{new double[]{}, new double[]{1}, new double[]{1}};

        
        String line = null;
        int userId = 0;
        /*
         * iterate over resources for each user
         */
        while ((line = reader.readLine()) != null) {
            userId++;

            /*
             * ignore users which lay outside the given window [firstUser, lastUser]
             */
            if (userId < firstUser || userId > lastUser) continue;

            
            line = line.trim();
            
            /*
             * has user enough posts and has suitable resource been found?
             */
            if (line.equals("") || line.equals("-1")) {
                // NO: ignore user
                writer.newLine();
                writerAPR.newLine();
                continue;
            }

            /*
             * outside: data starts with 1 ... in folkrank it starts with 0
             */
            final int r = Integer.parseInt(line) - 1;
            final int u = userId - 1;


            final LeavePostOutDataMapper mapper = new LeavePostOutDataMapper(u, r);

            /*
             * copy the facts
             */
            final FolkRankData data2 = mapper.mapData(data);                

            /*
             * set preference and compute
             */
            pref.setPreference(new int[][]{new int[]{}, new int[]{u}, new int[]{r}}, prefWeights);

            final FolkRank folk = new FolkRank(baseParam, param);
            log.info("starting folkrank computation for user " + userId);
            final FolkRankResult result = mapper.mapResult(folk.computeFolkRank(data2, mapper.mapPrefs(pref)));
            log.info("finished folkrank computation for user " + userId);
            SortedSet<IntItemWithWeight> topKTags = getTopK(data2, result.getWeights(), topK, 0);
            log.info("finished sorting results for user " + userId);
            
            /*
             * output top k tags
             */
            // write resource id (in output notation: starting with 1)
            writer.write(Integer.toString(r + 1) + ": ");
            for (IntItemWithWeight item: topKTags) {
                // write tag id (in output notation: starting with 1)
                writer.write(Integer.toString((item.item + 1)));
                // write weight
                writer.write("(" + item.weight + ") ");
            }
            
            /*
             * output top k tags of adapted PageRank computation 
             */
            // write resource id (in output notation: starting with 1)
            writerAPR.write(Integer.toString(r + 1) + ": ");
            topKTags = getTopK(data2, result.getAPRWeights(), topK, 0);
            for (IntItemWithWeight item: topKTags) {
                // write tag id (in output notation: starting with 1)
                writerAPR.write(Integer.toString((item.item + 1)));
                // write weight
                writerAPR.write("(" + item.weight + ") ");
            }
            
            
            writer.newLine();
            writerAPR.newLine();
            
            // write user id and computation error to stderr
            System.err.println(u + ": " + result.getErrors());
        }
        writer.close();
        writerAPR.close();

    }


    private static SortedSet<IntItemWithWeight> getTopK (FolkRankData facts, double[][] weights, int k, int dim) {
        double minWeight;
        SortedSet<IntItemWithWeight> set = new TreeSet<IntItemWithWeight>();

        minWeight = -100; // consider only items with positive weight 
        for (int item = 0; item < weights[dim].length; item++) {
            double currWeight = weights[dim][item];
            if (currWeight > minWeight) {
                /* new weight to consider found */
                set.add(new IntItemWithWeight(item, currWeight));
                if (set.size() > k) {
                    // new best weight, since we have more than k items in set
                    IntItemWithWeight last = set.last();
                    set.remove(last);
                    minWeight = set.last().weight;
                }
            }
        }

        return set;
    }

   
}
