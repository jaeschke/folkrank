package org.bibsonomy.folkrank.usecases;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.SortedSet;

import org.bibsonomy.folkrank.FolkRank;
import org.bibsonomy.folkrank.data.FolkRankData;
import org.bibsonomy.folkrank.data.FolkRankParam;
import org.bibsonomy.folkrank.data.FolkRankPref;
import org.bibsonomy.folkrank.data.FolkRankResult;
import org.bibsonomy.folkrank.data.ItemWithWeight;
import org.bibsonomy.folkrank.io.FactReader;
import org.bibsonomy.folkrank.io.FactReadingException;
import org.bibsonomy.folkrank.io.FileFactReader;
import org.bibsonomy.folkrank.process.FactReaderFactPreprocessor;
import org.bibsonomy.folkrank.process.HelperMethods;

/**
 * Reads strings of the form
 * 
 * {0, {'boomerang' 5}, 0, {}, 0, {}, 0.2, 0.5, 0.3, 1e-4, 50, 1}
 * 
 * (like in the original Matlab code) and computes a ranking
 * for each such line. 
 * 
 * The purpose of this class is to compare the Java-based FolkRank with
 * the Matlab implementation.
 * 
 * @author rja
 *
 */
public class FolkRankComparison {

	private static final String DEFAULT_FACT_DELIM = "|#|";

	private static final int TOP_K = 100;

	private static final int DIMENSIONS = 3;

	private static final String RESULT_DELIM = "=========== ";

	private final FolkRankData folkRankData;

	public FolkRankComparison(final FactReader<String> factReader) {
		final FactReaderFactPreprocessor prep = new FactReaderFactPreprocessor(factReader, true);
		prep.process();
		this.folkRankData = prep.getFolkRankData();
	}
	
	public FolkRankComparison(final String factFile, final String factDelim) throws FactReadingException, FileNotFoundException {
		this(new FileFactReader(factFile, factDelim));
	}
	
	public FolkRankComparison(final String factFile) throws FactReadingException, FileNotFoundException {
		this(factFile, DEFAULT_FACT_DELIM); 
	}
	
	/**
	 * 
	 * 
	 * @param args - expects as first argument of path to the fact file (with delimiter |#|)
	 * @throws FactReadingException
	 * @throws IOException
	 */
	public static void main(String[] args) throws FactReadingException, IOException {
		/*
		 * first argument: path to fact file
		 */
		System.err.println("reading facts");
		final FolkRankComparison frc = new FolkRankComparison(args[0]);
		/*
		 * input on stdin:
		 * 
		 * { 0, {'boomerang', 5 }, 0, {}, 0, {}, 0.2, 0.5, 0.3, 1e-4, 50, 1 }
		 */
		final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		String line;
		System.err.println("please enter your ranking requests");
		while ((line = reader.readLine()) != null) {
			if (line.startsWith("#")) continue;
			/*
			 * print params
			 */
			System.out.println(RESULT_DELIM + line);
			/*
			 * compute FolkRank
			 */
			final FolkRankResult result = frc.process(line);
			/*
			 * print adapted PageRank (APR)
			 */
			System.out.println(RESULT_DELIM + "PageRank");
			printResult(HelperMethods.getTopK(frc.getFolkRankData(), result.getAPRWeights(), TOP_K));
			/*
			 * print FolkRank
			 */
			System.out.println(RESULT_DELIM + "FolkRank");
			printResult(HelperMethods.getTopK(frc.getFolkRankData(), result.getWeights(), TOP_K));
			
			// next run
			System.out.println();
		}
		reader.close();
	}
	
	public SortedSet<ItemWithWeight<String>>[] getTopK(final String line, final int k) throws IOException, FactReadingException {
		return HelperMethods.getTopK(folkRankData, process(line), k);
	}
	

	public static void printResult(final SortedSet<ItemWithWeight<String>>[] topK) {
		for (int dim = 0; dim < DIMENSIONS; dim++) {
			System.out.println(RESULT_DELIM + " dim" + dim);
			for (final ItemWithWeight<String> iww: topK[dim]) {
				System.out.println(iww.getItem() + " " + iww.getWeight());
			}
		}
	}

	/**
	 * @line input of the form
	 * 
	 * <pre>
	 * {0, {'boomerang' 5}, 0, {}, 0, {}, 0.2, 0.5, 0.3, 1e-4, 50, 1}
	 * </pre>
	 * <ul>
	 * <li>tagBase
	 * <li>tagPref
	 * <li>userBase
	 * <li>userPref
	 * <li>resBase
	 * <li>resPref
	 * <li>alpha
	 * <li>beta
	 * <li>gamma
	 * <li>epsilon
	 * <li>maxIter
	 * </ul>
	 */
	public FolkRankResult process(final String line) throws IOException, FactReadingException {
		/*
		 * process input
		 */
		final String[] parts = line.substring(1, line.length() - 1).trim().split(",\\s");
		assert parts.length == 12;
		/*
		 * get preferences
		 */
		final double[] basePref = new double[DIMENSIONS];
		final int[][] prefItems = new int[DIMENSIONS][];
		final double[][] prefValues = new double[DIMENSIONS][];
		int partCtr = 0;
		for (int dim = 0; dim < DIMENSIONS; dim++) {
			basePref[dim] = Double.parseDouble(parts[partCtr++]);
			final String prefItemsValues = parts[partCtr++];
			assert prefItemsValues.charAt(0) == '{';
			assert prefItemsValues.charAt(prefItemsValues.length() - 1) == '}';
			// remove leading and trailing {} and split at whitespace
			final String[] prefItemsValuesParts = prefItemsValues.substring(1, prefItemsValues.length() - 1).trim().split("\\s");
//			assert prefItemsValuesParts.length % 2 == 0;
			prefItems[dim] = new int[prefItemsValuesParts.length / 2];
			prefValues[dim] = new double[prefItems.length];
			/*
			 *	set preference
			 */
			for (int i = 0; i < prefItems[dim].length; i++) {
				final String item = prefItemsValuesParts[i*2];
				// remove ''
				prefItems[dim][i] = folkRankData.getNodeMap().getInverseMapping(dim, item.substring(1, item.length() - 1));
				prefValues[dim][i] = Double.parseDouble(prefItemsValuesParts[i*2 + 1]);
			}
		}
		/*
		 * get parameters
		 */
		final FolkRankParam param = new FolkRankParam();
		param.setAlphaBetaGamma(
				Double.parseDouble(parts[partCtr++]), // alpha
				Double.parseDouble(parts[partCtr++]), // beta
				Double.parseDouble(parts[partCtr++])  // gamma
		);
		param.setStopCondition(
				Double.parseDouble(parts[partCtr++]), // epsilon
				Integer.parseInt(parts[partCtr++])    // maxIter
		);


		final FolkRankPref pref = new FolkRankPref(basePref);
		pref.setPreference(prefItems, prefValues);

		return new FolkRank(param).computeFolkRank(folkRankData, pref);
	}
 
	public FolkRankData getFolkRankData() {
		return folkRankData;
	}

}
