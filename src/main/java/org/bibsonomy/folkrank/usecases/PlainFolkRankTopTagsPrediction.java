package org.bibsonomy.folkrank.usecases;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.SortedSet;
import java.util.TreeSet;

import org.apache.log4j.Logger;
import org.bibsonomy.folkrank.FolkRank;
import org.bibsonomy.folkrank.data.FolkRankData;
import org.bibsonomy.folkrank.data.FolkRankParam;
import org.bibsonomy.folkrank.data.FolkRankPref;
import org.bibsonomy.folkrank.data.FolkRankResult;
import org.bibsonomy.folkrank.data.IntItemWithWeight;
import org.bibsonomy.folkrank.io.FactReadingException;
import org.bibsonomy.folkrank.io.FileFactReader;
import org.bibsonomy.folkrank.process.FactPreprocessor;
import org.bibsonomy.folkrank.process.FactReaderFactPreprocessor;

public class PlainFolkRankTopTagsPrediction {

	private static final Logger log = Logger.getLogger(PlainFolkRankTopTagsPrediction.class);

	private static final double COMMON_BASE_PREF_WEIGHT = 0;
	private static final String TEST_FILE_DELIM = ":";
	private static final String TRAIN_FILE_DELIM = "|#|";
	private static final String RESULT_FILE_DELIM = ":";
	/*
	 *  These three parameters control the order of the dimensions 
	 *  (e.g. the dimension that contains the users is Udim)
	 *  The input file for training data must match this order
	 */
	private static final short Udim = 0;
	private static final short Rdim = 1;
	private static final short Tdim = 2;

	public static void main(String[] args) throws IOException, FactReadingException {
		/*
		 * extract command line arguments
		 */
		if (args.length < 4) {
			System.err.println("usage:");
			System.err.println("java " + PlainFolkRankTopTagsPrediction.class.getName() + " K firstUser lastUser factDir inputUsers outputUsers");
			System.err.println("  -- computes for user-resource pairs in testdata the top K tags --");
			System.err.println("  where ");
			System.err.println("  K           is the number of top tags to output");
			System.err.println("  trainFileName     is the directory where the training file is (facts)");
			System.err.println("  testFileName  is a file which contains user resource pairs");
			System.err.println("  predictionFileName is a file which will be written and contains for each user-resource-pair the top K tags from folkrank");
			System.exit(1);
		}
		int argCtr = 0;
		final int topK = Integer.parseInt(args[argCtr++]);
		final String trainFileName = args[argCtr++];
		final String testFileName = args[argCtr++];
		final String predictionFileName = args[argCtr++];

		/*
		 * read training data and init folkrank data
		 */
		System.err.println("reading facts");
		final FactPreprocessor processor = new FactReaderFactPreprocessor(new FileFactReader(trainFileName, TRAIN_FILE_DELIM), true);
		processor.process();
		FolkRankData frData = processor.getFolkRankData();
		int numUsers = frData.getCounts()[Udim].length;
		int numResources = frData.getCounts()[Rdim].length;


		/*
		 * Prepare Writer for Output of the top k tags for each test (user-resource)-pair
		 */
		final BufferedWriter outWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(predictionFileName), "UTF-8"));

		/*
		 * Prepare Reader to read the test pairs 
		 */
		final BufferedReader trainReader = new BufferedReader(new InputStreamReader(new FileInputStream(testFileName), "UTF-8"));

		/*
		 * initialize FolkRank params
		 */
		final FolkRankParam param = new FolkRankParam();
		final FolkRankParam baseParam = new FolkRankParam();
		baseParam.setAlphaBetaGamma(0, 1, 0);
		/*
		 * all nodes get 1 unit preference
		 */
		final FolkRankPref pref = new FolkRankPref(new double[] {COMMON_BASE_PREF_WEIGHT, COMMON_BASE_PREF_WEIGHT, COMMON_BASE_PREF_WEIGHT});

		/*
		 * init FolkRank with test independent parameters
		 */
		final FolkRank folk = new FolkRank(baseParam, param);


		String line = null;
		/*
		 * iterate over the (user, resource)-pairs
		 */
		while ((line = trainReader.readLine()) != null) {

			/*
			 * read in user and resourcehash
			 */
			String[] parts = line.split(TEST_FILE_DELIM);
			String userId = parts[0].trim();
			String resId = parts[2].trim();
			String contentId = parts[1].trim();

			Integer u = frData.getNodeMap().getInverseMapping(Udim, userId);
			Integer r = frData.getNodeMap().getInverseMapping(Rdim, resId);
			if (u == null || r == null) {
				// we have a new user or a new url
				// we can not apply the regular folkrank strategy
				log.error("User "+userId + " or Resource "+resId + " not found. Avoiding ColdStart Problem by not recommending.");
				/*
				 * TODO: discuss behavior for coldstart
				 * especially important for task 1
				 */
				continue;
			}
			
			
			final int[][] prefItems = new int[][]{{},{},{}};
			final double[][] prefValues = new double[][]{{},{},{}};
			pref.setPreference(prefItems, prefValues);
			prefItems[Udim]  = new int[]{u};
			prefValues[Udim] = new double[]{numUsers};
			prefItems[Rdim] = new int[]{r};
			prefValues[Rdim] = new double[]{numResources};

			/*
			 * set preference and compute
			 */
			pref.setPreference(prefItems, prefValues);

			log.info("starting folkrank computation for user " + userId);
			final FolkRankResult result = folk.computeFolkRank(frData, pref);

			log.info("finished folkrank computation for user " + userId);
			SortedSet<IntItemWithWeight> topKTags = getTopK(result.getWeights(), topK, Tdim);
			log.info("finished sorting results for user " + userId);

			/*
			 * output top k tags
			 */
			// write resource id (in output notation: starting with 1)
			outWriter.write(contentId);
			for (IntItemWithWeight item: topKTags) {
				// write tag id (in output notation: starting with 1)
				outWriter.write(RESULT_FILE_DELIM + frData.getNodeMap().getMapping(Tdim)[item.getItem()]);
			}
			outWriter.newLine();

			// write user id and computation error to stderr
			System.err.println(u + ": " + result.getErrors());
		}
		outWriter.close();
	}


	private static SortedSet<IntItemWithWeight> getTopK (double[][] weights, int k, int dim) {
		double minWeight;
		SortedSet<IntItemWithWeight> set = new TreeSet<IntItemWithWeight>();

		minWeight = -100; // consider only items with positive weight 
		for (int item = 0; item < weights[dim].length; item++) {
			double currWeight = weights[dim][item];
			if (currWeight > minWeight) {
				/* new weight to consider found */
				set.add(new IntItemWithWeight(item, currWeight));
				if (set.size() > k) {
					// new best weight, since we have more than k items in set
					IntItemWithWeight last = set.last();
					set.remove(last);
					minWeight = set.last().weight;
				}
			}
		}

		return set;
	}


}
