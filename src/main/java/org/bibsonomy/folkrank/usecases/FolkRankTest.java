package org.bibsonomy.folkrank.usecases;

import java.sql.SQLException;

import org.bibsonomy.folkrank.FolkRank;
import org.bibsonomy.folkrank.data.FolkRankData;
import org.bibsonomy.folkrank.data.FolkRankParam;
import org.bibsonomy.folkrank.data.FolkRankPref;
import org.bibsonomy.folkrank.data.FolkRankResult;
import org.bibsonomy.folkrank.data.NodeMap;
import org.bibsonomy.folkrank.io.DBFactReader;
import org.bibsonomy.folkrank.io.DBRankWriter;
import org.bibsonomy.folkrank.io.FactReader;
import org.bibsonomy.folkrank.io.FactReadingException;
import org.bibsonomy.folkrank.io.FactWritingException;
import org.bibsonomy.folkrank.process.FactPreprocessor;
import org.bibsonomy.folkrank.process.FactReaderFactPreprocessor;
import org.bibsonomy.folkrank.strategy.SeparatedWeightInitializationStrategy;

public class FolkRankTest {

    private static long last = System.currentTimeMillis();
    
    public static void main(String[] args) throws FactReadingException, InterruptedException, FactWritingException, SQLException {

        
        
        FactReader<String> reader;
        FactPreprocessor prep;
        FolkRankData facts;
        FolkRankParam param;
        FolkRankPref pref;
        FolkRankResult result;
        
        /*
         * normales Ranking
         */
        String query = "(SELECT LOWER(t.tag_name), t.user_name, CONCAT(t.content_type,b.book_url_hash) AS resource " +
                       "  FROM tas t " +
                       "    JOIN bookmark b USING (content_id) " +
                       "  WHERE t.group = 0 AND t.content_type = 1 AND t.user_name != 'dblp' AND t.tag_name != 'imported' AND t.tag_name != 'system:imported' LIMIT 100000000)" +
                       "UNION ALL" +
                       "(SELECT LOWER(t.tag_name), t.user_name, CONCAT(t.content_type,b.simhash1) AS resource " +
                       "  FROM tas t " +
                       "    JOIN bibtex b USING (content_id) " +
                       "  WHERE t.group = 0 AND t.content_type = 2 AND t.user_name != 'dblp' AND t.tag_name != 'imported' AND t.tag_name != 'system:imported' LIMIT 100000000)";
        
        /*
         * HIER FLICKR-Ranking: Content_ID steht mit dran!
         */
        @SuppressWarnings("unused")
        String query1 = "(SELECT LOWER(t.tag_name), t.user_name, CONCAT(t.content_type,b.book_url_hash, '_', t.content_id) AS resource " +
        "  FROM tas t " +
        "    JOIN bookmark b USING (content_id) " +
        "  WHERE t.group = 0 AND t.content_type = 1 AND t.user_name != 'dblp' AND t.tag_name != 'imported' AND t.tag_name != 'system:imported' LIMIT 100000000)" +
        "UNION ALL" +
        "(SELECT LOWER(t.tag_name), t.user_name, CONCAT(t.content_type,b.simhash1, '_', t.content_id) AS resource " +
        "  FROM tas t " +
        "    JOIN bibtex b USING (content_id) " +
        "  WHERE t.group = 0 AND t.content_type = 2 AND t.user_name != 'dblp' AND t.tag_name != 'imported' AND t.tag_name != 'system:imported' LIMIT 100000000)";

        // read from data base
        reader = new DBFactReader(null, query, 3);
        //reader = new MySQLFactReader(, 3306, "bibsonomy", "bibsonomy", "", query, 3);
        
        // preprocess
        prep = new FactReaderFactPreprocessor(reader);
        
        // extract array
        // facts = prep.getFolkRankData();
        
        // write as string to file
        // writer = new StringFileFactWriter("facts.txt", "|#|");
        // writer.write(facts);

        // write as integer to file
        // writer = new IntegerFileFactWriter("facts", new String[]{"tag", "usr", "res"});
        // writer.write(facts);
        
        
        
        // read as string
        // reader = new FileFactReader("facts.txt", "|#|");
        
        // preprocess
        // prep = new FactReaderFactPreprocessor(reader);
        // prep = new MemorySavingFactReaderFactPreprocessor(reader);

        
        // read as integer
        // prep = new IntegerFileFactPreprocessor("data/fact", new String[] {"data/tag", "data/user", "data/res"});

        
        /*
         * set prefitems to rank (in this case some tags)
         */
        // String[] tags = new String[]{"boomerang", "semantic", "semanticweb", "politics", "german"};
        
        // prep.setPrefItems(new String[][]{tags, new String[]{}, new String[]{}});
        // prep.setPrefItems(new String[][]{new String[]{"merseburg"}, new String[]{}, new String[]{}});

        
        prep.process();
        
        // extract array
        facts = prep.getFolkRankData();

        // write as integer to file
        // writer = new IntegerFileFactWriter("_facts", new String[]{"_tag", "_usr", "_res"});
        // writer.write(facts);

        
        // compute FolkRank
        
        pref = new FolkRankPref(new double[] {1.0, 1.0, 1.0});
        // boomerang
        //pref.setPreference(new int[][]{new int[]{28163}, new int[]{}, new int[]{}}, new double[][]{new double[]{2.2835e+05},new double[]{},new double[]{}});
        //pref.setPreference(new int[][]{new int[]{3}, new int[]{}, new int[]{}}, new double[][]{new double[]{5.0},new double[]{},new double[]{}});
        // none
        //pref.setPreference(new int[][]{new int[]{}, new int[]{}, new int[]{}}, new double[][]{new double[]{},new double[]{},new double[]{}});
        param = new FolkRankParam();
        param.setAlphaBetaGamma(0.0, 0.6, 0.4);
        param.setStopCondition(10e-5, 50);
        param.setWeightInitializationStrategy(new SeparatedWeightInitializationStrategy());
        
        FolkRank folk = new FolkRank(param);

        
        /*
         * iterate over all dimensions
         */
        
        final NodeMap nodeMap = facts.getNodeMap();
         // initialize pref array
        int[][] prefItems = new int[facts.getDims()][];
        double[][] prefValues = new double[facts.getDims()][];
        pref.setPreference(prefItems, prefValues);
        //MySQLRankWriter rwriter = new MySQLRankWriter("gandalf", "3306", "bibsonomy", "bibsonomy", "12bibkde");
        DBRankWriter rwriter = new DBRankWriter();
        
        last = System.currentTimeMillis();
        for (int dim = 0; dim < 3; dim++) { // mapping.length; dim++) { //ONLY 
        	final String[] mapping = nodeMap.getMapping(dim);
        	for (int dimA = 0; dimA < facts.getDims(); dimA++) {
                if (dim != dimA) {
                    prefItems[dimA] = new int[]{};
                    prefValues[dimA] = new double[]{};
                } else {
                    prefItems[dimA] = new int[1];
                    prefValues[dimA] = new double[]{mapping.length};
                }
            }
            for (int item = 0; item < mapping.length; item++) {
                //set pref for current item
                prefItems[dim][0] = item;
                System.out.print(dim + " " + mapping[item] + ": compute ... ");
                result = folk.computeFolkRank(facts, pref);
                System.out.print(pastTime() + ", write ... ");
                try {
                    rwriter.writeTopK(facts, result, pref, param, 100);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                System.out.println(pastTime());
            }
        }
        
        /*
         * rank merseburg
         */
        /*
        int[][] prefItems = prep.getPrefItems();
        int[][] iterPrefItems = new int[prefItems.length][];
        for (int dim = 0; dim < iterPrefItems.length; dim++) {
            iterPrefItems[dim] = new int[]{};
        }
        /*
         * boomerang
         */
        /*System.out.println("merseburg");
        iterPrefItems[0] = new int[] {prefItems[0][0]};
        pref.setPreference(iterPrefItems, new double[][]{new double[]{1000000.0}, new double[]{}, new double[]{}});
        result = folk.computeFolkRank(facts, pref);
        orders = ResultPostProcessor.sort(result);
        ResultPostProcessor.printTopK(facts, result, orders, "_r_merseburg", 100);
        orders = null;
       
        
        
        /*MySQLRankWriter rwriter = new MySQLRankWriter("gandalf", "3306", "bibsonomy", "bibsonomy", "12bibkde");
        
        try {
            rwriter.writeTopK(facts, result, pref, param, 4);
        } catch (SQLException e) {
            e.printStackTrace();
        }
         */        
        
        
    }

    private static long pastTime() {
        long past = (System.currentTimeMillis() - last) / 1000;
        last = System.currentTimeMillis();
        return past;
    }

}
