package org.bibsonomy.folkrank.usecases;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.bibsonomy.folkrank.FolkRank;
import org.bibsonomy.folkrank.data.FolkRankData;
import org.bibsonomy.folkrank.data.FolkRankParam;
import org.bibsonomy.folkrank.data.FolkRankPref;
import org.bibsonomy.folkrank.data.FolkRankResult;
import org.bibsonomy.folkrank.data.IntItemWithWeight;
import org.bibsonomy.folkrank.io.FactReadingException;
import org.bibsonomy.folkrank.io.FileFactReader;
import org.bibsonomy.folkrank.process.FactPreprocessor;
import org.bibsonomy.folkrank.process.FactReaderFactPreprocessor;

public class TagRecoNicPredictionsTask1 {

	private static final Logger log = Logger.getLogger(TagRecoNicPredictionsTask1.class);

//	private static final double COMMON_BASE_PREF_WEIGHT = 0;
	private static final double COMMON_BASE_PREF_WEIGHT = 1;
	private static final String TEST_FILE_DELIM = "\\Q|#|\\E";
	private static final String TRAIN_FILE_DELIM = "|#|";
	private static final String TEST_FILE_WORD_DELIM = " ";
	private static final String RESULT_FILE_DELIM = " ";
	private static final Pattern pat = Pattern.compile("^([^\\(]*)\\(([^\\)]*)\\)");

	/*
	 *  These three parameters control the order of the dimensions 
	 *  (e.g. the dimension that contains the users is Udim)
	 *  The input file for training data must match this order
	 */
	private static final short Udim = 0;
	private static final short Rdim = 1;
	private static final short Tdim = 2;

	public static void main(String[] args) throws IOException, FactReadingException {
		long beginTime  = System.currentTimeMillis();
		/*
		 * extract command line arguments
		 */
		if (args.length < 6) {
			log.error("Invalid number of arguments");
			System.err.println("usage:");
			System.err.println("java " + TagRecoNicPredictionsTask1.class.getName() + " K firstUser lastUser factDir inputUsers outputUsers");
			System.err.println("  -- computes for user-resource pairs in testdata the top K tags --");
			System.err.println("  where ");
			System.err.println("  K           is the number of top tags to output");
			System.err.println("  trainFileName     is the directory where the training file is (facts)");
			System.err.println("  testFileName  is a file which contains user resource pairs");
			System.err.println("  predictionFileName is a file which will be written and contains for each user-resource-pair the top K tags from folkrank");
			System.err.println("  offset");
			System.err.println("  lines");
			System.err.println("  (optional) result Dimension: Users="+Udim+" Resources="+Rdim+" Tags="+Tdim);
			System.exit(1);
		}
		int argCtr = 0;
		final int topK = Integer.parseInt(args[argCtr++]);
		final String trainFileName = args[argCtr++];
		final String testFileName = args[argCtr++];
		final String predictionFileName = args[argCtr++];
		final int offset = Integer.parseInt(args[argCtr++]);
		final int lines = Integer.parseInt(args[argCtr++]);
		int resDim = Tdim;
		if (args.length>=6) {
			resDim = Integer.parseInt(args[argCtr++]);
		}
		
		log.warn(predictionFileName + ": Folkrank begins");

		/*
		 * If tas files come with words they are simply used for folkrank
		 * ATTENTION: URT should still be the correct order
		 * If the test files come with words, they will have to be read differentely
		 */

		/*
		 * read training data and init folkrank data
		 */
		System.err.println("reading facts");
		final FactPreprocessor processor = new FactReaderFactPreprocessor(new FileFactReader(trainFileName, TRAIN_FILE_DELIM), true);
		processor.process();
		FolkRankData frData = processor.getFolkRankData();
		int numUsers = frData.getCounts()[Udim].length;
		int numResources = frData.getCounts()[Rdim].length;
		numResources = Math.max(numResources, (int)Math.pow(10, 9));

		/*
		 * Prepare Writer for Output of the top k tags for each test (user-resource)-pair
		 */
		final BufferedWriter outWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(predictionFileName), "UTF-8"));

		/*
		 * Prepare Reader to read the test pairs 
		 */
		final BufferedReader trainReader = new BufferedReader(new InputStreamReader(new FileInputStream(testFileName), "UTF-8"));

		/*
		 * initialize FolkRank params
		 */
		final FolkRankParam param = new FolkRankParam();
		final FolkRankParam baseParam = new FolkRankParam();
		baseParam.setAlphaBetaGamma(0, 1, 0);
		/*
		 * all nodes get 1 unit preference
		 */
		final FolkRankPref pref = new FolkRankPref(new double[] {COMMON_BASE_PREF_WEIGHT, COMMON_BASE_PREF_WEIGHT, COMMON_BASE_PREF_WEIGHT});

		/*
		 * init FolkRank with test independent parameters
		 */
		final FolkRank folk = new FolkRank(baseParam, param);
		// do one calculation to init the baseline (this is needed since we sometimes use the baseline below)
		folk.computeFolkRank(frData, pref);

		/**
		 * This boolean is used to find out whether or not we have to use the folkrank baseline
		 * This occurs when we want to recommend for an unknown user with unknown resource(s)
		 */
		boolean needBaseline = true;
		int baseLineCount = 0;

		String line = null;
		/*
		 * iterate over the (user, resource)-pairs
		 */
		int processedLines = 0;
		while ((line = trainReader.readLine()) != null) {
			/*
			 * Decide whether the line is in the queried interval (between offset and offset + lines)
			 */
			processedLines++;
			if (processedLines <= offset) {
				continue;
			}
			if (processedLines > offset + lines) {
				break;
			}

			/*
			 * We have decided to process the line
			 */
			final int[][] prefItems = new int[][]{{},{},{}};
			final double[][] prefValues = new double[][]{{},{},{}};
			pref.setPreference(prefItems, prefValues);
			/*
			 * read in user, content id and resourcehash
			 */
			String[] parts = line.split(TEST_FILE_DELIM);
			String userId = parts[0].trim();
			String contentId = parts[1].trim();
			parts = parts[2].split(TEST_FILE_WORD_DELIM);
			// the zero th part is allways the resource id
			String resId = parts[0].trim();
			/*
			 * User Preferences
			 */
			Integer u = frData.getNodeMap().getInverseMapping(Udim, userId);
			if (u!=null) {
				// we have an old  user => we put the user into the preference vector
				needBaseline = false;
				prefItems[Udim] = new int[]{u};
				prefValues[Udim] = new double[]{numUsers};
			}
			/*
			 * decide to handle either resources or words
			 */
			if (parts.length == 1) {
				/*
				 *  we have no words => resourceId is R
				 */
				Integer r = frData.getNodeMap().getInverseMapping(Rdim, resId);
				if (r != null) {
					needBaseline = false;
					prefItems[Rdim] = new int[]{r};
					prefValues[Rdim] = new double[]{numResources};
				}
			} else {
				/*
				 *  we have several recources (words) in the R dimension,
				 */
				List<Integer> words = new ArrayList<Integer>();
				List<Double> scores = new ArrayList<Double>();
				/*
				 * go through the words, check whether they are known and set scores
				 * start with i=1 since for i=0 we would get the recourceHash stored in resId
				 */
				for (int i=1; i<parts.length; i++) {
					String word = parts[i].trim();
					double score = 1.0;
					Matcher matcher = pat.matcher(word);
					if (matcher.lookingAt()) {
						// we have a word with scores
						word = matcher.group(1).trim();
						score = Double.parseDouble(matcher.group(2).trim());
					}
					Integer w = frData.getNodeMap().getInverseMapping(Rdim, word);
					if (w!=null) {
						// we have an old wort to put into the preference vector
						words.add(w);
						scores.add(score);
					}
				}
				if (words.size()>0) {
					needBaseline = false;
					prefItems[Rdim] = new int[words.size()];
					prefValues[Rdim] = new double[words.size()];
					for (int i=0; i<words.size(); i++) {
						prefItems[Rdim][i] = words.get(i);
						prefValues[Rdim][i] = scores.get(i)*numResources;
					}
				}
			}

			FolkRankResult result = null;

			if (needBaseline) {
				result = folk.getBaselineResult();
				baseLineCount++;
				System.out.println("baseline");
			} else {
				/*
				 * set preference and compute
				 */
				pref.setPreference(prefItems, prefValues);

				log.info(predictionFileName + ": starting folkrank computation for user " + userId);
				result = folk.computeFolkRank(frData, pref);
				System.out.println("preference");
			}

			log.info(predictionFileName + ": finished folkrank computation for user " + userId);
			if (result==null) {
				System.out.println("result is null");
			} else {
				System.out.println("result is not null");
			}
			SortedSet<IntItemWithWeight> topKItems = getTopK(result.getWeights(), topK, resDim);
			log.info(predictionFileName + ": finished sorting results for user " + userId);

			/*
			 * output top k tags
			 */
			// write resource id (in output notation: starting with 1)
			outWriter.write(contentId);
			for (IntItemWithWeight item: topKItems) {
				// write tag id (in output notation: starting with 1)
				outWriter.write(RESULT_FILE_DELIM + frData.getNodeMap().getMapping(resDim)[item.getItem()]);
			}
			outWriter.newLine();

			// write user id and computation error to stderr
			System.err.println(u + ": " + result.getErrors());
		}
		outWriter.close();
		log.warn(predictionFileName + ": Folkrank took a total time of : " +(System.currentTimeMillis()-beginTime) +"Milis");
		log.warn(predictionFileName + ": The Baseline was used " + baseLineCount + " times.");
	}


	private static SortedSet<IntItemWithWeight> getTopK (double[][] weights, int k, int dim) {
		double minWeight;
		SortedSet<IntItemWithWeight> set = new TreeSet<IntItemWithWeight>();

		minWeight = -100; // consider only items with positive weight 
		for (int item = 0; item < weights[dim].length; item++) {
			double currWeight = weights[dim][item];
			if (currWeight > minWeight) {
				/* new weight to consider found */
				set.add(new IntItemWithWeight(item, currWeight));
				if (set.size() > k) {
					// new best weight, since we have more than k items in set
					IntItemWithWeight last = set.last();
					set.remove(last);
					minWeight = set.last().weight;
				}
			}
		}

		return set;
	}


}
