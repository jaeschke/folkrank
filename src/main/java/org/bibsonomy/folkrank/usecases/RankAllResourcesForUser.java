package org.bibsonomy.folkrank.usecases;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import org.apache.log4j.Logger;
import org.bibsonomy.folkrank.FolkRank;
import org.bibsonomy.folkrank.data.FolkRankData;
import org.bibsonomy.folkrank.data.FolkRankParam;
import org.bibsonomy.folkrank.data.FolkRankPref;
import org.bibsonomy.folkrank.data.FolkRankResult;
import org.bibsonomy.folkrank.io.FactReadingException;
import org.bibsonomy.folkrank.io.FileFactReader;
import org.bibsonomy.folkrank.process.FactPreprocessor;
import org.bibsonomy.folkrank.process.FactReaderFactPreprocessor;
import org.bibsonomy.folkrank.strategy.UnitedWeightInitializationStrategy;

public class RankAllResourcesForUser {

	private static final Logger log = Logger.getLogger(RankAllResourcesForUser.class);

	private static final double COMMON_BASE_PREF_WEIGHT = 0;
	private static final String TRAIN_FILE_DELIM = "\t";
	private static final String RESULT_FILE_DELIM = "\t";
	/*
	 *  These three parameters control the order of the dimensions 
	 *  (e.g. the dimension that contains the users is Udim)
	 *  The input file for training data must match this order
	 */
	private static final short Udim = 0;
	private static final short Rdim = 1;
	//private static final short Tdim = 2;

	public static void main(String[] args) throws IOException, FactReadingException {
		/*
		 * extract command line arguments
		 */
		if (args.length < 3) {
			System.err.println("usage:");
			System.err.println("java " + RankAllResourcesForUser.class.getName() + " factFile inputUsers outPutFileSuffix");
			System.err.println("  -- computes for users the weigths to all resources --");
			System.err.println("  where ");
			System.err.println("  factFileName     is the training file (facts)");
			System.err.println("  testFileName  is a file which contains users");
			System.err.println("  outPutFileName is a file which will be written and contains for each user the resources with weights as csv");
			System.exit(1);
		}
		int argCtr = 0;
		final String factFileName = args[argCtr++];
		final String userFileName = args[argCtr++];
		final String outPutFileName = args[argCtr++];

		/*
		 * read training data and init folkrank data
		 */
		System.out.println("reading facts");
		final FactPreprocessor processor = new FactReaderFactPreprocessor(new FileFactReader(factFileName, TRAIN_FILE_DELIM), true);
		processor.process();
		FolkRankData frData = processor.getFolkRankData();

		int numUsers = frData.getCounts()[Udim].length;
	
		/*
		 * Prepare Reader to read the test pairs 
		 */
		final BufferedReader userReader = new BufferedReader(new InputStreamReader(new FileInputStream(userFileName), "UTF-8"));

		/*
		 * initialize FolkRank params
		 */
		final FolkRankParam param = new FolkRankParam();
		final FolkRankParam baseParam = new FolkRankParam();
		baseParam.setAlphaBetaGamma(0, 1, 0);
		param.setAlphaBetaGamma(0, 0.7, 0.3);
		param.setWeightInitializationStrategy(new UnitedWeightInitializationStrategy());
		/*
		 * all nodes get 1 unit preference
		 */
		final FolkRankPref pref = new FolkRankPref(new double[] {COMMON_BASE_PREF_WEIGHT, COMMON_BASE_PREF_WEIGHT, COMMON_BASE_PREF_WEIGHT});

		/*
		 * init FolkRank with test independent parameters
		 */
		final FolkRank folk = new FolkRank(baseParam, param);


		final BufferedWriter outWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outPutFileName), "UTF-8"));
		String line = null;
		/*
		 * iterate over the (user, resource)-pairs
		 */
		while ((line = userReader.readLine()) != null) {

			/*
			 * read in user and resourcehash
			 */
			String userId = line.trim();
	
			Integer u = frData.getNodeMap().getInverseMapping(Udim, userId);
			if (u == null) {
				System.err.println("User "+userId + " not found. Avoiding ColdStart Problem by not recommending.");
				continue;
			}
			final int[][] prefItems = new int[][]{{},{},{}};
			final double[][] prefValues = new double[][]{{},{},{}};
			pref.setPreference(prefItems, prefValues);
			prefItems[Udim]  = new int[]{u};
			prefValues[Udim] = new double[]{numUsers};

			/*
			 * set preference and compute
			 */
			pref.setPreference(prefItems, prefValues);

			log.info("starting folkrank computation for user " + userId);
			final FolkRankResult result = folk.computeFolkRank(frData, pref);

			log.info("finished folkrank computation for user " + userId);
			/*
			 * Prepare Writer for Output of the top k tags for each test (user-resource)-pair
			 */

			for (int item=0; item<result.getWeights()[Rdim].length; item ++) {
				// write tag id (in output notation: starting with 1)
				outWriter.write(userId + RESULT_FILE_DELIM + frData.getNodeMap().getMapping(Rdim)[item] + RESULT_FILE_DELIM +result.getWeights()[Rdim][item]);
				outWriter.newLine();
			}

			// write user id and computation error to stderr
			System.out.println(u + ": " + result.getErrors());
		}
		outWriter.close();

	}

}
