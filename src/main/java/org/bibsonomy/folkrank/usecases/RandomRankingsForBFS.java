package org.bibsonomy.folkrank.usecases;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Arrays;
import java.util.Random;
import java.util.SortedSet;
import java.util.TreeSet;

import org.apache.log4j.Logger;
import org.bibsonomy.folkrank.FolkRank;
import org.bibsonomy.folkrank.data.FolkRankData;
import org.bibsonomy.folkrank.data.FolkRankParam;
import org.bibsonomy.folkrank.data.FolkRankPref;
import org.bibsonomy.folkrank.data.FolkRankResult;
import org.bibsonomy.folkrank.data.ItemWithWeight;
import org.bibsonomy.folkrank.io.FactReadingException;
import org.bibsonomy.folkrank.io.FileFactReader;
import org.bibsonomy.folkrank.process.FactPreprocessor;
import org.bibsonomy.folkrank.process.FactReaderFactPreprocessor;

/**
 * 
 * @author:  rja
 * @version: $Id$
 * $Author$
 * 
 */
public class RandomRankingsForBFS {

	private static final Logger log = Logger.getLogger(RandomRankingsForBFS.class);

	private static final int MAX_NO_OF_ITEMS = 100; // maximal number of items to write

	private final BufferedWriter writer;
	private final String factFileName;

	private boolean stop = false;

	public static void main(String[] args) throws FactReadingException, IOException {

		if (args.length < 2) {
			System.err.println("usage:");
			System.err.println("  java " + RandomRankingsForBFS.class.getName() + " tags.txt random_folkrank");
			System.exit(1);
		}

		/**
		 * format of file:
		 * tag|#|user|#|resource\n
		 */
		final String factFileName = args[0];
		/**
		 * format of file:
		 * dim,item: dim,item,weight dim,item,weight dim,item,weight ...
		 * ...
		 */
		final String resultFileName = args[1];

		/*
		 * configure input/output
		 */
		final BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(resultFileName), "UTF-8"));

		final RandomRankingsForBFS folkrank = new RandomRankingsForBFS(factFileName, writer);

		/*
		 * register shutdown hook which allows FolkRank to gracefull stop
		 */
		Runtime.getRuntime().addShutdownHook(new MyShutdown(folkrank));


		folkrank.computeRankings();
	}

	public RandomRankingsForBFS(final String factFileName, final BufferedWriter writer) {
		this.writer = writer;
		this.factFileName = factFileName;
	}


	public void computeRankings() throws FactReadingException, IOException {
		/*
		 * read training data (and don't store inverse mapping!)
		 */
		//final FactPreprocessor processor = new IntegerFileFactPreprocessor(factFileName, new String[]{"dim0", "dim1", "dim2"});
		final FactPreprocessor processor = new FactReaderFactPreprocessor(new FileFactReader(factFileName, " "), false);
		/*
		 * process facts
		 */
		log.info("processing facts");
		processor.process();
		/*
		 * get FolkRankData object
		 */
		final FolkRankData factsData = processor.getFolkRankData();
		/*
		 * get number of resources/users
		 */
		final int[] counts = new int[]{factsData.getCounts()[0].length, factsData.getCounts()[1].length, factsData.getCounts()[2].length};
		log.info("got " + Arrays.toString(counts) + " items");
		/*
		 * initialize parameters for FolkRank
		 */
		final FolkRankParam param = new FolkRankParam();
		/*
		 * create a new FolkRank instance
		 */
		final FolkRank folkrank = new FolkRank(param);
		/*
		 * initialize preference
		 */
		final FolkRankPref pref = new FolkRankPref(new double[] {1.0, 1.0, 1.0});
		/*
		 * generate random tag recommendations for each dimension
		 */
		final Random rand = new Random(1000);
		while (!stop) {
			/*
			 * dimension loop
			 */
			for (int dim = 0; dim < counts.length; dim++) {
				/*
				 * get random node
				 */
				int item = rand.nextInt(counts[dim]);
				/*
				 * set general preference
				 */
				final int[][] prefItems = new int[][]{{},{},{}};
				final double[][] prefValues = new double[][]{{},{},{}};
				pref.setPreference(prefItems, prefValues);
				/*
				 * set preference for dimension and node
				 */
				prefItems[dim]  = new int[]{item};
				prefValues[dim] = new double[]{counts[dim]};
				/*
				 * do computation
				 */
				final FolkRankResult result = folkrank.computeFolkRank(factsData, pref);
				/*
				 * sort and write result for each dimension 
				 */
				writer.write(dim + "," + factsData.getNodeMap().getMapping(dim)[item] + ":");
				for (int dimR = 0; dimR < 3; dimR++) {
					final SortedSet<ItemWithWeight<String>> sortedItems = sortItems(result.getWeights()[dimR], factsData.getNodeMap().getMapping(dimR));
					/*
					 * write result
					 */
					writeResult(dimR, sortedItems);
				}
				writer.write("\n");
			}
		}
		log.info("shutting down ..");
		writer.close();
		log.info("stopped.");
	}


	/** Writes results in the format
	 * <pre>
	 * dim,item1,weight dim,item2,weight  ...
	 * ...
	 * </pre>
	 * @param dim
	 * @param sortedItems
	 * @throws IOException
	 */
	private void writeResult(final int dim, final SortedSet<ItemWithWeight<String>> sortedItems) throws IOException {
		for (final ItemWithWeight<String> item: sortedItems) {
			writer.write(" " + dim + "," + item.item + "," + item.weight);
		}
	}


	/** Sorts all items from the result and maps them back to strings.
	 * Returns only the top {@value #MAX_NO_OF_ITEMS} tags. 
	 * 
	 * @param result
	 * @param inverseMapping
	 * @return
	 */
	private SortedSet<ItemWithWeight<String>> sortItems(final double[] weights, final String[] inverseMapping) {
		final SortedSet<ItemWithWeight<String>> set = new TreeSet<ItemWithWeight<String>>();
		double minWeight = -100;

		for (int item = 0; item < weights.length; item++) {
			double currWeight = weights[item];
			if (currWeight > minWeight) {
				final String itemString = inverseMapping[item];
				set.add(new ItemWithWeight<String>(itemString, currWeight));
				if (set.size() > MAX_NO_OF_ITEMS) {
					// new best weight, since we have more than k items in set
					final ItemWithWeight<String> last = set.last();
					set.remove(last);
					minWeight = set.last().weight;
				}
			}
		}

		return set;
	}

	public void stop() {
		log.info("shutdown requested ...");
		this.stop = true;
	}



	// Example shutdown hook class
	private static class MyShutdown extends Thread {

		private final RandomRankingsForBFS folkrank;

		public MyShutdown(RandomRankingsForBFS folkrank) {
			super();
			this.folkrank = folkrank;
		}


		@Override
		public void run() {
			folkrank.stop();
		}
	}

}

