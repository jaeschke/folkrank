package org.bibsonomy.folkrank.usecases;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

import org.bibsonomy.folkrank.FolkRank;
import org.bibsonomy.folkrank.data.FolkRankData;
import org.bibsonomy.folkrank.data.FolkRankParam;
import org.bibsonomy.folkrank.data.FolkRankPref;
import org.bibsonomy.folkrank.data.FolkRankResult;
import org.bibsonomy.folkrank.data.ItemWithWeight;
import org.bibsonomy.folkrank.io.FactReadingException;
import org.bibsonomy.folkrank.io.FileFactReader;
import org.bibsonomy.folkrank.process.FactPreprocessor;
import org.bibsonomy.folkrank.process.FactReaderFactPreprocessor;

/**
 * 
 * @author:  rja
 * @version: $Id$
 * $Author$
 * 
 */
public class JensRecommender {

	private static final int TAG = 1; // tag dimension
	private static final int RES = 2; // resource dimension
	private static final int MAX_NO_OF_TAGS_TO_RECOMMEND = 75; // maximal number of tags to recommend

	public static void main(String[] args) throws FactReadingException, IOException {

		final String baseDir = "/home/rja/recommender/jens";
		/**
		 * format of file:
		 * user\ttag\tresource\n
		 */
		final String trainFileName = baseDir + "/train.dat";

		/**
		 * format of file:
		 * resource\n
		 */
		final String testFileName  = baseDir + "/test_hashes";
		/**
		 * format of file:
		 * tagId\ttag\n
		 */
		final String testTagsFileName  = baseDir + "/test_tags.dat";

		final String resultFileName = baseDir + "/result.dat";
		
		/*
		 * read test data resources 
		 * 
		 * We do this before reading the training data such that we can map
		 * the resource hashes to integer ids in the training data during
		 * reading the training data.
		 */
		final String[] testResources = readTestResources(testFileName);

		/*
		 * read test tags
		 */
		final String[] testTags = readTestTags(testTagsFileName);
		

		/*
		 * read training data
		 */
		final FactPreprocessor processor = new FactReaderFactPreprocessor(new FileFactReader(trainFileName, "\t"));
		/*
		 * set testResources as prefItems to get back a mapping
		 * String -> Integer
		 */
		processor.setPrefItems(new String[][]{{},testTags,testResources});
		/*
		 * process facts
		 */
		processor.process();

		/*
		 * get FolkRankData object
		 */
		final FolkRankData factsData = processor.getFolkRankData();
		/*
		 * get number of resources
		 */
		final int numberOfResources = factsData.getCounts()[RES].length;

		/*
		 * get prefItem mappings
		 */
		final int[] testResourceMappings = processor.getPrefItems()[RES]; // 2 = resource dimension
		final int[] testTagsMappings     = processor.getPrefItems()[TAG]; // 1 = tag dimension
		final Map<Integer, String> inverseTestTagsMap = getInverseMap(testTags, testTagsMappings);

		/*
		 * initialize parameters for FolkRank
		 */
		final FolkRankParam param = new FolkRankParam();

		/*
		 * create a new FolkRank instance
		 */
		final FolkRank folkrank = new FolkRank(param);

		final FolkRankPref pref = new FolkRankPref(new double[] {1.0, 1.0, 1.0});

		/*
		 * compute baseline
		 */
		final FolkRankResult baseLineResult = folkrank.compute(factsData, pref);
		/*
		 * sort baseline
		 */
		final SortedSet<ItemWithWeight<String>> sortedBaselineTags = sortTags(baseLineResult, inverseTestTagsMap);

		final BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(resultFileName), "UTF-8"));
		
		/*
		 * generate tag recommendations for each resource in the test data set
		 */
		for (int i = 0; i < testResources.length; i++) {
			final String testResource = testResources[i];
			final int testResourceId = testResourceMappings[i];

			SortedSet<ItemWithWeight<String>> sortedTags; 
			
			if (testResourceId != -1) {
				System.err.print("f");
				/*
				 * resource contained in training data
				 */
				pref.setPreference(new int[][]{{},{},{testResourceId}}, new double[][]{{},{},{1.0/numberOfResources}});
				final FolkRankResult result = folkrank.computeFolkRank(factsData, pref);
				/*
				 * sort result
				 */
				sortedTags = sortTags(result, inverseTestTagsMap);
				
			} else {
				System.err.print(".");
				/*
				 * resource not contained in training data
				 * --> take baseline
				 */
				sortedTags = sortedBaselineTags;
			}
			/*
			 * print result
			 */
			writer.write(testResource + ": " + sortedTags + "\n");
			if (i % 50 == 0) {
				System.err.println();
			}
			
		}
		writer.close();
	}

	
	/** Creates the inverse map of keys[i] -> values[i], i.e., 
	 * values[i] -> keys[i].
	 *  
	 * @param keys
	 * @param values
	 * @return
	 */
	private static Map<Integer, String> getInverseMap(final String[] keys, final int[] values) {
		final Map<Integer, String> map = new HashMap<Integer, String>();
		for (int i = 0; i < keys.length; i++) {
			map.put(values[i], keys[i]);
		}
		return map;
	}
	

	/** Sorts all tags from the result which are contained in testTagsMapping.
	 * Returns only the top {@value #MAX_NO_OF_TAGS_TO_RECOMMEND} tags. 
	 * 
	 * @param result
	 * @param testTagMapping
	 * @return
	 */
	private static SortedSet<ItemWithWeight<String>> sortTags(final FolkRankResult result, final Map<Integer,String> testTagMapping) {
		final SortedSet<ItemWithWeight<String>> set = new TreeSet<ItemWithWeight<String>>();
		final double[] tagWeights = result.getWeights()[TAG];
		double minWeight = -100;

		for (int item = 0; item < tagWeights.length; item++) {
			if (testTagMapping.containsKey(item)) {
				double currWeight = tagWeights[item];
				if (currWeight > minWeight) {
					final String tag = testTagMapping.get(item);
					set.add(new ItemWithWeight<String>(tag, currWeight));
					if (set.size() > MAX_NO_OF_TAGS_TO_RECOMMEND) {
						// new best weight, since we have more than k items in set
						final ItemWithWeight<String> last = set.last();
						set.remove(last);
						minWeight = set.last().weight;
					}
				}
			}
		}

		return set;
	}



	/** Reads all resource hashes from the given file. The file format should be:
	 * <br/>
	 * resourceHash1
	 * resourceHash2
	 * ...
	 * 
	 * @param testFileName
	 * @return
	 * @throws IOException 
	 */
	private static String[] readTestResources(final String testFileName) throws IOException {
		final List<String> testResources = new LinkedList<String>();
		final BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(testFileName), "UTF-8"));
		String line;
		while ((line = reader.readLine()) != null) {
			testResources.add(line.trim().toLowerCase());
		}
		reader.close();
		/*
		 * copy list into array for prefItems
		 */
		final String[] testResourcesArray = new String[testResources.size()];
		int i = 0;
		for (final String resource: testResources) {
			testResourcesArray[i++] = resource;
		}
		/*
		 * destroy list;
		 */
		testResources.clear();

		return testResourcesArray;
	}

	/** Reads all tags from the given file. The file format should be:
	 * <br/>
	 * tagId\ttag\n
	 * 
	 * @param testFileName
	 * @return
	 * @throws IOException 
	 */
	private static String[] readTestTags(final String testTagsFileName) throws IOException {
		final List<String> testTags = new LinkedList<String>();
		final BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(testTagsFileName), "UTF-8"));
		String line;
		while ((line = reader.readLine()) != null) {
			testTags.add(line.trim().split("\t")[1]);
		}
		reader.close();
		/*
		 * copy list into array for prefItems
		 */
		final String[] testTagsArray = new String[testTags.size()];
		int i = 0;
		for (final String resource: testTags) {
			testTagsArray[i++] = resource;
		}
		/*
		 * destroy list;
		 */
		testTags.clear();

		return testTagsArray;
	}

}

