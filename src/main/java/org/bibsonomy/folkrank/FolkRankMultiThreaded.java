package org.bibsonomy.folkrank;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.log4j.Logger;
import org.bibsonomy.folkrank.data.FolkRankData;
import org.bibsonomy.folkrank.data.FolkRankParam;
import org.bibsonomy.folkrank.data.NodeMap;
import org.bibsonomy.folkrank.data.OutputData;
import org.bibsonomy.folkrank.database.DBHandler;
import org.bibsonomy.folkrank.io.DBFactReader;
import org.bibsonomy.folkrank.io.DBJobQueue;
import org.bibsonomy.folkrank.io.DBRankWriter;
import org.bibsonomy.folkrank.io.FactReader;
import org.bibsonomy.folkrank.io.FactReadingException;
import org.bibsonomy.folkrank.io.DBJobQueue.RankingJob;
import org.bibsonomy.folkrank.process.FactPreprocessor;
import org.bibsonomy.folkrank.process.FactReaderFactPreprocessor;
import org.bibsonomy.folkrank.strategy.SeparatedWeightInitializationStrategy;
import org.bibsonomy.folkrank.strategy.WeightInitializationStrategy;

/** Multithreaded version of FolkRank. 
 * 
 * @author ckr
 * @version $Id$
 */
public class FolkRankMultiThreaded {

	private static final Logger log = Logger.getLogger(FolkRankMultiThreaded.class);

	private FactReader<String> reader;
	private FactPreprocessor prep;
	private FolkRankData facts;
	private final FolkRankParam param;
	private final DBJobQueue jobQueue;
	private final DBRankWriter rwriter;
	/**
	 * queue for results
	 */
	private final ConcurrentLinkedQueue<OutputData<String>> resultQueue = new ConcurrentLinkedQueue<OutputData<String>>();

	private final Properties prop;

	/*
	 * default settings
	 */
	private int k = 20;
	private int noOfThreads = 3;
	private int maxNoOfRuns = 0; // 0 = infinity
	private int milliSecondsBetweenRuns = 0;
	private int milliSecondsBetweenComputations = 0;
	private int maxSecondsJobAge = 60;
	private int jobqueueCommitCount = 10000;

	private int maxSecondsWaitForNewResults = 1;


	public FolkRankMultiThreaded (final Properties prop) throws FactReadingException, SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		this.prop = prop;

		DBHandler.setProperties(prop);

		/*
		 * configure FolkRrank
		 */
		log.info("configuring folkrank parameters");

		param = new FolkRankParam();
		param.setAlphaBetaGamma(getDouble("folkrank.alpha"), getDouble("folkrank.beta"), getDouble("folkrank.gamma"));
		param.setStopCondition(getDouble("folkrank.epsilon"), getInt("folkrank.maxIter"));

		log.info("initializing weight initializing strategy");
		WeightInitializationStrategy strategy = null;      

		try {
			strategy = (WeightInitializationStrategy) Class.forName(prop.getProperty("folkrank.strategy")).newInstance();
		} catch (final Exception e) {
			/*
			 * TODO: implement proper exception handling
			 */
			log.warn("initialization of weight initialization strategy failed, setting " + 
					SeparatedWeightInitializationStrategy.class.getName() + " as default.");
			strategy = new SeparatedWeightInitializationStrategy();
		}

		param.setWeightInitializationStrategy(strategy);

		/*
		 * rank writer
		 */
		log.info("initializing rank writer");

		rwriter = new DBRankWriter();

		/*
		 * initialize job queue
		 */
		log.info("initializing job queue reader/writer");
		jobQueue = new DBJobQueue();

		/*
		 * initialize: 
		 * - number of top-k items to compute, 
		 * - maximal number of runs
		 * - waiting time between two computations
		 * - waiting time between two runs 
		 * - number of jobs added to the queue before "commit" is called
		 */
		k                               = getInt("folkrank.k");
		maxNoOfRuns                     = getInt("folkrank.maxNoOfRuns");
		milliSecondsBetweenRuns         = getInt("folkrank.milliSecondsBetweenRuns");
		milliSecondsBetweenComputations = getInt("folkrank.milliSecondsBetweenComputations");
		noOfThreads					    = getInt("folkrank.threadSize");
		jobqueueCommitCount             = getInt("folkrank.jobqueue.commitCount");
		maxSecondsJobAge                = getInt("folkrank.jobqueue.maxSecondsJobAge");
		maxSecondsWaitForNewResults     = getInt("folkrank.jobqueue.maxSecondsWaitForNewResults");

	}

	private void runFolkRank() {
		try {
			/*
			 * configure input database connection
			 */
			log.info("configuring slave database connection for data input");
			final Connection slaveConnection = DBHandler.getInstance().getSlaveConnection();
			/*
			 * disable transactions to speed up reading
			 */
			slaveConnection.setTransactionIsolation(Connection.TRANSACTION_READ_UNCOMMITTED);

			reader = new DBFactReader(slaveConnection, prop.getProperty("db.query.input"), 3);

			/*
			 * generate preprocessor for facts, preprocess, extract facts
			 */
			log.info("preprocessing facts (i.e., reading data)");
			prep = new FactReaderFactPreprocessor(reader);
			prep.process(); 
			facts = prep.getFolkRankData();


			/*
			 * do the real work
			 */
			log.info("processing job queue");

			if (maxNoOfRuns == 0){
				log.info("running an infinite number of runs");
				while (true){
					oneRun(facts.getNodeMap());
				}
			} else {
				log.info("running " + maxNoOfRuns + " runs");
				for (int i = 0; i < maxNoOfRuns; i++){
					oneRun(facts.getNodeMap());
				}
			}

		} catch (final SQLException e) {
			log.fatal(e, e);
		} catch (final NumberFormatException e) {
			log.fatal(e);
		} catch (final FactReadingException e) {
			log.fatal(e);
		} 
	}

	/** Does one run, including creation of threads.
	 * 
	 * @param mapping
	 * @throws SQLException
	 */
	private void oneRun(final NodeMap nodeMap) {
		/*
		 * fill queue
		 */
		try {
			if (jobQueue.isEmpty() && facts.getFacts().length > 0) {
				log.info("filling job queue");
				fillJobQueue();
			}
		} catch (final SQLException e) {
			log.fatal("could not check or fill job queue: " + e);
		}

		/*
		 * create and start the threads
		 */
		log.info("creating thread pool with " + noOfThreads + " threads");
		final ExecutorService threadPool = Executors.newFixedThreadPool(noOfThreads);

		log.info("starting " + noOfThreads + " threads");
		for (int i = 0; i < noOfThreads; i++){
			threadPool.execute(new JobQueueProcessor(nodeMap, param, jobQueue, facts, k, milliSecondsBetweenComputations, resultQueue));
		}

		/*
		 * writing results
		 */
		try{
			writeResults();
			Thread.sleep(milliSecondsBetweenRuns);
		} catch (final InterruptedException e){
			log.error(e);
		}

		/*
		 * shutting thread pool down
		 */
		log.info("shutting thread pool down");
		threadPool.shutdown();

	}

	/** Writes the output to the database.
	 * 
	 * @throws SQLException
	 */
	private void writeResults() {
		log.info("start writing the output data");

		/*
		 * If queue is empty, we're finished.
		 */
		while (!jobQueue.isEmpty()) {
			/*
			 * write all results from the result queue into the database and delete the finished jobs
			 */
			clearResultQueue();
			/*
			 * kill possibly old jobs
			 */
			jobQueue.killOldJobs(maxSecondsJobAge);
			try {
				log.info("main thread sleeping for " + maxSecondsWaitForNewResults + " second(s) because no results to write");
				Thread.sleep(maxSecondsWaitForNewResults * 1000);
			} catch (final InterruptedException e) {
				log.fatal(e);
			}
		}
		/*
		 * When we reach this point, ALL threads on ALL machines are finished ...
		 * 
		 * Now we can release the new rankings by renaming the ranking/weight tables.
		 * 
		 * ALL machines try to do this ... the first one wins (this is a transaction!) 
		 */
		log.info("release new rankings by renaming the corresponding table");
		try {
			DBHandler.getInstance().activateNewRanking();
		} catch (SQLException e) {
			log.error("Could not release new rankings.", e);
		}
	}

	/** Writes all results in the result queue to the database.
	 * 
	 * FIXME: this is not deadlock safe!
	 * @throws SQLException - if the transactions could not be rolled back on deadlock.
	 */
	private void clearResultQueue() {
		int resultCounter = 0; 
		while(resultQueue.size() > 0) {
			final OutputData<String> result = resultQueue.poll();
			try {
				/*
				 * insert ranking
				 */
				rwriter.writeTopK(result.getTopK(), result.getDate(), 
						result.getAlpha(), result.getBeta(), result.getGamma(), 
						result.getDelta(), result.getIter(), result.getPrefDim(), result.getPrefItem(), result.getPrefValue(), result.getBasePref());

				/*
				 * delete finished job
				 */
				jobQueue.remove(new RankingJob(result.getPrefDim(), result.getPrefItem(), 0)); // FIXME: correct job id!
			} catch (SQLException e) {
				log.error("Could not insert ranking/delete job.", e);
			}
			resultCounter++;
		}
		if (resultCounter > 0) {
			log.info("wrote " + resultCounter + " rankings");
		}
	}

	/** Main method to run folkrank.
	 * 
	 * @param args - args[0] must contain the path to the properties file for 
	 * configuring the folkrank runs.
	 * @throws ClassNotFoundException 
	 * @throws IllegalAccessException 
	 * @throws InstantiationException 
	 * @throws SQLException 
	 * @throws FactReadingException 
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 */
	public static void main(String[] args) throws FactReadingException, SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException, FileNotFoundException, IOException {
		if (args.length < 1) {
			System.out.println("usage:");
			System.out.println("  java " + FolkRankMultiThreaded.class.getName() + " folkrank.properties");
			System.exit(1);
		}

		// TODO: remove! FIXME
		//PropertyConfigurator.configureAndWatch( "src/main/resources/org/bibsonomy/folkrank/log4j.properties", 60*1000 );

		/*
		 * load properties to configure FolkRank
		 */
		final Properties prop = new Properties();
		prop.load(new FileInputStream(args[0]));

		/*
		 * start the computation
		 */
		final FolkRankMultiThreaded folkrank = new FolkRankMultiThreaded(prop);
		folkrank.runFolkRank();

	}

	/** Adds for each item in each dimension a ranking job to the job queue.
	 * 
	 * Note: since this is done in rather big chunks ("folkrank.jobqueue.commitCount")
	 * before commiting, there will be deadlocks, if {@link FolkRankMultiThreaded} is 
	 * started on another machine during filling the job queue. 
	 * 
	 * @throws SQLException
	 */
	private synchronized void fillJobQueue () throws SQLException {

		int commitCounter = 0;

		final NodeMap nodeMap = facts.getNodeMap();
		for (int dim = 0; dim < facts.getDims(); dim++) {
			int counter = 0;
			for (final String item: nodeMap.getMapping(dim)) {
				jobQueue.addWithoutCommit(new DBJobQueue.RankingJob(dim, item, 0));
				counter++;
				commitCounter++;
				if (commitCounter % jobqueueCommitCount == 0) {
					jobQueue.commitAdd();
				}
			}
			log.info("added " + counter + " items in dimension " + dim + " to ranking queue");
		}

		jobQueue.commitAdd();
	}

	/** Extract a double property.
	 * @param propName
	 * @return A double value contained in propName.
	 */
	private double getDouble(String propName) {
		return Double.parseDouble(prop.getProperty(propName).trim());
	}

	/** Extract an integer property.
	 * @param propName
	 * @return An integer value contained in propName.
	 */
	private int getInt(final String propName) {
		try {
			return Integer.parseInt(prop.getProperty(propName).trim());
		} catch (final NumberFormatException e) {
			log.warn("Could not get property " + propName + ": " + e);
		} catch (final NullPointerException e) {
			log.warn("Could not get property " + propName + ": " + e);
		}
		throw new NumberFormatException();
	}
}