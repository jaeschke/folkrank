package org.bibsonomy.folkrank.map;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Random;
import java.util.SortedSet;
import java.util.TreeSet;

import org.bibsonomy.folkrank.FolkRank;
import org.bibsonomy.folkrank.data.FolkRankData;
import org.bibsonomy.folkrank.data.FolkRankParam;
import org.bibsonomy.folkrank.data.FolkRankPref;
import org.bibsonomy.folkrank.data.FolkRankResult;
import org.bibsonomy.folkrank.data.StandardFolkRankResult;
import org.junit.Before;
import org.junit.Test;

/**
 * @author rja
 * @version $Id$
 */
public class GenericDataMapperTest {

	private static final double MIN = Double.NEGATIVE_INFINITY; 
	
	private final FolkRankData data = new FolkRankData(10, new int[]{2, 10, 10}, null);
	private final Random rand = new Random();
	
	
	@Before
	public void setUp() {
		data.setFact(0, new int[]{0, 0, 0});
		data.setFact(1, new int[]{0, 1, 2});
		data.setFact(2, new int[]{0, 1, 3});
		data.setFact(3, new int[]{0, 2, 3});
		data.setFact(4, new int[]{0, 2, 5});
		data.setFact(5, new int[]{1, 0, 9});
		data.setFact(6, new int[]{1, 0, 1});
		data.setFact(7, new int[]{1, 9, 9});
		data.setFact(8, new int[]{1, 9, 1});
		data.setFact(9, new int[]{1, 9, 7});
	}
	
	@Test
	public void testMap() {
		
		final DataMapper mapper = new GenericDataMapper(3);
		
		final FolkRankData newData = mapper.mapData(data);
		final int[][] newCounts = newData.getCounts();
		/*
		 * check number of elements for each dimension
		 */
		assertEquals(2, newCounts[0].length);
		assertEquals(4, newCounts[1].length);
		assertEquals(7, newCounts[2].length);
		/*
		 * check new facts
		 */
		final int[][] newFacts = newData.getFacts();
		assertArrayEquals(new int[]{0, 0, 0}, newFacts[0]);
		assertArrayEquals(new int[]{0, 1, 1}, newFacts[1]);
		assertArrayEquals(new int[]{0, 1, 2}, newFacts[2]);
		assertArrayEquals(new int[]{0, 2, 2}, newFacts[3]);
		assertArrayEquals(new int[]{0, 2, 3}, newFacts[4]);
		assertArrayEquals(new int[]{1, 0, 4}, newFacts[5]);
		assertArrayEquals(new int[]{1, 0, 5}, newFacts[6]);
		assertArrayEquals(new int[]{1, 3, 4}, newFacts[7]);
		assertArrayEquals(new int[]{1, 3, 5}, newFacts[8]);
		assertArrayEquals(new int[]{1, 3, 6}, newFacts[9]);
		
	}

	@Test
	public void testMapBack() {
		final DataMapper mapper = new GenericDataMapper(3);
		
		mapper.mapData(data);
		
		final FolkRankResult folkRankResult = new StandardFolkRankResult();
		
		folkRankResult.setWeights(new double[][]{
				new double[]{0.0, 0.1},
				new double[]{0.0, 0.1, 0.2, 0.9},
				new double[]{0.0, 0.2, 0.3, 0.5, 0.9, 0.1, 0.7}
		});
		
		final FolkRankResult mappedBackResult = mapper.mapResult(folkRankResult);
		
		final double[][] weights = mappedBackResult.getWeights();
		assertArrayEquals(new double[]{0.0, 0.1}, weights[0], 10e-15);
		assertArrayEquals(new double[]{0.0, 0.1, 0.2, MIN, MIN, MIN, MIN, MIN, MIN, 0.9}, weights[1], 10e-15);
		assertArrayEquals(new double[]{0.0, 0.1, 0.2, 0.3, MIN, 0.5, MIN, 0.7, MIN, 0.9}, weights[2], 10e-15);

	}

	/**
	 * A test with random data that shall ensure that there are no gaps in the 
	 * result data. 
	 * 
	 * @throws Exception
	 */
	@Test
	public void testGapRemoval() throws Exception {
		/*
		 * change experiment parameters here
		 */
		final int minNoOfFacts = 100;
		final int maxNoOfFacts = 20000;
		final int[] maxNoOfItems = new int[]{30, 50, minNoOfFacts};
		final int runs = 100;

		for (int i = 0; i < runs; i++) {
			/*
			 * get random facts
			 */
			final FolkRankData data = getRandomFacts(minNoOfFacts, maxNoOfFacts, maxNoOfItems);
			/*
			 * map them
			 */
			final GenericDataMapper mapper = new GenericDataMapper(maxNoOfItems.length);
			final FolkRankData newData = mapper.mapData(data);
			/*
			 * check for gaps
			 */
			final int[][] facts = newData.getFacts();
			for (int dim = 0; dim < maxNoOfItems.length; dim++) {
				assertHasNoGaps(facts, dim);
			}
		}
	}
	
	@Test
	public void testGapRemovalWithFolRank() throws Exception {
		/*
		 * change experiment parameters here
		 */
		final int minNoOfFacts = 100;
		final int maxNoOfFacts = 20000;
		final int[] maxNoOfItems = new int[]{30, 50, minNoOfFacts};
		final int runs = 100;

		for (int i = 0; i < runs; i++) {
			/*
			 * get random facts
			 */
			final FolkRankData data = getRandomFacts(minNoOfFacts, maxNoOfFacts, maxNoOfItems);
			/*
			 * map them
			 */
			final GenericDataMapper mapper = new GenericDataMapper(maxNoOfItems.length);
			final FolkRankData newData = mapper.mapData(data);
			/*
			 * check for gaps
			 */
			final int[][] facts = newData.getFacts();
			for (int dim = 0; dim < maxNoOfItems.length; dim++) {
				assertHasNoGaps(facts, dim);
			}
			/*
			 * run FolkRank
			 */
			final FolkRank folkRank = new FolkRank(new FolkRankParam(0.7));
			final FolkRankPref pref = new FolkRankPref(new double[]{1.0,1.0,1.0});
			final FolkRankResult result = folkRank.computeFolkRank(newData, pref);
			final FolkRankResult mappedBackResult = mapper.mapResult(result);
		}
	}
	
	
	private void assertHasNoGaps(final int[][] facts, final int dim) {
		final SortedSet<Integer> items = new TreeSet<Integer>();
		for (int i = 0; i < facts.length; i++) {
			items.add(facts[i][dim]);
		}
		assertEquals(new Integer(0), items.first());
		assertEquals(new Integer(items.size() - 1), items.last());
	}
	
	/**
	 * Generates a random fact list 
	 * 
	 * @param minSize - the minimal number of facts
	 * @param maxSize - the maximal number of facts
	 * @param maxNoOfItems - the maximal number of items per dimension (this array also
	 * determines the number of dimensions)
	 * @return
	 */
	private FolkRankData getRandomFacts(final int minSize, final int maxSize, final int[] maxNoOfItems) {
		final int noOfDims = maxNoOfItems.length;
		final int noOfFacts = rand.nextInt(maxSize - minSize) + minSize;
		
		final int[][] facts = new int[noOfFacts][noOfDims];
		final int[] noOfItemsPerDimension = new int[noOfDims];
		Arrays.fill(noOfItemsPerDimension, Integer.MIN_VALUE);
		// generate facts
		for (int i = 0; i < facts.length; i++) {
			facts[i] = new int[noOfDims];
			for (int dim = 0; dim < noOfDims; dim++) {
				final int item = rand.nextInt(maxNoOfItems[dim]);
				facts[i][dim] = item;
				// remember max value
				if (item >= noOfItemsPerDimension[dim]) {
					noOfItemsPerDimension[dim] = item + 1;
				}
			}
		}
//		System.out.println("count = " + Arrays.toString(noOfItemsPerDimension));
//		System.out.println("facts = " + Arrays.deepToString(facts));
		
		final FolkRankData folkRankData = new FolkRankData(noOfFacts, noOfItemsPerDimension, null);
		// add facts
		for (int i = 0; i < facts.length; i++) {
			folkRankData.setFact(i, facts[i]);
		}
		
		return folkRankData;
	}
	
}
