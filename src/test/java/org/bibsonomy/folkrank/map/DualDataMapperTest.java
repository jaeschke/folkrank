package org.bibsonomy.folkrank.map;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import org.bibsonomy.folkrank.data.FolkRankData;
import org.junit.Before;
import org.junit.Test;


/**
 * @author rja
 * @version $Id$
 */
public class DualDataMapperTest {
	private final FolkRankData data = new FolkRankData(10, new int[]{2, 10, 10}, null);
	
	@Before
	public void setUp() {
		data.setFact(0, new int[]{0, 0, 0});
		data.setFact(1, new int[]{0, 1, 2});
		data.setFact(2, new int[]{0, 1, 3});
		data.setFact(3, new int[]{0, 2, 3});
		data.setFact(4, new int[]{0, 2, 5});
		data.setFact(5, new int[]{1, 0, 9});
		data.setFact(6, new int[]{1, 0, 1});
		data.setFact(7, new int[]{1, 9, 9});
		data.setFact(8, new int[]{1, 9, 1});
		data.setFact(9, new int[]{1, 9, 7});
	}
	
	@Test
	public void testMapDual() throws Exception {
		
		final GenericDataMapper dataMapper1 = new GenericDataMapper(3);
		final GenericDataMapper dataMapper2 = new GenericDataMapper(3);
		
		final DualDataMapper mapper = new DualDataMapper(dataMapper1, dataMapper2);
		
		
		final FolkRankData newData = mapper.mapData(data);
		
		final int[][] newCounts = newData.getCounts();
		/*
		 * check number of elements for each dimension
		 */
		assertEquals(2, newCounts[0].length);
		assertEquals(4, newCounts[1].length);
		assertEquals(7, newCounts[2].length);
		/*
		 * check new facts
		 */
		final int[][] newFacts = newData.getFacts();
		assertArrayEquals(new int[]{0, 0, 0}, newFacts[0]);
		assertArrayEquals(new int[]{0, 1, 1}, newFacts[1]);
		assertArrayEquals(new int[]{0, 1, 2}, newFacts[2]);
		assertArrayEquals(new int[]{0, 2, 2}, newFacts[3]);
		assertArrayEquals(new int[]{0, 2, 3}, newFacts[4]);
		assertArrayEquals(new int[]{1, 0, 4}, newFacts[5]);
		assertArrayEquals(new int[]{1, 0, 5}, newFacts[6]);
		assertArrayEquals(new int[]{1, 3, 4}, newFacts[7]);
		assertArrayEquals(new int[]{1, 3, 5}, newFacts[8]);
		assertArrayEquals(new int[]{1, 3, 6}, newFacts[9]);
		
	}
}
