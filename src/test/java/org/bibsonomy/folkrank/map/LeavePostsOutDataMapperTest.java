package org.bibsonomy.folkrank.map;

import junit.framework.Assert;

import org.bibsonomy.folkrank.FolkRankTest;
import org.bibsonomy.folkrank.data.FolkRankData;
import org.junit.Test;


public class LeavePostsOutDataMapperTest {
	
	@Test
	public void mapDataTest() {
		FolkRankData originalData = FolkRankTest.produceMissingValuesData();
		LeavePostsOutDataMapper mapper = new LeavePostsOutDataMapper(0, new int[]{0}, 0, 1);
		FolkRankData mappedData = mapper.mapData(originalData);
		Assert.assertEquals(3, mappedData.getFacts().length);
		Assert.assertEquals(-1, mappedData.getFacts()[0][3]);
		Assert.assertEquals(5, mappedData.getCounts()[0][0]);
		Assert.assertEquals(3, mappedData.getCounts()[0][1]);
		Assert.assertEquals(3, mappedData.getCounts()[1][0]);
		Assert.assertEquals(5, mappedData.getCounts()[1][1]);
		Assert.assertEquals(6, mappedData.getCounts()[2][0]);
		Assert.assertEquals(2, mappedData.getCounts()[2][1]);
		Assert.assertEquals(3, mappedData.getCounts()[3][0]);
		Assert.assertEquals(3, mappedData.getCounts()[3][1]);
		/*
		 * DO testing if the mapping considers the missing values appropriately
		 * What happens in the actual folkrank. Is the weight spread correctly?
		 */
	}

}
