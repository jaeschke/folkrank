package org.bibsonomy.folkrank.usecases;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.io.InputStream;
import java.util.SortedSet;

import org.bibsonomy.folkrank.data.ItemWithWeight;
import org.bibsonomy.folkrank.io.FactReadingException;
import org.bibsonomy.folkrank.io.StreamFactReader;
import org.junit.Test;

public class FolkRankComparisonTest {

	/**
	 * Tests the ranking for "boomerang".
	 * 
	 * @throws FactReadingException
	 * @throws IOException
	 */
	@Test
	public void testGetTopK() throws FactReadingException, IOException {
		final InputStream facts = FolkRankComparisonTest.class.getClassLoader().getResourceAsStream("group_kde.tas");
		final FolkRankComparison comparison = new FolkRankComparison(new StreamFactReader(facts, " ", 3));
		
		final SortedSet<ItemWithWeight<String>>[] topK = comparison.getTopK("{0, {}, 0, {'boomerang' 5}, 0, {}, 0.2, 0.5, 0.3, 1e-4, 50, 1}", 10);
		
		/*
		 * christoph is our boomerang expert
		 */
		assertEquals("schmitz", topK[0].iterator().next().getItem());
		/*
		 * the highest ranked tag should be "boomerang" itself.
		 */
		assertEquals("boomerang", topK[1].iterator().next().getItem());
	}

}
