package org.bibsonomy.folkrank;

import static org.junit.Assert.assertEquals;

import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.SortedSet;

import org.bibsonomy.folkrank.data.ItemWithWeight;
import org.junit.Ignore;
import org.junit.Test;

@Ignore
public class FolkRankInMemoryMultiThreadingTest {

    @Test
    
    public void testFolkrank() {
        initFolkRank();

        final List<String> preference = new LinkedList<String>();
        preference.add("semantic");
        /*
         * run folkrank
         */
        final FolkRankInMemoryMultiThreading folkrank = new FolkRankInMemoryMultiThreading(FolkRankInMemoryMultiThreading.ItemType.TAG, preference);
        folkrank.run();

        /*
         * print results
         */
        final SortedSet<ItemWithWeight<String>>[] result = folkrank.getResult();
        printResults(result);

        /*
         * test results
         */
        assertEquals(preference.get(0), result[1].first().item);
    }

    @Test
    public void testMultiThreadingFolkRank() {
        initFolkRank();

        final List<String> preference = new LinkedList<String>();
        preference.add("semantic");
        
        for (int i = 0; i < 1000; i++) {
            /*
             * run folkrank
             */
            final FolkRankInMemoryMultiThreading folkrank = new FolkRankInMemoryMultiThreading(FolkRankInMemoryMultiThreading.ItemType.TAG, preference);
            new Thread(folkrank);
        }


    }


    /**
     * Initializes the FolkRank algorithm (i.e. sets the data and parameters)
     */
    private void initFolkRank() {
        /*
         * set data
         */
        FolkRankInMemoryMultiThreading.updateData(getData());
        /*
         * configure properties
         */
        FolkRankInMemoryMultiThreading.configure(getProperties());
    }

    private void printResults(final SortedSet<ItemWithWeight<String>>[] itemsWithWeight) {
        for (int dim = 0; dim < itemsWithWeight.length; dim++) {
            System.out.println("--- dim = " + dim + " ---");
            for (final ItemWithWeight<String> itemWithWeight: itemsWithWeight[dim]) {
                System.out.println(itemWithWeight.item + ": " + itemWithWeight.weight);
            }
        }
    }

    private Properties getProperties() {
        final Properties prop = new Properties();
        prop.setProperty("folkrank.strategy", "org.semanticdesktop.nepomuk.comp.folkpeer.folkrank.strategy.SeparatedWeightInitializationStrategy");
        prop.setProperty("folkrank.alpha", "0.0");
        prop.setProperty("folkrank.beta", "0.6");
        prop.setProperty("folkrank.gamma", "0.4");
        prop.setProperty("folkrank.epsilon", "10e-5");
        prop.setProperty("folkrank.maxIter", "10");
        prop.setProperty("folkrank.k", "100");
        prop.setProperty("folkrank.maxNoOfRuns", "0");
        return prop;
    }

    private List<String[]> getData() {
        final List<String[]> tas = new LinkedList<String[]>();

        tas.add(new String[]{"robert", "semantic", "http://www.semanticweb.org"});
        tas.add(new String[]{"robert", "web", "http://www.semanticweb.org"});
        tas.add(new String[]{"robert", "search", "http://www.google.de"});
        tas.add(new String[]{"robert", "web", "http://www.google.de"});
        tas.add(new String[]{"robert", "engine", "http://www.google.de"});

        tas.add(new String[]{"claudia", "search", "http://www.google.de"});
        tas.add(new String[]{"claudia", "engine", "http://www.google.de"});
        tas.add(new String[]{"claudia", "search", "http://www.yahoo.com"});
        tas.add(new String[]{"claudia", "engine", "http://www.yahoo.com"});
        tas.add(new String[]{"claudia", "search", "http://www.metager.de"});
        tas.add(new String[]{"claudia", "web", "http://www.metager.de"});
        tas.add(new String[]{"claudia", "folksonomy", "http://www.bibsonomy.org"});
        tas.add(new String[]{"claudia", "search", "http://www.bibsonomy.org"});
        tas.add(new String[]{"claudia", "tagging", "http://www.bibsonomy.org"});

        tas.add(new String[]{"dirk", "tagging", "http://www.bibsonomy.org"});
        tas.add(new String[]{"dirk", "search", "http://www.google.de"});
        return tas;
    }

}
