package org.bibsonomy.folkrank.data;

import static org.junit.Assert.assertEquals;

import java.util.TreeSet;

import org.junit.Test;

/**
 * @author rja
 * @version $Id$
 */
public class IntItemWithWeightTest {

	/**
	 * In the LeavePostOutTest we remove the last element from a TreeSet - which
	 * should be the one with the smallest weight. This test ensures that our
	 * compareTo implementation is correct such that the set is in descending 
	 * order.
	 */
	@Test
	public void testCompareTo() {
		final TreeSet<IntItemWithWeight> set = new TreeSet<IntItemWithWeight>();
		set.add(new IntItemWithWeight(1, 0.1));
		set.add(new IntItemWithWeight(2, 0.2));
		set.add(new IntItemWithWeight(3, 0.3));
		set.add(new IntItemWithWeight(4, 0.25));
		set.add(new IntItemWithWeight(5, 0.15));
		set.add(new IntItemWithWeight(6, 0.05));

		/*
		 * smallest one is the last element
		 */
		assertEquals(0.05, set.last().getWeight(), 10e-15);
		/*
		 * biggest one is the first element
		 */
		assertEquals(0.3, set.first().getWeight(), 10e-15);
		
		/*
		 * let's ensure that negative values also work ...
		 */
		set.add(new IntItemWithWeight(7, -0.05));
		assertEquals(-0.05, set.last().getWeight(), 10e-15);
		
		System.out.println(set);
	}

}
