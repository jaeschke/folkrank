package org.bibsonomy.folkrank;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import junit.framework.Assert;

import org.apache.derby.iapi.services.io.NewByteArrayInputStream;
import org.bibsonomy.folkrank.data.FolkRankData;
import org.bibsonomy.folkrank.data.FolkRankParam;
import org.bibsonomy.folkrank.data.FolkRankPref;
import org.bibsonomy.folkrank.data.FolkRankResult;
import org.bibsonomy.folkrank.io.DBFactReader;
import org.bibsonomy.folkrank.io.FactReader;
import org.bibsonomy.folkrank.io.FactReadingException;
import org.bibsonomy.folkrank.io.FileFactReader;
import org.bibsonomy.folkrank.io.StreamFactReader;
import org.bibsonomy.folkrank.process.FactReaderFactPreprocessor;
import org.bibsonomy.folkrank.process.HelperMethods;
import org.bibsonomy.folkrank.process.IntegerFileFactPreprocessor;
import org.junit.Test;

/**
 * Tests some general properties of FolkRank, e.g., the sums of the 
 * weights, ... 
 * 
 * @author rja
 *
 */
public class FolkRankTest {

	private static final double MAX_WEIGHT_LOSS = 10e-14;
	private static final String FR_WP_CORR = "folkRankWithPrefCorr";
	private static final String PR_NP0 = "pageRankWithoutPref0";
	private static final String TAB = "\t";
	private static final String PR_WP0 = "pageRankWithPref0";
	private static final String FR_WP = "folkRankWithPref0";
	private static final String PR_WP1 = "pageRankWithPref1";
	private static final double MAX_ERROR = 10e-15;
	private static final int MAX_ITER = 1000;
	private static final double DAMPING_D = 0.7; 

	private static final int PRINT_MAX_VALUES = 10;
	private static final int PRINT_DIM = 0;
	/*
	 * items are separated by " "; order:
	 * user tag resource
	 */
	private static final String TEST_FACTS_ITEM_SEPARATOR = " ";
	private static final String TEST_REGULAR_FACTS = "group_kde.tas";
	private static final int NO_OF_REGULAR_DIMENSIONS = 3;

	private static final String TEST_HYPER_FACTS = "multi_dim_example.tas";
	private static final int NO_OF_HYPER_DIMENSIONS = 8;
	/*
	 * no preference
	 */
	private static final String[][] NO_REGULAR_PREF_ITEMS  = new String[][]{new String[]{}, new String[]{}, new String[]{}};
	private static final double[][] NO_REGULAR_PREF_VALUES = new double[][]{new double[]{}, new double[]{}, new double[]{}};
	private static final String[][] NO_HYPER_PREF_ITEMS  = new String[][]{new String[]{}, new String[]{}, new String[]{}, new String[]{}, new String[]{}, new String[]{}, new String[]{}, new String[]{}};
	private static final double[][] NO_HYPER_PREF_VALUES = new double[][]{new double[]{}, new double[]{}, new double[]{}, new double[]{}, new double[]{}, new double[]{}, new double[]{}, new double[]{}};
	/*
	 * preference for the tag "semantic" = 99
	 */
	private static final String REGULAR_PREF_ITEM = "semantic";
	private static final String HYPER_PREF_ITEM = "conceptB";
	
	private static final int REGULAR_PREF_ITEM_INT = 99;
	private static final int HYPER_PREF_ITEM_INT = 1;
	private static final int PREF_VALUE = 1;
	private static final String[][] REGULAR_PREF_ITEMS  = new String[][]{new String[]{}, new String[]{REGULAR_PREF_ITEM},  new String[]{}};
	private static final String[][] HYPER_PREF_ITEMS  = new String[][]{new String[]{}, new String[]{}, new String[]{}, new String[]{}, new String[]{}, new String[]{}, new String[]{HYPER_PREF_ITEM},  new String[]{}};
	private static final double[][] REGULAR_PREF_VALUES = new double[][]{new double[]{}, new double[]{PREF_VALUE}, new double[]{}};
	private static final double[][] HYPER_PREF_VALUES = new double[][]{new double[]{}, new double[]{}, new double[]{}, new double[]{}, new double[]{}, new double[]{}, new double[]{PREF_VALUE}, new double[]{}};
	private static final int REGULAR_PREF_DIM = 1;
	private static final int HYPER_PREF_DIM = 6;
	
	
	private static Connection getConnection() throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		Class.forName("com.mysql.jdbc.Driver").newInstance();
		return DriverManager.getConnection(
				"jdbc:mysql://gromit:3306/bibsonomy?useUnicode=true&characterEncoding=UTF-8&characterSetResults=UTF-8",
				"bibsonomy",
				"12bibkde"
		);
	}

	/**
	 * Computes the baseline ranking and checks general properties:
	 * 
	 * <ul>
	 * <li>The sum of the resulting weights should be 1 for each dimension: 1 - |w| &lt; 10e-14 </li>
	 * <li>All weights should be equal or larger than 0:                    w[i] &gt;= 0</li>
	 * </ul>
	 * 
	 */
	@Test
	public void testBaselineProperties() {
		testBaselineProperties(NO_REGULAR_PREF_ITEMS, NO_REGULAR_PREF_VALUES, TEST_REGULAR_FACTS, NO_OF_REGULAR_DIMENSIONS);
		testBaselineProperties(NO_HYPER_PREF_ITEMS, NO_HYPER_PREF_VALUES, TEST_HYPER_FACTS, NO_OF_HYPER_DIMENSIONS);
	}

	private void testBaselineProperties(final String[][] noPrefItems, final double[][] noPrefValues, final String testFacts, final int noOfDimensions) {
		try {
			final FolkRankResult ranking = computeTestRanking(1, 0, noPrefItems, noPrefValues, null, false, testFacts, noOfDimensions);
			/*
			 * check the weights
			 */
			final double[][] weights = ranking.getWeights();
			for (int dim = 0;  dim < noOfDimensions; dim++) {
				/*
				 * 1 - |w| should be smaller than 10e-14
				 */
				assertTrue(Math.abs(1 - HelperMethods.getSum(weights[dim])) < MAX_WEIGHT_LOSS);
				/*
				 * w[i] >= 0
				 */
				for (int i = 0; i < weights[dim].length; i++) {
					assertTrue(weights[dim][i] >= 0);
				}
			}
		} catch (FactReadingException e) {
			fail(e.getMessage());
		}
	}

	/**
	 * Computes the advanced PageRank ranking and checks general properties:
	 * 
	 * <ul>
	 * <li>The sum of the resulting weights should be 1 for each dimension: 1 - |w| &lt; 10e-14 </li>
	 * <li>All weights should be equal or larger than 0:                    w[i] &gt;= 0</li>
	 * </ul>
	 */
	@Test 
	public void testAprPreferenceProperties() {
		testAprPreferenceProperties(TEST_REGULAR_FACTS, NO_OF_REGULAR_DIMENSIONS, REGULAR_PREF_ITEMS, REGULAR_PREF_VALUES, REGULAR_PREF_ITEM_INT, REGULAR_PREF_DIM);
		testAprPreferenceProperties(TEST_HYPER_FACTS, NO_OF_HYPER_DIMENSIONS, HYPER_PREF_ITEMS, HYPER_PREF_VALUES, HYPER_PREF_ITEM_INT, HYPER_PREF_DIM);
	}
	
	private void testAprPreferenceProperties(final String testFacts, final int noOfDimensions, final String[][] prefItems, final double[][] prefValues, final int prefItemInt, final int prefDim) {
		try {
			/*
			 * compute ranking for tag 'semantic'
			 */
			final FolkRankResult ranking = computeTestRanking(0.7, 0, prefItems, prefValues, null, false, testFacts, noOfDimensions);
			/*
			 * check the weights
			 */
			final double[][] weights = ranking.getWeights();
			for (int dim = 0;  dim < noOfDimensions; dim++) {
				/*
				 * 1 - |w| should be smaller than 10e-14
				 */
				assertTrue(Math.abs(1 - HelperMethods.getSum(weights[dim])) < MAX_WEIGHT_LOSS);
				/*
				 * w[i] >= 0
				 */
				for (int i = 0; i < weights[dim].length; i++) {
					assertTrue(weights[dim][i] >= 0);
				}
			}
			/*
			 * get top item (tag)
			 * should be 99 = semantic
			 */
			int topTag = 0;
			double topWeight = Double.MIN_VALUE;
			final double[] tWeights = weights[prefDim];
			for (int tag = 0; tag < tWeights.length; tag++) {
				if (tWeights[tag] > topWeight) {
					topWeight = tWeights[tag];
					topTag = tag;
				}
			}
			assertEquals(prefItemInt, topTag);
		} catch (FactReadingException e) {
			fail(e.getMessage());
		}
	}
	
	/**
	 * Computes the FolkRank ranking and checks general properties:
	 * 
	 * <ul>
	 * <li>The sum of the resulting weights should be 0 for each dimension: |w| &lt; 10e-14 </li>
	 * </ul>
	 */
	@Test 
	public void testFolkRankProperties() {
		testFolkRankProperties(TEST_REGULAR_FACTS, NO_OF_REGULAR_DIMENSIONS, REGULAR_PREF_ITEMS, REGULAR_PREF_VALUES, REGULAR_PREF_ITEM_INT, REGULAR_PREF_DIM);
		testFolkRankProperties(TEST_HYPER_FACTS, NO_OF_HYPER_DIMENSIONS, HYPER_PREF_ITEMS, HYPER_PREF_VALUES, HYPER_PREF_ITEM_INT, HYPER_PREF_DIM);
	}
	
	
	private void testFolkRankProperties(final String testFacts, final int noOfDimensions, final String[][] prefItems, final double[][] prefValues, final int prefItemInt, final int prefDim) {
		try {
			/*
			 * compute ranking for tag 'semantic'
			 */
			final FolkRankResult ranking = computeTestRanking(0.7, 0, prefItems, prefValues, null, true, testFacts, noOfDimensions);
			/*
			 * check the weights
			 */
			final double[][] weights = ranking.getWeights();
			for (int dim = 0;  dim < noOfDimensions; dim++) {
				/*
				 * |w| should be smaller than 10e-14
				 */
				assertTrue(Math.abs(HelperMethods.getSum(weights[dim])) < MAX_WEIGHT_LOSS);
			}
			/*
			 * get top item (tag)
			 * should be 99 = semantic
			 */
			int topTag = 0;
			double topWeight = Double.MIN_VALUE;
			final double[] tWeights = weights[prefDim];
			for (int tag = 0; tag < tWeights.length; tag++) {
				if (tWeights[tag] > topWeight) {
					topWeight = tWeights[tag];
					topTag = tag;
				}
			}
			/*
			 * best items in each dimension (?) should have positive weight
			 */
			assertEquals(prefItemInt, topTag);
		} catch (FactReadingException e) {
			fail(e.getMessage());
		}
	}
	
	public void testCompute() throws Exception {
		
		
		/*
		 * no preference -> baseline
		 */

		//                                                             base
		final FolkRankResult pageRankWithPref0    = compute(DAMPING_D, 0, REGULAR_PREF_ITEMS,   REGULAR_PREF_VALUES,   null, false, PR_WP0);
		final FolkRankResult pageRankWithPref1    = compute(DAMPING_D, 1, REGULAR_PREF_ITEMS,   REGULAR_PREF_VALUES,   null, false, PR_WP1);
		final FolkRankResult pageRankWithoutPref0 = compute(1,         0, NO_REGULAR_PREF_ITEMS, NO_REGULAR_PREF_VALUES, null, false, PR_NP0);
		final FolkRankResult folkRankWithPref     = compute(DAMPING_D, 0, REGULAR_PREF_ITEMS,   REGULAR_PREF_VALUES,   null, true,  FR_WP);
		// as in the paper: d_0 = 1, p_1 = [0]_jaeschke = viel
		final FolkRankResult folkRankWithPrefCorr = compute(DAMPING_D, 0, REGULAR_PREF_ITEMS,   REGULAR_PREF_VALUES,   pageRankWithoutPref0, true, FR_WP_CORR);

		System.out.println(TAB + PR_WP0 + TAB + FR_WP);
		printPreference(REGULAR_PREF_ITEMS);
		printResults(pageRankWithPref0, folkRankWithPref);

		System.out.println(TAB + PR_WP0 + TAB + PR_NP0);
		printPreference(NO_REGULAR_PREF_ITEMS);
		printResults(pageRankWithPref0, pageRankWithoutPref0);

		System.out.println(TAB + PR_WP1 + TAB + FR_WP);
		printPreference(REGULAR_PREF_ITEMS);
		printResults(pageRankWithPref1, folkRankWithPref);

		System.out.println(TAB + PR_WP1 + TAB + PR_WP0);
		printPreference(REGULAR_PREF_ITEMS);
		printResults(pageRankWithPref1, pageRankWithPref0);
		
		System.out.println(TAB + PR_WP0 + TAB + FR_WP_CORR);
		printPreference(REGULAR_PREF_ITEMS);
		printResults(pageRankWithPref0, folkRankWithPrefCorr);
	}

	public static void main(String[] args) throws FactReadingException, FileNotFoundException {

		final FolkRankTest t = new FolkRankTest();
		final FolkRankResult cta = t.computeTasAll();
		final double[][] ctaw = cta.getWeights();
		for (int i = 0; i < PRINT_MAX_VALUES; i++) {
			final double wa = ctaw[PRINT_DIM][i];
			System.out.println(
					i + ":\t" +
					wa + TAB
			);
		}
	}
	
	private FolkRankResult computeTasAll() throws FactReadingException, FileNotFoundException {
		final FactReader<String> factReader = new FileFactReader("/home/rja/del2005_tas_all.txt", "|#|");

		final FactReaderFactPreprocessor prep = new FactReaderFactPreprocessor(factReader, true);
		prep.setPrefItems(new String[][]{new String[]{"boomerang"}, new String[]{},new String[]{}});
		prep.process();
		final int[][] prefItems2 = prep.getPrefItems();


		final FolkRankData data = prep.getFolkRankData();
		final FolkRankParam param = new FolkRankParam();
		param.setStopCondition(10e-4, 50);
		param.setAlphaBetaGamma(0.2, 0.5, 0.3);

		final FolkRankPref pref = new FolkRankPref(new double[] {
				0,0,0
		});
		pref.setPreference(prefItems2, new double[][]{new double[]{5}, new double[]{}, new double[]{}});

		final FolkRank folkRank = new FolkRank(param);
		return folkRank.computeFolkRank(data, pref);
	}
	

	
	
	/**
	 * 
	 * 
	 * @param factReader
	 * @param d
	 * @param basePrefAll
	 * @param prefItems
	 * @param prefValues
	 * @param baseline
	 * @param useFolkRank - if <code>true</code>, {@link FolkRank#computeFolkRank(FolkRankData, FolkRankPref)} is called
	 * instead of {@link FolkRank#compute(FolkRankData, FolkRankPref)}.
	 * @return
	 * @throws FactReadingException
	 */
	private FolkRankResult computeTestRanking(final double d, final double basePrefAll, final String[][] prefItems, final double[][] prefValues, final FolkRankResult baseline, final boolean useFolkRank, final String testFacts, final int noOfDimensions) throws FactReadingException {
		final InputStream facts = FolkRankTest.class.getClassLoader().getResourceAsStream(testFacts);
		final FactReader<String> factReader = new StreamFactReader(facts, TEST_FACTS_ITEM_SEPARATOR, noOfDimensions);		

		// create a preprocessor that stores the inverse mappings
		final FactReaderFactPreprocessor prep = new FactReaderFactPreprocessor(factReader, true);
		// set preferred Items (p-Vector)
		prep.setPrefItems(prefItems);
		prep.process();
		// the indexes in the facts lists of the prefItems
		final int[][] prefItemsIndexes = prep.getPrefItems();

		final FolkRankData data = prep.getFolkRankData();
		
		// set param values
		final FolkRankParam param = new FolkRankParam();
		param.setStopCondition(MAX_ERROR, MAX_ITER);
		param.setAlphaBetaGamma(0, d, 1 - d);

		
		// set a base Preference for each element in each dimension
		final double[] basePref = new double[noOfDimensions];
		for (int dim = 0; dim < noOfDimensions; dim++) {
			basePref[dim] = basePrefAll;
		}
		// set base Pref and extra Pref for the actually preferenced items
		final FolkRankPref pref = new FolkRankPref(basePref);
		pref.setPreference(prefItemsIndexes, prefValues);

		final FolkRank folkRank = new FolkRank(param);
		if (baseline != null) folkRank.setBaselineResult(baseline);

		if (useFolkRank) {
			return folkRank.computeFolkRank(data, pref);
		}
		
		final FolkRankResult result = folkRank.compute(data, pref);
//		System.out.println(HelperMethods.getTopK(data, result, 10)[1]);
		return result;
	}
	
	private FolkRankResult compute(final double d, final double basePrefAll, final String[][] prefItems, final double[][] prefValues, final FolkRankResult baseline, final boolean useFolkRank, final String alg) throws FactReadingException, FileNotFoundException {
		System.out.println(alg + ": folkrank: " + useFolkRank + ", base: " + basePrefAll + ", d: " + d);
		final double[] basePref = new double[] {
				basePrefAll,					basePrefAll, 		basePrefAll
		};

		final FactReader<String> factReader = new StreamFactReader(FolkRankTest.class.getClassLoader().getResourceAsStream(TEST_REGULAR_FACTS), TEST_FACTS_ITEM_SEPARATOR, 3);

		final FactReaderFactPreprocessor prep = new FactReaderFactPreprocessor(factReader, true);
		prep.setPrefItems(prefItems);
		prep.process();
		final int[][] prefItems2 = prep.getPrefItems();


		final FolkRankData data = prep.getFolkRankData();
		final FolkRankParam param = new FolkRankParam();
		param.setStopCondition(MAX_ERROR, MAX_ITER);
		param.setAlphaBetaGamma(0, d, 1 - d);

		final FolkRankPref pref = new FolkRankPref(basePref);
		pref.setPreference(prefItems2, prefValues);

		final FolkRank folkRank = new FolkRank(param);
		if (baseline != null) folkRank.setBaselineResult(baseline);


		if (useFolkRank) {
			return folkRank.computeFolkRank(data, pref);
		}
		return folkRank.compute(data, pref);
	}

	
	private void printPreference(final Object[][] prefItems) {
		for (int dim = 0; dim < prefItems.length; dim++) {
			System.out.print("\t{");
			for (int i = 0; i < prefItems[dim].length; i++) {
				System.out.print(prefItems[dim][i]);
			}
			System.out.print("}");
		}
		System.out.println();
	}


	private void printResults(final FolkRankResult a, final FolkRankResult b) {

		/*
		 * header
		 */
//		System.out.println(
//				"errors:\t" + 
//				a.getErrors().getLast() + TAB + 
//				b.getErrors().getLast()
//		);
		/*
		 * get sum of deltas
		 */
		double delta = 0;
		double suma = 0;
		double sumb = 0;
		final double[][] aWeights = a.getWeights();
		final double[][] bWeights = b.getWeights();

		for (int dim = 0; dim < aWeights.length; dim++) { 
			for (int i = 0; i < aWeights[dim].length; i++) {
				final double wa = aWeights[dim][i];
				final double wb = bWeights[dim][i];
				final double diff = Math.abs(wa - wb);
				suma += wa;
				sumb += wb;
				delta += diff;
			}
		}
		/*
		 * sums
		 */
		System.out.println("sums:\t" + suma + TAB + sumb + TAB + delta);
		//		Util.printWeightsSums(aWeights, "  postA");
		//		Util.printWeightsSums(bWeights, "  postB");

		/*
		 * data
		 */
		for (int i = 0; i < PRINT_MAX_VALUES; i++) {
			final double wa = aWeights[PRINT_DIM][i];
			final double wb = bWeights[PRINT_DIM][i];
			System.out.println(
					i + ":\t" +
					wa + TAB +
					wb + TAB +
					Math.abs(wa - wb)
			);
		}
	}

	
	/**
	 * Reads facts from the database and writes them into a file.
	 * 
	 * @throws FactReadingException
	 * @throws SQLException
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws ClassNotFoundException
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	private void writeFactsFromDatabase(final String file) throws FactReadingException,
	SQLException, InstantiationException, IllegalAccessException,
	ClassNotFoundException, FileNotFoundException, IOException {
		Class.forName("com.mysql.jdbc.Driver").newInstance();

		final Connection connection = DriverManager.getConnection(
				"jdbc:mysql://gromit:3306/bibsonomy?useUnicode=true&characterEncoding=UTF-8&characterSetResults=UTF-8",
				"bibsonomy",
				""
		);

		final String query = 
			"SELECT b.user_name, t.tag_lower, b.book_url_hash " + 
			"FROM bookmark b " + 
			"JOIN tas t USING (content_id) " +
			"JOIN groups g ON (g.user_name = b.user_name) " + 
			"WHERE g.group = 3 AND b.group = 0";


		final FactReader<String> factReader = new DBFactReader(connection, query, 3);
		final BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file)));
		while (factReader.hasNext()) {
			final String[] fact = factReader.getFact();
			for (int i = 0; i < fact.length; i++) {
				writer.write(fact[i]);
				if (i + 1 < fact.length) writer.write(TEST_FACTS_ITEM_SEPARATOR);
			}
			writer.write("\n");
		}
		writer.close();
		factReader.close();
	}

	@Test
	public void missingValuesTest() {
		final FolkRankData originalData = produceMissingValuesData();
		Assert.assertEquals(11, originalData.getCounts()[0][0]);
		Assert.assertEquals(3, originalData.getCounts()[0][1]);
		Assert.assertEquals(9, originalData.getCounts()[1][0]);
		Assert.assertEquals(5, originalData.getCounts()[1][1]);
		Assert.assertEquals(9, originalData.getCounts()[2][0]);
		Assert.assertEquals(5, originalData.getCounts()[2][1]);
		Assert.assertEquals(9, originalData.getCounts()[3][0]);
		Assert.assertEquals(3, originalData.getCounts()[3][1]);
	}

	public static FolkRankData produceMissingValuesData() {
		int numDims = 4;
		String dir = "/home/sdo/testing/missingValuesTest";
		final String[] mappingFileNames = new String[numDims];
		for (int dim = 0; dim < numDims; dim++) {
			mappingFileNames[dim] = dir + "/dim" + dim;
		}
		final IntegerFileFactPreprocessor prep = new IntegerFileFactPreprocessor(dir + "/fact", mappingFileNames);
		prep.process();
		final FolkRankData originalData = prep.getFolkRankData();
		return originalData;
	}
	
}
