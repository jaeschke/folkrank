package org.bibsonomy.folkrank.process;

import org.bibsonomy.folkrank.data.DefaultNodeMap;
import org.bibsonomy.folkrank.data.FolkRankData;
import org.bibsonomy.folkrank.data.FolkRankPref;
import org.bibsonomy.folkrank.strategy.WeightInitializationStrategy;
import org.bibsonomy.folkrank.strategy.ZeroTolerantSeparatedWeightInitializationStrategy;
import org.junit.Assert;
import org.junit.Test;

public class ZeroToleranceTest {


	@Test
	public void testZeroToleranceStrategyBasics() {
		final int DIMS = 8;
		int facts[][] = new int[4][];
		facts[0] = new int[]{1, 1, 3, 0, 1, 1, 1, 0};
		facts[1] = new int[]{1, 1, 2, 0, 1, 1, 1, 0};
		facts[2] = new int[]{1, 1, 3, 2, 1, 1, 1, 0};
		facts[3] = new int[]{1, 1, 2, 2, 1, 1, 1, 0};
		int[] numberOfItemsPerDimension = new int[DIMS];
		numberOfItemsPerDimension[0] =2;
		numberOfItemsPerDimension[1] =2;
		numberOfItemsPerDimension[2] =4;
		numberOfItemsPerDimension[3] =3;
		numberOfItemsPerDimension[4] =2;
		numberOfItemsPerDimension[5] =2;
		numberOfItemsPerDimension[6] =2;
		numberOfItemsPerDimension[7] =2;
		FolkRankData data = new FolkRankData(4, numberOfItemsPerDimension, new DefaultNodeMap(numberOfItemsPerDimension));
		for (int i = 0; i<4; i++) {
			data.setFact(i, facts[i]);
		}
		WeightInitializationStrategy strategy = new ZeroTolerantSeparatedWeightInitializationStrategy();
		double[] basePrefWeights = new double[DIMS];
		double[][] weights = new double[DIMS][];
		int[][] prefItems = new int[DIMS][];
		double[][] prefValues = new double[DIMS][];
		for (int dim=0; dim<DIMS; dim++) {
			basePrefWeights[dim] = 1;
			weights[dim] = new double[numberOfItemsPerDimension[dim]];
			prefItems[dim] = new int[]{};
			prefValues[dim] = new double[]{};
		}
		double[] prefWeights = new double[DIMS];
		double[] prefWeightsNormFactors = new double[DIMS];

		// Test1: give pref to user 0 who is an isolated node
		FolkRankPref pref = new FolkRankPref(basePrefWeights);
		prefItems[0] = new int[]{0};
		prefValues[0] = new double[]{1};
		pref.setPreference(prefItems, prefValues);
		try {
			strategy.initalizeWeights(pref, weights, prefWeights, prefWeightsNormFactors, data.getCounts());
			Assert.fail("My method did not throw an Illegal Argument Exception, but should have!");
		} catch (IllegalArgumentException e) {
		}
		
		// Test 2 now only non-isolated Items get Preference
		prefItems[0] = new int[]{1};
		strategy.initalizeWeights(pref, weights, prefWeights, prefWeightsNormFactors, data.getCounts());
		// assert weights (should be 0 where nodes are isolated) and should add up to 1 per Dimension
		Assert.assertEquals(0, weights[0][0], 0);
		Assert.assertEquals(1, weights[0][1], 0);
		Assert.assertEquals(0.5, weights[2][2], 0);
		Assert.assertEquals(0, weights[3][1], 0);
		for (int dim=0; dim<DIMS; dim++) {
			double sum = 0.0;
			for (int i = 0; i<weights[dim].length; i++) {
				sum += weights[dim][i];
			}
			Assert.assertEquals(1, sum, 0);
		}
		Assert.assertEquals(prefWeights[0], 0.5, 0);
		Assert.assertEquals(prefWeights[1], 1, 0);
		Assert.assertEquals(prefWeights[2], 0.5, 0);
		Assert.assertEquals(prefWeightsNormFactors[0], 2, 0);
		Assert.assertEquals(prefWeightsNormFactors[1], 1, 0);
		Assert.assertEquals(prefWeightsNormFactors[2], 2, 0);
		Assert.assertEquals(prefWeightsNormFactors[3], 2, 0);
		
		
		System.out.println("");
	}

}
