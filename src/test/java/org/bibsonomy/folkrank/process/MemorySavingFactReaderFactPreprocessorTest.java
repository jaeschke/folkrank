package org.bibsonomy.folkrank.process;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.FileNotFoundException;

import org.bibsonomy.folkrank.data.FolkRankData;
import org.bibsonomy.folkrank.data.NodeMap;
import org.bibsonomy.folkrank.io.FactReader;
import org.bibsonomy.folkrank.io.FactReadingException;
import org.bibsonomy.folkrank.io.FileFactReader;
import org.junit.Test;

public class MemorySavingFactReaderFactPreprocessorTest {

	/*
	 * items are separated by " "; order:
	 * user tag resourceint
	 */
	private static final String TEST_FACTS_ITEM_SEPARATOR = " ";
	private static final String TEST_REGULAR_FACTS = "group_kde.tas";
	private static final int NO_OF_REGULAR_DIMENSIONS = 3;

	private static final String TEST_HYPER_FACTS = "multi_dim_example.tas";
	private static final int NO_OF_HYPER_DIMENSIONS = 8;
	
	/**
	 * Checks, if reading the same fact file with the memory saving variant and the
	 * "regular" variant gives the same results.
	 */
	@Test
	public void testProcessFacts() {
		this.testProcessFacts(TEST_REGULAR_FACTS, NO_OF_REGULAR_DIMENSIONS);
		this.testProcessFacts(TEST_HYPER_FACTS, NO_OF_HYPER_DIMENSIONS);
	}
	
	/**
	 * Checks, if reading the same fact file with the memory saving variant and the
	 * "regular" variant gives the same results.
	 */
	private void testProcessFacts(final String TEST_FACTS, final int NO_OF_DIMENSIONS) {	
		final String factFile = MemorySavingFactReaderFactPreprocessor.class.getClassLoader().getResource(TEST_FACTS).toString().substring("file:".length());
		final FolkRankData folkRankData1 = getFolkRankDataWithMemorySavingFactFileFactPreprozessor(factFile);
		final FolkRankData folkRankData2 = getFolkRankDataWithFactFileReader(factFile);
		final int noOfDimensions = folkRankData1.getFacts()[0].length;
		assertEquals(NO_OF_DIMENSIONS, noOfDimensions);
		/*
		 * compare data
		 */
		assertArrayEquals(folkRankData2.getCounts(), folkRankData1.getCounts());
		
		final NodeMap nodeMap1 = folkRankData1.getNodeMap();
		final NodeMap nodeMap2 = folkRankData2.getNodeMap();
		for (int dim=0; dim < NO_OF_DIMENSIONS; dim++) {
			assertArrayEquals(folkRankData2.getFacts()[dim], folkRankData1.getFacts()[dim]);
			assertArrayEquals(nodeMap2.getMapping(dim), nodeMap1.getMapping(dim));
		}
		for (int dim = 0; dim < NO_OF_DIMENSIONS; dim++) {
			final Object[] mapping = nodeMap1.getMapping(dim);
			for (int i = 0; i < mapping.length; i++) {
				assertEquals(nodeMap1.getInverseMapping(dim, nodeMap1.getMapping(dim, i)), new Integer(i));
				assertEquals(nodeMap2.getInverseMapping(dim, nodeMap1.getMapping(dim, i)), new Integer(i));
				assertEquals(nodeMap2.getInverseMapping(dim, nodeMap1.getMapping(dim, i)), nodeMap1.getInverseMapping(dim, nodeMap1.getMapping(dim, i)));
			}
		}
	}
	
	
	
	/**
	 * read facts using {@link FactReaderFactPreprocessor}.
	 * 
	 * @param factFile
	 * @return
	 */
	private FolkRankData getFolkRankDataWithFactFileReader(final String factFile) {
		FactReader<String> factReader2 = null;
		try {
			factReader2 = new FileFactReader(factFile, TEST_FACTS_ITEM_SEPARATOR);
		} catch (FactReadingException e) {
			fail(e.getMessage());
		} catch (FileNotFoundException e) {
			fail(e.getMessage());
		}		
		final FactReaderFactPreprocessor frfp = new FactReaderFactPreprocessor(factReader2, true);
		frfp.process();
		final FolkRankData folkRankData2 = frfp.getFolkRankData();
		return folkRankData2;
	}

	/**
	 * Reads facts using {@link MemorySavingFactReaderFactPreprocessor}.
	 * 
	 * @param factFile
	 * @return
	 */
	private FolkRankData getFolkRankDataWithMemorySavingFactFileFactPreprozessor(final String factFile) {
		FactReader<String> factReader1 = null;
		try {
			factReader1 = new FileFactReader(factFile, TEST_FACTS_ITEM_SEPARATOR);
		} catch (FactReadingException e) {
			fail(e.getMessage());
		} catch (FileNotFoundException e) {
			fail(e.getMessage());
		}		
		final MemorySavingFactReaderFactPreprocessor msfrfp = new MemorySavingFactReaderFactPreprocessor(factReader1, true);
		msfrfp.process();
		final FolkRankData folkRankData1 = msfrfp.getFolkRankData();
		return folkRankData1;
	}

}
